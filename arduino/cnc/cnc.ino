//65.102.51.86

//***************************************************** Project Settings

/*
int testPin = 22;
int testFlipPin = 23;
bool testFlip = false;
bool doFlipTest = false;

int dirXPin = 24;
int xPin = 25;
int dirYPin = 26;
int yPin = 27;
int dirYPin2 = 28;
int yPin2 = 29;
int pumpPin = 30;
*/

int testPin = 22;
int testFlipPin = 22;
bool testFlip = false;
bool doFlipTest = false;

int dirXPin = 24;
int xPin = 25;
int dirYPin = 26;
int yPin = 27;
//int dirYPin2 = 12;
//int yPin2 = 13;
int dirZPin = 28;// z is height, always bugged me that x/y are not commonly map coordinates
int zPin = 29;
int pumpPin = 30;

int xAmount = 0;// -1 = moving left, 0 not moving, 1 moving right.  App will control this.
int yAmount = 0;
int zAmount = 0;

long xPosition = 0;// the step number
long yPosition = 0;
long zPosition = 0;

int stepMicrosecondDelay = 1;// the pulse of one on/off cycle, this is the speed

bool gotStartingValues = false;// only send values when we got the starting values from android
bool gotStartingX = false;
bool gotStartingY = false;
bool gotStartingZ = false;

long gotoX = 0;
long gotoY = 0;
long gotoZ = 0;

bool moveX = false;
bool moveY = false;
bool moveZ = false;

bool isPumping = false;

#include <SoftwareSerial.h>

// communication for the com port, pins are when using an UNO board, mSerial2 is for a MEGA 2560
//int comTxPin = 1;
//int comRxPin = 2;
#define mSerial2 Serial2
//SoftwareSerial mSerial2(4, 5);
int comBaudRate = 2400;//9600;// scale defaults to 2400
String comIncomingStr = "";
int comTransmitInterval = 100;// transmitting to Android this often, this is throttling
unsigned long lastComTransmit = 0;

#define testSerial Serial
bool useTestSerial = false;



//***************************************************** End Project Settings

//***************************************************** Utils 

#define mSerial3 Serial3
//SoftwareSerial mSerial3(2, 3);// RX, TX
boolean serial3isReady = false;
unsigned long lastSendCommandTime = 0;

char tempBuffer[7];

String floatToString(float f)
{
  dtostrf(f,0,2,tempBuffer);
  return String(tempBuffer);
}

String intToString(long i)
{
  return String(i);
}

long stringToInt(String str)
{
  return str.toInt();
}

void togglePin(int pinNum)
{
  if(digitalRead(pinNum) == HIGH)
  {
    digitalWrite(pinNum, LOW);
  }
  else
  {
    digitalWrite(pinNum, HIGH);
  }  
}

unsigned long getTimeSince(unsigned long then)
{
  // account for the possibility of millis() rolling over
  unsigned long now = millis();
  if(now < then)
  {
    unsigned long n = 4294967295 - then + now;
    return n;
  }
  return (unsigned long)now - then;
}

unsigned long getTimeSinceMicroseconds(unsigned long then)
{
  // account for the possibility of millis() rolling over
  unsigned long now = micros();
  if(now < then)
  {
    unsigned long n = 4294967295 - then + now;
    return n;
  }
  return (unsigned long)now - then;
}

boolean buttonClick(int inPin, boolean &state)
{
  return buttonClick(inPin, state, false);
}

boolean buttonClick(int inPin, boolean &state, boolean switchPolarity)
{
  // return true if clicked, click happens on button down (not button up)
  boolean buttonIsOn = false;
  if(switchPolarity)
  {
    if(digitalRead(inPin) == LOW)
    {
      buttonIsOn = true;
    }
  }
  else
  {
    if(digitalRead(inPin) == HIGH)
    {
      buttonIsOn = true;
    }
  }
  if(buttonIsOn != state)
  {
    if(buttonIsOn)
    {
      // click
      state = true;// sets the pointer variable
      return true;
    }
    else
    {
      state = false;
    }
  }
  return false;
}

boolean button(int outPin, boolean desiredState, boolean currentState)
{
  if(desiredState != currentState)
  {
    // changing
    currentState = desiredState;
    if(currentState)
    {
      digitalWrite(outPin, HIGH);
    }
    else
    {
      digitalWrite(outPin, LOW);
    }
  }
  return currentState;
}

boolean simpleSwitch(int inPin, int outPin, boolean state)
{
  return simpleSwitch(inPin, outPin, state, false);
}

boolean simpleSwitch(int inPin, int outPin, boolean state, boolean switchPolarity)
{


  boolean newState = state;
  if(state)
  {
    // we are currently on
    if(switchPolarity)
    {
      if(digitalRead(inPin) != LOW)
      {
        // turn off
        newState = false;
      }
    }
    else
    {
      if(digitalRead(inPin) != HIGH)
      {
        // turn off
        newState = false;
      }
    }
  }
  else
  {
    // we are currently off
    if(switchPolarity)
    {
      // turn on
      if(digitalRead(inPin) != HIGH)
      {
        newState = true;
      }
    }
    else
    {
      if(digitalRead(inPin) != LOW)
      {
        // turn on
        newState = true;
      }
    }
  }

  if(state != newState)
  {

    if(newState)
    {
      digitalWrite(outPin, HIGH);
    }
    else
    {
      digitalWrite(outPin, LOW);
    }
  }


  return newState;
}

//***************************************************** End Utils

/***************************************************** Bluetooth */

bool bluetoothIsConnected = false;// will turn true once we get confirmation that we are connected to Android

// Bluetooth rate is 38400
// connect tx/rx on Bluetooth to Serial3 tx/rx

void setupBlueToothConnection()
{
  mSerial3.begin(38400); //Set BluetoothBee BaudRate to default baud rate 38400
  // mSerial3.print("\r\n+STBD=38400\r\n"");// BaudRate
  //mSerial3.begin(57600);
  //mSerial3.print("\r\n+STBD=38400\r\n");// BaudRate
  //mSerial3.print("\r\n+STBD=57600\r\n");// BaudRate - might need to be put here
  //mSerial3.begin(115200); //Set BluetoothBee BaudRate to default baud rate 38400
  mSerial3.print("\r\n+STWMOD=0\r\n"); //set the bluetooth work in slave mode
  mSerial3.print("\r\n+STNA=SeeedBTSlave\r\n"); //set the bluetooth name as "SeeedBTSlave"
  mSerial3.print("\r\n+STOAUT=1\r\n"); // Permit Paired device to connect me
  mSerial3.print("\r\n+STAUTO=0\r\n"); // Auto-connection should be forbidden here
  delay(5000); // This delay is required.
  mSerial3.print("\r\n+INQ=1\r\n"); //make the slave bluetooth inquirable
  //mSerial3.print("\r\n+STBD=38400\r\n"");// BaudRate - might need to be put here
  //Serial.println("The slave bluetooth is inquirable!");
  delay(5000); // This delay is required.
  mSerial3.flush();
}

/***************************************************** End Bluetooth */

/***************************************************** Loop Serial Communication functions */

String incomingStr = "";

void doSerialLoopFunctions()
{
  String mChar;
  /*
  while(//mSerial.available() > 0)
  {
    mChar = String(char(//mSerial.read()));
    incomingStr += mChar;
  }
  */
  
  while(mSerial3.available())
  {
    mChar = String(char(mSerial3.read()));
    incomingStr += mChar;
  }
  /*
  while(acc.available())
  {
    mChar = String(char(acc.read()));
    incomingStr += mChar;
  }
  */
  
  
  sendTestSerial("incomingStr = " + incomingStr);
  
  // interpret string: incomingStr
  while(incomingStr.indexOf("</root>") > -1)
  {
    int i = incomingStr.indexOf("</root>");
    int e = i + 5;
    int b = incomingStr.indexOf("<root>");
    if(b > -1)
    {
      String stringToRead = incomingStr.substring(b, i);
      //Serial.println("stringToRead = " + stringToRead);
      int e2 = stringToRead.indexOf("</command>");
      if(e2 > -1)
      {
        int b2 = stringToRead.indexOf("<command>");
        if(b2 > -1)
        {
          // parse this command, expect only one command
          String command = stringToRead.substring(b2 + 9, e2);
          //mSerial.println("<root><incomingCommand>" + command + "</incomingCommand></root>");
          doCommand(command, stringToRead);
        }
      }
    }
    incomingStr = incomingStr.substring(i + 7);
  }  
  
}

String getParameters(String stringToRead)
{
  int e2 = stringToRead.indexOf("</params>");
  if(e2 > -1)
  {
    int b2 = stringToRead.indexOf("<params>");
    if(b2 > -1)
    {
      return stringToRead.substring(b2 + 8, e2);
    }
  }
  return "";
}


void doCommand(String command, String stringToRead)
{
  // from Android
  if(command == "startCommunication")
  {
    bluetoothIsConnected = true;
    sendSerial("<root><command>connectionComplete</command></root>");
  }
  if(command == "testForEcho")
  {
    bluetoothIsConnected = true;
    togglePin(testPin);
    // send back confirmation of connection
    //mSerial.print("<root><command>entreNous</command></root>");
    mSerial3.print("<root><command>entreNous</command></root>");    
  }

  String params = getParameters(stringToRead);
  
  if(command == "UP")
  {
    digitalWrite(testPin, HIGH);
    yAmount = 1;
  }
  if(command == "DOWN")
  {
    digitalWrite(testPin, HIGH);
    yAmount = -1;
  }
  if(command == "LEFT")
  {
    digitalWrite(testPin, HIGH);
    xAmount = -1;
  }
  if(command == "RIGHT")
  {
    digitalWrite(testPin, HIGH);
    xAmount = 1;
  }
  if(command == "Z UP")
  {
    digitalWrite(testPin, HIGH);
    zAmount = 1;
  }
  if(command == "Z DOWN")
  {
    digitalWrite(testPin, HIGH);
    zAmount = -1;
  }
  if(command == "PUMP UP")
  {
    zAmount = 1;
  }
  if(command == "PUMP DOWN")
  {
    zAmount = -1;
  }
  if(command == "STOP")
  {
    sendTestSerial("got STOP");
    
    digitalWrite(testPin, LOW);
    digitalWrite(pumpPin, LOW);
    xAmount = 0;
    yAmount = 0;
    zAmount = 0;

    moveX = false;
    moveY = false;
    moveZ = false;
  }
  if(command == "STOP X")
  {
    digitalWrite(testPin, LOW);
    xAmount = 0;
  }
  if(command == "STOP Y")
  {
    digitalWrite(testPin, LOW);
    yAmount = 0;
  }
  if(command == "STOP Z")
  {
    digitalWrite(testPin, LOW);
    zAmount = 0;
  }
  if(command == "PUMP")
  {
    isPumping = true;
    digitalWrite(pumpPin, HIGH);
  }
  if(command == "STOP PUMP")
  {
    isPumping = false;
    digitalWrite(pumpPin, LOW);
  }
  if(command == "SET X POSITION")
  {
    xPosition = stringToInt(params);
    gotStartingX = true;
  }
  if(command == "SET Y POSITION")
  {
    yPosition = stringToInt(params);
    gotStartingY = true;
  }
  if(command == "SET Z POSITION")
  {
    zPosition = stringToInt(params);
    gotStartingZ = true;
  }
  if(command == "sendToScale")
  {
    sendCOM(params);
    sendSerial("<root><command>comData</command><data>Arduino received: " + params + "</data></root>");
  }
  if(command == "motorDelay")
  {
    // just "delay 100" from the app, the app will send  "motorDelay"
    stepMicrosecondDelay = params.toInt();
    sendSerial("<root><command>comData</command><data>" + String(stepMicrosecondDelay) + "</data></root>");
  }
  if(command == "gotoX")
  {
    gotoX = stringToInt(params);
    moveX = true;
  }
  if(command == "gotoY")
  {
    gotoY = stringToInt(params);
    moveY = true;
  }
  if(command == "gotoZ")
  {
    gotoZ = stringToInt(params);
    moveZ = true;
  }
  if(command == "gotoXY")
  {
    sendTestSerial("got gotoXY");
    int commaIndex = params.indexOf(",");
    gotoX = stringToInt(params.substring(0, commaIndex));
    gotoY = stringToInt(params.substring(commaIndex + 1));
    moveX = true;
    moveY = true;
  }

  if(gotStartingX && gotStartingY && gotStartingZ) gotStartingValues = true;
  
}

/***************************************************** Loop COM Communication functions */

void doCOMLoopFunctions()
{
  String mChar;

  
  while(mSerial2.available())
  {
    mChar = String(char(mSerial2.read()));
    comIncomingStr += mChar;
  }
  
}

void sendCOM(String str)
{
  //mSerial2.write("Q");
  //mSerial2.write('\n');// r is definitely CR  n is LF
  //mSerial2.println("Q");
  //mSerial2.flush();
  mSerial2.println(str);
}

/***************************************************** Android functions */

void sendSerial(String str)
{
  // send to computer and tablet
  //if(useMSerial) mSerial.println(str);
  mSerial3.print(str);
  mSerial3.flush();
  //mSerialBuffer += str;
}

void sendTestSerial(String str)
{
  if(!useTestSerial) return;
  testSerial.println(str);
  
}

void setup()
{
  pinMode(testPin, OUTPUT);
  digitalWrite(testPin, LOW);
  pinMode(testFlipPin, OUTPUT);
  digitalWrite(testFlipPin, LOW);

  pinMode(dirXPin, OUTPUT);
  digitalWrite(dirXPin, LOW);
  pinMode(xPin, OUTPUT);
  digitalWrite(xPin, LOW);

  pinMode(dirYPin, OUTPUT);
  digitalWrite(dirYPin, LOW);
  pinMode(yPin, OUTPUT);
  digitalWrite(yPin, LOW);

  //pinMode(dirYPin2, OUTPUT);
  //digitalWrite(dirYPin2, LOW);
  //pinMode(yPin2, OUTPUT);
  //digitalWrite(yPin2, LOW);

  pinMode(dirZPin, OUTPUT);
  digitalWrite(dirZPin, LOW);
  pinMode(zPin, OUTPUT);
  digitalWrite(zPin, LOW);

  pinMode(pumpPin, OUTPUT);
  digitalWrite(pumpPin, LOW);

  mSerial2.begin(comBaudRate);

  if(useTestSerial)
  {
    testSerial.begin(9600);
    sendTestSerial("using test serial");
  }

  delay(2000);

  setupBlueToothConnection();
}

void loop()
{

  doSerialLoopFunctions();

  doCOMLoopFunctions();
  
  if(bluetoothIsConnected && getTimeSince(lastComTransmit) > comTransmitInterval)
  {
    lastComTransmit = millis();
    //sendSerial("<root><command>comData</command><data>hello COM device!!</data></root>");
    String comTempStr = comIncomingStr;
    if(comTempStr != "")
    {
      sendSerial("<root><command>comData</command><data>" + comTempStr + "</data></root>");
      comIncomingStr = "";
    }

    // send the main per frame package
    if(gotStartingValues)
    {
      String str = "<root><command>loop</command>";
      str += "<xPosition>" + intToString(xPosition) + "</xPosition>";
      str += "<yPosition>" + intToString(yPosition) + "</yPosition>";
      str += "<zPosition>" + intToString(zPosition) + "</zPosition>";
      if(isPumping)
      {
        str += "<pumping>1</pumping>";
      }
      else
      {
        str += "<pumping>0</pumping>";
      }
      str += "</root>";
      sendSerial(str);
    }
  }
  

  if(doFlipTest)
  {
    if(testFlip)
    {
      testFlip = false;
      digitalWrite(testFlipPin, LOW);
    }
    else
    {
      testFlip = true;
      digitalWrite(testFlipPin, HIGH);
    }

    delay(500);
  }

  if(xAmount != 0 || yAmount != 0 || zAmount != 0)
  {

    if(xAmount == 0)
    {
      digitalWrite(xPin, LOW);
    }
    else
    {
      if(xAmount < 0)
      {
        digitalWrite(dirXPin, LOW);
      }
      else
      {
        digitalWrite(dirXPin, HIGH);
      }
    }

    if(yAmount == 0)
    {
      digitalWrite(yPin, LOW);
      //digitalWrite(yPin2, LOW);
    }
    else
    {
      if(yAmount < 0)
      {
        digitalWrite(dirYPin, LOW);
        //digitalWrite(dirYPin2, LOW);
      }
      else
      {
        digitalWrite(dirYPin, HIGH);
        //digitalWrite(dirYPin2, HIGH);
      }
    }

    if(zAmount == 0)
    {
      digitalWrite(zPin, LOW);
      //digitalWrite(yPin2, LOW);
    }
    else
    {
      if(zAmount < 0)
      {
        digitalWrite(dirZPin, LOW);
      }
      else
      {
        digitalWrite(dirZPin, HIGH);
      }
    }

    if(xAmount != 0)
    {
      digitalWrite(xPin, HIGH);
      xPosition += xAmount;
    }
    if(yAmount != 0)
    {
      digitalWrite(yPin, HIGH);
      //digitalWrite(yPin2, HIGH);
      yPosition += yAmount;
    }
    if(zAmount != 0)
    {
      digitalWrite(zPin, HIGH);
      zPosition += zAmount;
    }
    delayMicroseconds(stepMicrosecondDelay);

    digitalWrite(xPin, LOW);
    digitalWrite(yPin, LOW);
    //digitalWrite(yPin2, LOW);
    digitalWrite(zPin, LOW);
    delayMicroseconds(stepMicrosecondDelay);
  }

  int subLoopCount = 0;
  while(subLoopCount < 1 && (moveX || moveY || moveZ))
  {
    subLoopCount++;
    
    if(moveX)
    {
      if(gotoX == xPosition)
      {
        moveX = false;
      }
      else
      {
        if(gotoX < xPosition)
        {
          digitalWrite(dirXPin, LOW);
          xPosition -= 1;
        }
        else
        {
          digitalWrite(dirXPin, HIGH);
          xPosition += 1;
        }
        digitalWrite(xPin, HIGH);
      }
    }
    if(moveY)
    {
      if(gotoY == yPosition)
      {
        moveY = false;
      }
      else
      {
        if(gotoY < yPosition)
        {
          digitalWrite(dirYPin, LOW);
          yPosition -= 1;
        }
        else
        {
          digitalWrite(dirYPin, HIGH);
          yPosition += 1;
        }
        digitalWrite(yPin, HIGH);
      }
    }
    if(moveZ)
    {
      if(gotoZ == zPosition)
      {
        moveZ = false;
      }
      else
      {
        if(gotoZ < zPosition)
        {
          digitalWrite(dirZPin, LOW);
          zPosition -= 1;
        }
        else
        {
          digitalWrite(dirZPin, HIGH);
          zPosition += 1;
        }
        digitalWrite(zPin, HIGH);
      }
    }

    delayMicroseconds(stepMicrosecondDelay);

    digitalWrite(xPin, LOW);
    digitalWrite(yPin, LOW);
    //digitalWrite(yPin2, LOW);
    digitalWrite(zPin, LOW);
    delayMicroseconds(stepMicrosecondDelay);
    
  }
  
}
