# Rand Barnett / Dot Trombone LLC Code Samples

## Rand's OOP (Object-Oriented Programming) History

I have a long history with OOP, but I chose to use a procedural style in PHP.

* 2004  Actionscript  Instantiated bullet MovieClips with custom collision detection code.
* 2005  Actionscript  Instantiated hand coded list boxes and scrollpanes.
* 2005  PHP  Wrote a class called "Note" for a webpage music synthesizer/sequencer.
* 2008  Actionscript 3  Got up to speed with Actionscript 3 while coding the Sid the Science Kid Website Game.
* 2009  Actionscript 3  Made a robust window GUI with a Windows XP skin to practice my OOP.  You can see this in the Dot Trombone App https://dottrombone.com/apps/dot-trombone-app
* 2010-2018  Actionscript 3  Continued to make advanced OOP projects in Adobe AIR for Learning.com and Belle & Wissell.
* 2018  PHP/Laravel  I was the primary developer on a large Enterprise Scheduling and POS web app.  This was my first MVC app.  I arguably made the mistake of putting a lot of business logic on the models so the functions could be reused between controllers.
* 2020  C#/Unity3d  Coded an Enterprise 2d business app in Unity.  The app was 100% code based.  Unity was chosen over Xamarin because we felt that the Unity platform would last longer than Xamarin.  The app was previously coded in Adobe AIR.  It turned out that Unity wasn't a good replacement for Adobe AIR and regretted not chosing Xamarin.
* 2021  C#.NET  Coded the Enterprise web app PVR Dashboard in ASP.NET to get practice coding ASP.NET in a real world application.  Also coded a WinForms app that sends models to an ASP.NET custom API.  Also coded my DT Virtual Accounts software using WinForms and LINQ.  https://dottrombone.com/apps/dt-virtual-accounts  The DT Virtual Accounts app can be downloaded on that page. 

As of Jan 2022, the winforms/dt_virtual_accounts is my most modern code.  It queries the models using LINQ and then saves all the models as JSON in a user selected file location.