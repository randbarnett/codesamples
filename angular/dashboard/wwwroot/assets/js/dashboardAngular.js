var angularApp;
angularApp = angular.module('dashboardAngularApp', []);
	
angularApp.filter('trustAsHtml',['$sce', function($sce)
{
    return function(text)
	{
      	return $sce.trustAsHtml(text);
	};
}]);

function pagesToArray(i) {
	var array = new Array();
	for (var a = 0; a < i; a++) {
		array.push(a + 1);
	}
	return array;
}
	
angularApp.controller('mainAngularController', ['$scope', '$http', function ($scope, $http) {
	$scope.clearPage = function () {
		$scope.showOrders = true;
		$scope.showDetailsDiv = false;
		$scope.showPVRDetailsDiv = false;
		$scope.showOrderWaiting = false;
		$scope.showOrderRecords = false;
		$scope.showPVRRecords = false;
		$scope.showPagination = false;
		$scope.showPageNotFound = true;
		$scope.singlePage = false;
		$scope.showUploadUpdates = false;
		$scope.showConsoleDiv = false;
		$scope.showEmptyResults = false;
		$scope.showUserManager = false;
		$scope.showUserManagerUserDetails = false;
		$scope.showUserLinker = false;
		$scope.showUserLinkerDetails = false;
		$scope.showUnvalidatedTally = false;
		$scope.$apply();
		sizeCalc();
	}

	$scope.getSinglePage = function (url) {
		$scope.clearPage();
		$scope.showPageNotFound = false;
		var params = {};
		$http.get(url, { params: params }).then(function (response) {
			$scope.singlePage = true;
			$scope.singlePageContent = response.data;
		});
	}

	$scope.showAdminPage = function (page, url, params) {
		$scope.clearPage();
		$scope.showPageNotFound = false;
		if (page == "uploadUpdates") {
			$scope.showUploadUpdates = true;
			$scope.showConsoleDiv = true;
			$scope.$apply();
			setTimeout(sizeCalc, 10);
		}
		if (page == "userManager") {
			$http.get(url, params).then(function(response) {
				$scope.showUserManager = true;
				$scope.Users = response.data.Users;
				$scope.editUserDetails = function ($event, isEdit) {
					var id = (isEdit ? $event.target.attributes.userId.value : -1);
					$scope.getUserManagerUserDetails(id);
				}
			}).finally(function () {
				setTimeout(sizeCalc, 10);
			});
		}
		if (page == "userLinker") {
			$http.get(url, params).then(function (response) {
				$scope.showUserLinker = true;
				$scope.UserLinks = response.data.UserLinks;
				$scope.editUserLink = function ($event, isEdit) {				
					var id = (isEdit ? $event.target.attributes.userLinkId.value : -1);
					$scope.getUserLinkDetails(id);
				};
				$scope.deleteUserLink = function (id, sellnum) {
					$scope.doDeleteUserLink(id, sellnum);
                }
				
			}).finally(function () {
				setTimeout(sizeCalc, 10);
			});
		}
	}

	$scope.getSearchResults = function (ajaxFile, params, pageType) {
		$scope.showOrders = true;
		$scope.showDetailsDiv = false;
		$scope.showPVRDetailsDiv = false;
		$scope.showOrderWaiting = true;
		$scope.showOrderRecords = false;
		$scope.showPVRRecords = false;
		$scope.showPagination = false;
		$scope.showPageNotFound = false;
		$scope.singlePage = false;
		$scope.showConsoleDiv = false;
		$scope.showUploadUpdates = false;
		$scope.showUserManager = false;
		$scope.showUserManagerUserDetails = false;
		$scope.showUserLinker = false;
		$scope.showUserLinkerDetails = false;
		$scope.showUnvalidatedTally = false;
		$scope.orderSorting = false;
		$scope.pvrSorting = false;

		params.page = 1;
		params.paginationClick = false;
		params.orderBy = false;
		params.isSortClick = false;//sortClick;
		params.isSearch = true;
		params.clearSearch = true;

		$http.get(ajaxFile, { params: params }).then(function (response) {
			$scope.showOrderWaiting = false;

			if (pageType == 'pvrs') {
				$scope.showPVRRecords = true;
				$scope.pvrSorting = true;
				$scope.showPagination = true;
				$scope.pagination = response.data.pagination;
				$scope.orderBy = response.data.orderBy;
				$scope.showPVREmptyResults = (response.data.PVRs.length == 0 ? true : false);
				$scope.pvrRecords = response.data.PVRs;
				setReportTotal("", response.data.reportRowCount);
				$scope.clickPVRDetails = function (id) {
					$scope.getPVRDetails(id);
					if (isMobile != undefined && isMobile != null && isMobile == true) showPane(3);
				}
				$scope.paginationClick = function (obj) {
					openPage(ajaxFile, obj.target.attributes.value.value, true);
				}
				$scope.sortClick = function (pFilter) {
					openPage(ajaxFile, 1, false, pFilter);
				}
			}
			else {
				// orders
				if (response.data.type == "unvalidated") {
					$scope.showUnvalidatedTally = true;
					$scope.unvalidatedTallyRecords = response.data.unvalidatedTallyRecords;
					$scope.unvalidatedTallyRecordsByContract = response.data.unvalidatedTallyRecordsByContract;
					$scope.searchUnvalidatedOrdersForSellnumAngular = function (sellNum) {
						searchUnvalidatedOrdersForSellnum(sellNum);
					}
					$scope.searchUnvalidatedOrdersForContractAngular = function (contract) {
						searchUnvalidatedOrdersForContract(contract);
					}
				}
				$scope.orderSorting = true;
				$scope.showOrderRecords = true;
				$scope.showPagination = (response.data.results.length == 0 ? false : true);
				$scope.showEmptyResults = (response.data.results.length == 0 ? true : false);
				$scope.orderRecords = response.data.results;
				$scope.paginationPages = pagesToArray(response.data.numberOfPages);
				$scope.currentPage = response.data.pageNumber;
				$scope.numberOfPages = response.data.numberOfPages;
				$scope.orderBy = response.data.orderBy;
				setReportTotal(response.data.reportTotal, response.data.reportRowCount);
				$scope.clickDetails = function (id) {
					$scope.getOrderDetails(id);
					if (isMobile != undefined && isMobile != null && isMobile == true) showPane(3);
				}
				$scope.paginationClick = function (obj) {
					openPage(ajaxFile, obj.target.attributes.value.value, true);
				}
				$scope.sortClick = function (pFilter) {
					openPage(ajaxFile, 1, false, pFilter);
				}
			}
		}).finally(function () {
			setTimeout(sizeCalc, 10);
		});
	}

	$scope.getResults = function (ajaxFile, pageNum, paginationClick, pSort, pageType) {
		$scope.showOrders = true;
		$scope.showDetailsDiv = false;
		$scope.showPVRDetailsDiv = false;
		$scope.showOrderWaiting = true;
		$scope.showOrderRecords = false;
		$scope.showPVRRecords = false;
		$scope.showPagination = false;
		$scope.showPageNotFound = false;
		$scope.singlePage = false;
		$scope.showConsoleDiv = false;
		$scope.showUploadUpdates = false;
		$scope.showUserManager = false;
		$scope.showUserManagerUserDetails = false;
		$scope.showUserLinker = false;
		$scope.showUserLinkerDetails = false;
		$scope.showUnvalidatedTally = false;
		$scope.orderSorting = false;
		$scope.pvrSorting = false;

		var params = new Object();
		params.page = pageNum;
		params.paginationClick = paginationClick;
		params.orderBy = pSort;
		params.isSortClick = false;//sortClick;
		params.isSearch = false;
		params.clearSearch = ((pSort == false && paginationClick == false) ? true : false);

		$http.get(ajaxFile, { params: params }).then(function (response) {
			$scope.showOrderWaiting = false;

			if (response.data.success == false) {
				$scope.singlePage = true;
				$scope.singlePageContent = response.data.message;
			}
			else {
				if (pageType == 'pvrs') {
					$scope.pvrSorting = true;
					$scope.showPVRRecords = true;
					$scope.pvrType = response.data.pvrType;
					$scope.showPagination = (response.data.PVRs.length == 0 ? false : true);
					$scope.paginationPages = pagesToArray(response.data.numberOfPages);
					$scope.currentPage = response.data.pageNumber;
					$scope.numberOfPages = response.data.numberOfPages;
					$scope.orderBy = response.data.orderBy;
					$scope.showPVREmptyResults = (response.data.PVRs.length == 0 ? true : false);
					$scope.pvrRecords = response.data.PVRs;
					setReportTotal("", response.data.reportRowCount);
					$scope.clickPVRDetails = function (id) {
						$scope.getPVRDetails(id);
						if (isMobile != undefined && isMobile != null && isMobile == true) showPane(3);
					}
					$scope.paginationClick = function (obj) {
						openPage(ajaxFile, obj.target.attributes.value.value, true);
					}
					$scope.sortClick = function (pFilter) {
						openPage(ajaxFile, 1, false, pFilter);
					}
				}
				else {
					// orders
					if (response.data.type == "unvalidated") {
						$scope.showUnvalidatedTally = true;
						$scope.unvalidatedTallyRecords = response.data.unvalidatedTallyRecords;
						$scope.unvalidatedTallyRecordsByContract = response.data.unvalidatedTallyRecordsByContract;
						$scope.searchUnvalidatedOrdersForSellnumAngular = function (sellNum) {
							searchUnvalidatedOrdersForSellnum(sellNum);
						}
						$scope.searchUnvalidatedOrdersForContractAngular = function (contract) {
							searchUnvalidatedOrdersForContract(contract);
						}
					}
					$scope.orderSorting = true;
					$scope.showOrderRecords = (response.data.results.length == 0 ? false : true);
					$scope.showPagination = (response.data.results.length == 0 ? false : true);
					$scope.showEmptyResults = (response.data.results.length == 0 ? true : false);
					$scope.orderRecords = response.data.results;
					
					$scope.paginationPages = pagesToArray(response.data.numberOfPages);
					$scope.currentPage = response.data.pageNumber;
					$scope.numberOfPages = response.data.numberOfPages;
					$scope.orderBy = response.data.orderBy;
					setReportTotal(response.data.reportTotal, response.data.reportRowCount);
					$scope.clickDetails = function (id) {
						$scope.getOrderDetails(id);
						if (isMobile != undefined && isMobile != null && isMobile == true) showPane(3);
					}
					$scope.paginationClick = function (obj) {
						openPage(ajaxFile, obj.target.attributes.value.value, true);
					}
					$scope.sortClick = function (pFilter) {
						openPage(ajaxFile, 1, false, pFilter);
					}
				}
            }

			

		}).finally(function () {
			setTimeout(sizeCalc, 10);
		});
	};

	$scope.getOrderDetails = function (id) {
		if ($('#order-tile-' + id).hasClass('order-tile-active')) return;

		$scope.showDetailsDiv = false;
		$scope.showDetailsWaiting = true;

		$('.order-tile').removeClass('order-tile-active');
		$('#order-tile-' + id).addClass('order-tile-active');

		$http.get('/api/order/details/' + id, {}).then(function (response) {
			$scope.orderDetails = response.data;
			$scope.showDetailsDiv = true;
			$scope.showDetailsWaiting = false;

			$scope.doOrderDetailsValidateClick = function (id, sonum) {
				lastValidateId = id;
				$http.get("/api/order/validate/" + id, {}).then(function (response) {
					if (response.data.success) {
						$scope.orderDetails.validated = true;
						$("#leftSideValidatedCheck-" + id).removeClass('ng-hide');
                    }
					
					$scope.orderDetails.validatedStr = response.data.message;
					
				});
			}

			$scope.doOrderDetailsRequestOrderRevisionClick = function (id, shipmentsOrOpenOrders) {
				showModal("modalRequestOrderRevision");
				$("#requestOrderRevisionMessage").attr("orderId", id);
				orderRevisionId = id;
			}
		});
	}

	$scope.getPVRDetails = function (id) {
		if ($('#pvr-tile-' + id).hasClass('pvr-tile-active')) return;

		$('.pvr-tile').removeClass('pvr-tile-active');
		$('#pvr-tile-' + id).addClass('pvr-tile-active');

		$scope.showDetailsDiv = false;
		$scope.showPVRDetailsDiv = false;
		$scope.showDetailsWaiting = true;
		$scope.showPVRDetailsPOs = false;

		$http.get('/api/pvr/details/' + id, {}).then(function (response) {
			var isActivePVR = false;
			if (response.data.pvrType == 'Active' || response.data.pvrType == 'Expired') isActivePVR = true;
			$scope.pvrType = response.data.pvrType;
			$scope.pvrDetailsRecord = response.data.pvr;
			$scope.pvrDetailsResponse = response.data;
			$scope.showPVRDetailsDiv = true;
			$scope.showDetailsWaiting = false;

			//$scope.pvrDetailsInternalNotes = response.data.record.addinfo;
			//$scope.pvrDetailsCustomerNotes = response.data.record.customernotes

			$scope.swapNotes = function (id, isInternal) {
				swapNotes(id, isInternal);
			}

			$scope.doNotesChange = function (id, isInternal) {
				doNotesChangeAngular(id, isInternal, 'Active');
			}

			$scope.doShowHidePVRDetailsPOs = function () {
				if ($scope.showPVRDetailsPOs) {
					$scope.showPVRDetailsPOs = false;
				}
				else {
					$scope.showPVRDetailsPOs = true;
				}
			}

			$scope.doCopyPVRClick = function (id, isEdit) {
				doCopyPVRClick(id, isEdit, isActivePVR);
			}

			$scope.doExtendClick = function (id) {
				if ($("#pvrnewdates" + id).css('display') != 'block') {
					$("#pvrnewdates" + id).show();
					$("#pvrnewdates" + id + ' .datepicker').pickadate({
						today: '',
						clear: '',
						close: ''
					});
				}
				else {
					$("#pvrnewdates" + id).hide();
				}
			}

			$scope.doExtendSubmitClick = function (id) {
				doExtendSubmitClick(id);
			}

			$scope.doTerminateClick = function (id) {
				doTerminateClick(id);
			}

			$scope.doTerminateSubmitClick = function (id) {
				doTerminateSubmitClick(id);
			}

			$scope.doApproveClick = function (id) {
				doApproveClick(id);
			}

			$scope.doDeclineClick = function (id) {
				doDeclineClick(id);
			}
		});
	}

	$scope.submitImportData = function () {
		$scope.consoleSuccessDiv = false;
		$scope.consoleProcessing = false;
		var fileType = $("#importDataFileType").val();
		if (fileType == "") {
			$scope.consoleText = "You forgot to select a file import type!";
			$scope.consoleSuccessDiv = true;
			$scope.consoleSuccess = false;
		}
		else {
			var file = document.getElementById("importDataFile").files[0];

			$scope.consoleProcessing = true;
			$scope.consoleText = "Submitting " + fileType + " data import.<br /><br />Please wait...";

			var fd = new FormData();
			fd.append("postedFile", file);
			fd.append("fileType", fileType);

			$http.post("/api/importer/dataimport", fd, { headers: { "Content-Type": undefined } }).then(function(response) {
				$scope.consoleProcessing = false;
				$scope.consoleSuccessDiv = true;
				$scope.consoleSuccess = response.data.success;
				$scope.consoleText = response.data.message;
			}, function (response) {
				$scope.consoleProcessing = false;
				$scope.consoleSuccessDiv = true;
				$scope.consoleSuccess = false;
				$scope.consoleText = response.data;
			});
		}
	}

	$scope.getUserManagerUserDetails = function (id) {
		$scope.consoleSuccessDiv = false;
		$scope.consoleProcessing = true;
		$scope.showConsoleDiv = true;
		$scope.consoleText = "Getting User Details...";

		$http.get("/api/usermanager/details/" + id, {}).then(function (response) {
			$scope.consoleProcessing = false;
			$scope.consoleSuccessDiv = false;
			$scope.consoleText = "";
			$scope.consoleSuccess = response.data.success;
			//$scope.consoleText = response.data.message;
			$scope.User = response.data.User;
			$scope.Type = response.data.Type;
			$scope.showUserManagerUserDetails = true;

			$scope.submitUserManagerUserDetails = function () {
				var newPassword = $("#userManagerEditUserPassword").val();
				if (newPassword == "") newPassword = '-default-';
				$scope.User.Password = newPassword;
				var user = JSON.stringify($scope.User);
				$scope.consoleSuccessDiv = false;
				$scope.consoleProcessing = true;
				$scope.showConsoleDiv = true;
				$scope.consoleText = "Saving user...";

				$http.post("/api/usermanager", user).then(function (response) {
					$scope.consoleProcessing = false;
					$scope.consoleSuccessDiv = true;
					$scope.consoleText = response.data.message;
					$scope.consoleSuccess = response.data.success;
					// refresh the left side too!
					if (response.data.success) {
						$scope.Users = response.data.Users;
						$scope.User = response.data.User;
						$scope.Type = response.data.Type;
					}

				}).catch(function (e) {
					$scope.consoleProcessing = false;
					$scope.consoleSuccessDiv = true;
					$scope.showConsoleDiv = true;
					$scope.consoleText = "Failed.";
					$scope.consoleSuccess = false;
				});
			}
		});
	}

	$scope.getUserLinkDetails = function (id) {
		$scope.consoleSuccessDiv = false;
		$scope.consoleProcessing = true;
		$scope.showConsoleDiv = true;
		$scope.consoleText = "Getting User Details...";

		$http.get("/api/usermanager/userlinker/details/" + id, {}).then(function (response) {
			$scope.consoleProcessing = false;
			$scope.consoleSuccessDiv = false;
			$scope.consoleText = "";
			$scope.consoleSuccess = response.data.success;
			//$scope.consoleText = response.data.message;
			$scope.UserLink = response.data.UserLink;
			$scope.Type = response.data.Type;
			$scope.showUserLinkerDetails = true;
			$scope.DistrictUsers = response.data.DistrictUsers;
			$scope.RegionalUsers = response.data.RegionalUsers;

			$scope.submitUserLinkerDetails = function () {
				var userLink = JSON.stringify($scope.UserLink);
				$scope.consoleSuccessDiv = false;
				$scope.consoleProcessing = true;
				$scope.showConsoleDiv = true;
				$scope.consoleText = "Saving user link...";

				$http.post("/api/usermanager/userlinker", userLink).then(function (response) {
					$scope.consoleProcessing = false;
					$scope.consoleSuccessDiv = true;
					$scope.consoleText = response.data.message;
					$scope.consoleSuccess = response.data.success;
					// refresh the left side too!
					if (response.data.success) {
						//$scope.UserLinks = response.data.UserLinks;
						$scope.UserLink = response.data.UserLink;
						$scope.UserLinks = response.data.UserLinks;
						$scope.DistrictUsers = response.data.DistrictUsers;
						$scope.RegionalUsers = response.data.RegionalUsers;
						$scope.Type = response.data.Type;
					}

				}).catch(function (e) {
					$scope.consoleProcessing = false;
					$scope.consoleSuccessDiv = true;
					$scope.showConsoleDiv = true;
					$scope.consoleText = "Failed.";
					$scope.consoleSuccess = false;
				});
			}
		});
	}

	$scope.doDeleteUserLink = function (id, sellnum) {
		if (confirm("Are you sure you want to delete this user link for sellnum " + sellnum + "?"))
		{
			$http({
					method: 'Delete',
					url: "/api/usermanager/userlinker/" + id
				}).then(function (response) {
					if (response.data.success) {
						$scope.UserLinks = response.data.UserLinks;
					}
					alert(response.data.Message);
				}, function (response) {
					alert("Delete not successful.");
				});
				
        }
    }
		
}]);