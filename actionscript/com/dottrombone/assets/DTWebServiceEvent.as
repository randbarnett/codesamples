package com.dottrombone.assets 
{
	/**
	 * ...
	 * @author Rand Barnett
	 */
	
	import flash.events.Event;
	
	public class DTWebServiceEvent extends Event
	{		
		public static const COMPLETE:String = "complete";
		public static const ERROR:String = "error";
		public var data:Object = new Object();
		public var xml:XML = null;
		
		public function DTWebServiceEvent(type:String) 
		{
			super(type);
		}
		
	}

}