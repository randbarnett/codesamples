package com.dottrombone.assets
{
    
    import flash.display.Stage;
    import flash.events.Event;
	import flash.events.EventDispatcher;
    import flash.events.KeyboardEvent;
	import com.dottrombone.assets.KeyEvent;
    
    /**
     * The Key class recreates functionality of
     * Key.isDown of ActionScript 1 and 2. Before using
     * Key.isDown, you first need to initialize the
     * Key class with a reference to the stage using
     * its Key.initialize() method. For key
     * codes use the flash.ui.Keyboard class.
     *
     * Usage:
     * Key.initialize(stage);
     * if (Key.isDown(Keyboard.LEFT)) {
     *    // Left key is being pressed
     * }
     */
    public class Key extends EventDispatcher
	{
        private static var initialized:Boolean = false;  // marks whether or not the class has been initialized
        private static var keysDown:Object = new Object();  // stores key codes of all keys pressed
        public static var instance:Key = null;
        /**
         * Initializes the key class creating assigning event
         * handlers to capture necessary key events from the stage
         */
		
		public static const SHIFT:uint = 16;
		public static const SPACE:uint = 32;
		public static const UP:uint = 38;
		public static const DOWN:uint = 40;
		public static const LEFT:uint = 37;
		public static const RIGHT:uint = 39;
		public static const BACKSPACE:uint = 8;
		public static const DELETE:uint = 46;
		public static const INSERT:uint = 45;
		public static const ENTER:uint = 13;
		public static const TAB:uint = 9;
		public static const CTRL:uint = 17;
		public static const ALT:uint = 18;
		 
		
        public static function initialize(stage:Stage) {
			if (instance == null) instance = new Key();
			if(stage!=null){
				if (!initialized) {
					// assign listeners for key presses and deactivation of the player
					stage.addEventListener(KeyboardEvent.KEY_DOWN, keyPressed);
					stage.addEventListener(KeyboardEvent.KEY_UP, keyReleased);
					stage.addEventListener(Event.DEACTIVATE, clearKeys);
					
					// mark initialization as true so redundant
					// calls do not reassign the event handlers
					initialized = true;
				}
			}
        }
        
        /**
         * Returns true or false if the key represented by the
         * keyCode passed is being pressed
         */
        public static function isDown(keyCode:uint):Boolean {
            if (!initialized) {
                // throw an error if isDown is used
                // prior to Key class initialization
                throw new Error("Key class has yet been initialized.");
            }
            return Boolean(keyCode in keysDown);
        }
        
        /**
         * Event handler for capturing keys being pressed
         */
        private static function keyPressed(event:KeyboardEvent):void {
            // create a property in keysDown with the name of the keyCode
            keysDown[event.keyCode] = true;
			//trace("keypress="+event.keyCode);
			var e:KeyEvent = new KeyEvent(KeyEvent.KEY_DOWN);
			e.keyCode = event.keyCode;
			instance.dispatchEvent(e);
        }
        
        /**
         * Event handler for capturing keys being released
         */
        private static function keyReleased(event:KeyboardEvent):void {
            if (event.keyCode in keysDown) {
                // delete the property in keysDown if it exists
                delete keysDown[event.keyCode];
				var e:KeyEvent = new KeyEvent(KeyEvent.KEY_UP);
				e.keyCode = event.keyCode;
				instance.dispatchEvent(e);
            }
        }
        
        /**
         * Event handler for Flash Player deactivation
         */
        private static function clearKeys(event:Event):void {
            // clear all keys in keysDown since the player cannot
            // detect keys being pressed or released when not focused
            keysDown = new Object();
        }
		
		public static function keyCode(str:String):uint
		{
			
			switch(str)
			{
				case 'a':
					return 65;
				case 'b':
					return 66;
				case 'c':
					return 67;
				case 'd':
					return 68;
				case 'e':
					return 69;
				case 'f':
					return 70;
				case 'g':
					return 71;
				case 'h':
					return 72;
				case 'i':
					return 73;
				case 'j':
					return 74;
				case 'k':
					return 75;
				case 'l':
					return 76;
				case 'm':
					return 77;
				case 'n':
					return 78;
				case 'o':
					return 79;
				case 'p':
					return 80;
				case 'q':
					return 81;
				case 'r':
					return 82;
				case 's':
					return 83;
				case 't':
					return 84;
				case 'u':
					return 85;
				case 'v':
					return 86;
				case 'w':
					return 87;
				case 'x':
					return 88;
				case 'y':
					return 89;
				case 'a':
					return 90;
				case 'shift':
					return SHIFT;
				case 'space':
					return SPACE;
				case 'up':
					return UP;
				case 'down':
					return DOWN;
				case 'left':
					return LEFT;
				case 'right':
					return RIGHT;
				case 'backspace':
					return BACKSPACE;
				case 'delete':
					return DELETE;
				case 'insert':
					return INSERT;
				case '-':
				case '_':
					return 189;
				case '=':
				case '+':
					return 187;
				case '[':
					return 219;
				case ']':
					return 221;
				case '\\':
					return 220;
				case ';':
				case ':':
					return 186;
				case '"':
				case "'":
					return 222;
				case ',':
				case '<':
					return 188;
				case '.':
				case '>':
					return 190;
				case '/':
				case '?':
					return 191;
				case 'enter':
					return ENTER;
					
			}
			
			var i:int = parseInt(str, 10);
			
			if (str.indexOf("keypad") == 0)
			{
				i = parseInt(str.substr(6, 1), 10);
				return 96 + i;
			}
			
			
			if (i >= 0 && i < 10)
			{
				return 48 + i;
			}
			
			return 0;
		}
    }
}