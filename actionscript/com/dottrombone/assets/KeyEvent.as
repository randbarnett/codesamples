package com.dottrombone.assets 
{
	/**
	 * ...
	 * @author Rand Barnett
	 */
	
	import flash.events.Event;
	
	public class KeyEvent extends Event
	{
		
		public static const KEY_DOWN:String = "keyDown";
		public static const KEY_UP:String = "keyUp";
		public var keyCode:uint = 0;
		
		public function KeyEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false):void
		{
			super(type, bubbles, cancelable);
		}
		
	}

}