package com.dottrombone.assets 
{
	/**
	 * ...
	 * @author Rand Barnett
	 */
	
	import flash.errors.IOError;
	import flash.events.*;
	import flash.events.EventDispatcher;
	import com.dottrombone.assets.DTWebServiceEvent;
	import flash.net.URLRequestHeader;
	import flash.net.URLVariables;
	import flash.net.URLRequestMethod;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLLoader;
	import com.dottrombone.objects.Tracer;
	import com.dottrombone.assets.Time;
	
	public class DTWebService extends EventDispatcher
	{
		public static var sessionCookie:String = "";
		public var serviceAddress:String = "";
		private var lastResponseStatus:int;
		private var requestArray:Array = new Array();// array of all ongoing requests, used for error reporting
		public var parseResponse:Boolean = true;
		public var useJSONtoSend:Boolean = false;
		public var useJSONtoReceive:Boolean = true;
		public var useXMLtoReceive:Boolean = true;// only if useJSONtoReceive is false;
		public var useCookies:Boolean = true;
		
		public function DTWebService(address:String = "") 
		{
			serviceAddress = address;
		}

		public function sendRequest(method:String = "GET", params:Object = null)
		{
			Tracer.instance.doTrace(Time.currentReadableTime + " DTWebService sendRequest " + serviceAddress);
			
			var urlReq:URLRequest;
			
			var k:String;
			
			
			if (method.toLowerCase() == "get")
			{			
				// parse params
				var str:String = "";
				var array:Array = new Array();
				for (k in params)
				{
					array.push(k + "=" + escape(params[k]));
				}
				
				if (array.length > 0)
				{
					str = array.join("&");
					str = "?" + str;
				}
				
				urlReq = new URLRequest(serviceAddress + str);
				urlReq.method = URLRequestMethod.GET;
			}
			else
			{
				// POST
				urlReq = new URLRequest(serviceAddress);
				urlReq.method = URLRequestMethod.POST;
				/* encode and prepare the data for transfer */	
				var urlVars:URLVariables = new URLVariables();
			
				if (useJSONtoSend)
				{
					urlReq.data = JSON.stringify(params);
					//var hdr:URLRequestHeader = new URLRequestHeader("Content-type", "application/json");
					//urlReq.requestHeaders.push(hdr);
					urlReq.contentType = "application/json";
				}
				else
				{
					if (params != null)
					{
						for (k in params)
						{
							urlVars[k] = params[k];
						}
					}
					urlReq.data = urlVars;
				}
			}
			
			urlReq.manageCookies = useCookies;
			if (false && sessionCookie != "")
			{
				var header:URLRequestHeader = new URLRequestHeader("cookie", "PHPSESSID=" + sessionCookie);
				urlReq.requestHeaders.push(header);
			}
			
			/* make the actual call */
			lastResponseStatus = 0;
			var urlLoader:URLLoader	= new URLLoader();
			
			if(!useJSONtoSend) urlLoader.dataFormat = URLLoaderDataFormat.TEXT;
			urlLoader.addEventListener(IOErrorEvent.IO_ERROR, doIOError);
			urlLoader.addEventListener(flash.events.HTTPStatusEvent.HTTP_RESPONSE_STATUS, doResponseStatus);
			urlLoader.addEventListener(Event.COMPLETE, sendCompleteEvent);
			requestArray.push([urlLoader, serviceAddress]);
			urlLoader.load(urlReq);
		}
		
		private function doIOError(e:IOErrorEvent):void
		{
			Tracer.instance.doTrace("webservice io error " + e.text);
			//ErrorLogs.instance.doTrace("io error:");
			//ErrorLogs.instance.doTrace("webservice io error url: " + getRequestUrl(e.target as URLLoader));
			removeRequest((e.currentTarget as URLLoader));
			var e2:DTWebServiceEvent = new DTWebServiceEvent(DTWebServiceEvent.ERROR);
			dispatchEvent(e2);
		}
		
		private function doResponseStatus(e:HTTPStatusEvent):void
		{
			Tracer.instance.doTrace("webservice response status " + e.toString());
			lastResponseStatus = e.status;
			if (lastResponseStatus != 200)
			{
				//ErrorLogs.instance.doTrace("webservice response " + lastResponseStatus + " url: " + getRequestUrl((e.currentTarget as URLLoader)));
			}
		}
		
		private function getRequestUrl(loader:URLLoader):String
		{
			for (var a:int = 0; a < requestArray.length; a++)
			{
				if ((requestArray[a][0] as URLLoader) == loader)
				{
					return requestArray[a][1];
				}
			}
			return "";
		}
		
		private function removeRequest(loader:URLLoader):void
		{
			//return;
			var urlLoader:URLLoader;
			for (var a:int = 0; a < requestArray.length; a++)
			{
				urlLoader = requestArray[a][0];
				if (urlLoader == loader)
				{
					urlLoader.removeEventListener(IOErrorEvent.IO_ERROR, doIOError);
					urlLoader.removeEventListener(flash.events.HTTPStatusEvent.HTTP_RESPONSE_STATUS, doResponseStatus);
					urlLoader.removeEventListener(Event.COMPLETE, sendCompleteEvent);
					requestArray.splice(a, 1);
					a--;
				}
			}
		}

		public function sendCompleteEvent(evt:Event):void 
		{
			//trace(evt.target.data);
			
			removeRequest((evt.currentTarget as URLLoader));
			
			/* send successful */
			trace('success');
			var e:DTWebServiceEvent = new DTWebServiceEvent(DTWebServiceEvent.COMPLETE);
			var data:Object = new Object();
			Tracer.instance.doTrace("DTWebService Complete Event");
			Tracer.instance.doTrace("Response Status =" + lastResponseStatus.toString());
			if (lastResponseStatus != 200)
			{
				var e2:DTWebServiceEvent = new DTWebServiceEvent(DTWebServiceEvent.ERROR);
				dispatchEvent(e2);
			}
			else
			{
				trace("JSON to parse, this is in DTWebService");
				trace(evt.target.data);
				if (parseResponse)
				{
					if (useJSONtoReceive)
					{
						data = JSON.parse(evt.target.data);
					}
					else if (useXMLtoReceive)
					{
						e.xml = new XML(evt.target.data);
					}
				}
				e.data = data;
				dispatchEvent(e);
			}
			
			
			
		}
		
		public function destroy():void
		{
			while (requestArray.length > 0)
			{
				removeRequest(requestArray[0][0]);
			}
		}
		
	}

}