﻿package com.dottrombone.assets{
	
	import flash.display.*;
	import flash.geom.Point;
	import com.dottrombone.objects.SpriteExtended;
	
	public class CollisionDetection{
			
		public static const rd:Number = 180/Math.PI;
		public static const dg:Number = Math.PI/180;
		public static var padding:Number=0;
		
		public static function detectCollision(dragging:Sprite,x1:Number,y1:Number,objects:Array,reboundType:String="rebound"):Array{
			y1 *= -1;//standard position
			var degrees:Number = Math.atan2(y1,x1)*rd;
			var speed:Number = Math.sqrt((y1 * y1) + (x1 * x1));
			var returnArray:Array = detectCollisionDegrees(dragging,degrees,speed,objects,reboundType);
			return(returnArray);
		}
		
		public static function detectCollisionDegrees(dragging:Sprite,degrees:Number,speed:Number,objects:Array,reboundType:String="rebound"):Array{
			
			if(degrees>360){
				do{
					degrees-=360;
				}while(degrees>360);
			}
			if(degrees<0){
				do{
					degrees+=360;
				}while(degrees<0);
			}
			
			
			var x1:Number;
			var y1:Number;
			
			var oldX:Number=dragging.x;
			var oldY:Number=dragging.y;
			var absX=Math.abs(Math.cos(degrees*dg)*speed);
			var absY=Math.abs(Math.sin(degrees*dg)*speed);
			
			var collision:Array=getCollision(dragging,degrees,speed,objects);
			//trace("first collision");
			if(collision[0]==true){
				
				dragging.x+=collision[1][1];
				dragging.y+=collision[1][2];
				
				/*
				var hitting:Boolean=isHitTest(dragging,collision[1][0],objects);
				if(hitting){
					//trace("hitting");
					if(side==1||side==3){
						dragging.y-=collision[1][2];
					}
					else{
						dragging.x-=collision[1][1];
					}
				}
				*/
				if(reboundType=="slide"){
					
					var side:uint=collision[1][5];
					
					
					//set new speed
					speed-=collision[1][3];
					
					x1=Math.abs(Math.cos(degrees*dg)*collision[1][3]);
					y1=Math.abs(Math.sin(degrees*dg)*collision[1][3]);
					
					if(side==1||side==3){
						speed=absY-y1;
					}
					else{
						speed=absX-x1;
					}
					
					
					var quad:uint=findQuad(degrees);
					var slide:uint=0;
					switch(side){
						case 1:
							if(quad==1){
								slide=2;
							}
							else{
								slide=4;
							}
							break;
						case 2:
							if(quad==1){
								slide=1;
							}
							else{
								slide=3;
							}
							break;
						case 3:
							if(quad==2){
								slide=2;
							}
							else{
								slide=4;
							}
							break;
						case 4:
							if(quad==3){
								slide=3;
							}
							else{
								slide=1;
							}
							break;
						default:
							trace("broken slide");
					}
					
					switch(slide){
						case 1:
							degrees=0;
							break;
						case 2:
							degrees=90;
							break;
						case 3:
							degrees=180;
							break;
						case 4:
							degrees=270;
							break;
					}
					var collision2:Array=getCollision(dragging,degrees,speed,objects,true);
					if(collision2[0]==true){
						//trace("2nd collision");
						
						if(slide==1||slide==3){
							dragging.x+=collision2[1][1];
						}
						else{
							dragging.y+=collision2[1][2];
						}
						
					}
					else{
						
						//no 2nd hit
						x1=Math.cos(degrees*dg)*speed;
						y1=(Math.sin(degrees*dg)*speed)*-1;
						
						dragging.x+=x1;
						dragging.y+=y1;
						
						
						var bM:Object=dragging.getBounds(dragging.parent);//boundsMoving
						var bS:Object=collision[1][0].getBounds(dragging.parent);//boundsStatic
						var g:Number=0;
						var hit:Boolean=false;
						x1=0;
						y1=0;
						switch(slide){
							case 1:
								if(bM.x>bS.x+bS.width){
									g=bM.x-(bS.x+bS.width);
									x1=-1*g;	
									hit=true;
								}
								break;
							case 2:
								if(bM.y+bM.height<bS.y){
									g=bS.y-(bM.y+bM.height);
									y1=g;
									hit=true;
								}
								break;
							case 3:
								if(bM.x+bM.width<bS.x){
									g=bS.x-(bM.x+bM.width);
									x1=g;
									hit=true;
								}
								break;
							case 4:
								if(bM.y>bS.y+bS.height){
									g=bM.y-(bS.y+bS.height);
									y1=-1*g;
									hit=true;
								}
								break;
						}
						/*
						if(hit){
							trace("hit");
							var d:Number=0;
							switch(side){
								case 1:
									d=0;
									break;
								case 2:
									d=90;
									break;
								case 3:
									d=180;
									break;
								case 4:
									d=270;
									break;
							}
							var collision3:Array=getCollision(dragging,d,.01,objects);
							if(collision3[0]){
								
								dragging.x+=collision3[1][1];
								dragging.y+=collision3[1][2];
								
							}
						}
									*/
						//collision2[1][3]-=g;
						
						
						dragging.x+=x1;
						dragging.y+=y1;
						
					}
				}
			}
			else{
				//didn't hit anything
				x1=Math.cos(degrees*dg)*speed;
				y1=(Math.sin(degrees*dg)*speed)*-1;
				
				dragging.x+=x1;
				dragging.y+=y1;
				
			}
			if(collision[0]==true&&reboundType=="slide"){
				return(collision.concat(collision2));
			}
			else{
				return(collision);
			}
		}
		
		public static function isHitTest(dragging:Sprite,hitting:Sprite,objects:Array):Boolean{
			var obj:Sprite;
			for(var a=0;a<objects.length;a++){
				obj=objects[a];
				if(obj!=dragging&&obj!=hitting){
					if(obj.hitTestObject(dragging)){
						return(true);
					}
				}
			}
			return(false);
		}
		public static function findQuad(tD:Number):uint{
			var quad:uint=0;
			if(tD<0){
				tD+=360;
			}
			if(tD<90){
				quad=1;
			}
			else if(tD<180){
				quad=2;
			}
			else if(tD<270){
				quad=3;
			}
			else{
				quad=4;
			}
			return(quad);
		}
		
		public static function getCollision(dragging:Sprite,degrees:Number,speed:Number,objects:Array,sliding:Boolean=false):Array{
			
			var bounds:Object=dragging.getBounds(dragging.parent);
			if(dragging.getChildByName("inside")!=null){
				bounds=dragging.getChildByName("inside").getBounds(dragging.parent);
			}
			var xN:Number;
			var yN:Number;
			var wN:Number;
			var hN:Number;			
			
			var tD:Number=degrees;
			var tV:Number;
			
			var hitArray:Array=new Array();
			var tempArray:Array;
			var a:Number;
			var b:Number;
			var c:Number;
			var g:Number;
			var quad:uint;
			var side:uint;
			var xB:Number;
			var yB:Number;
			var wB:Number;
			var hB:Number;
			var hit:Boolean;
			var low:Number;
			var high:Number;
			var tD2:Number;
			var x1:Number;
			var y1:Number;
			var rebound:Number;
			var q:uint;
			var bounds2:Object;
			var temper:Number=.3;
			var x2:Number;
			var y2:Number;

    
			for (var p:Number=0;p<objects.length; p++)
			{
				if (objects[p] != dragging)
				{
					
					tV=speed;
					
					xN=bounds.x+(bounds.width/2);
					yN=bounds.y+(bounds.height/2);
					wN=bounds.width/2;
					hN=bounds.height/2;
					
					x2=Math.cos((tD-180)*dg)*temper;
					y2=(Math.sin((tD-180)*dg)*temper)*-1;
					
					xN+=x2;
					yN+=y2;
					
					tV+=temper;
					
					bounds2=objects[p].getBounds(dragging.parent);
					if(objects[p].getChildByName("inside")!=null){
						bounds2=objects[p].getChildByName("inside").getBounds(dragging.parent);
					}
					xB = bounds2.x+(bounds2.width/2);
					yB = bounds2.y+(bounds2.height/2);
					wB = (bounds2.width/2)+(2*padding);
					hB = (bounds2.height/2)+(2*padding);
					
					if(sliding){
						if(tD==0){
							//side=1;
						}
						if(tD==90){
							//side=2;
						}
						if(tD==180||tD==-180){
							//side=3;
						}
						if(tD==270||tD==-90){
							//side=4;
						}
					}
					
					hit=true;
					if((bounds2.x+bounds2.width+tV)<bounds.x)hit=false;
					if((bounds2.y+bounds2.height+tV)<bounds.y)hit=false;
					if((bounds.x+bounds.width+tV)<bounds2.x)hit=false;
					if((bounds.y+bounds.height+tV)<bounds2.y)hit=false;
					
					
					
					if (hit)
					{
						quad = 0;
						side = 0;
						
											
						
						if (yB + hB < yN - hN && xN + wN < xB - wB)
						{
							quad = 1;
						} // end if
						if (yB + hB < yN - hN && xB + wB < xN - wN)
						{
							quad = 2;
						} // end if
						if (yN + hN < yB - hB && xB + wB < xN - wN)
						{
							quad = 3;
						} // end if
						if (yN + hN < yB - hB && xN + wN < xB - wB)
						{
							quad = 4;
						} // end if
						
						
						//trace(quad);
						
						if (quad == 0)
						{
							if (xN + wN <= xB - wB)
							{
								side = 1;
							} // end if
							if (yB + hB <= yN - hN)
							{
								side = 2;
							} // end if
							if (xB + wB < xN - wN)
							{
								side = 3;
							} // end if
							if (yN + hN <= yB - hB)
							{
								side = 4;
							} // end if
						} // end if
						if (quad == 1)
						{
							//xN-=.0001;
							//yN+=.0001;
							//trace ("quad 1");
							a=(yN-hN)-(yB+hB);
							b=(xB-wB)-(xN+wN);
							tD2=Math.atan2(a,b)*rd;
							if(tD==tD2){
								
								tD-=1;
								if(tD<=0){
									tD+=2;
								}
							}
							if(tD<tD2){
								side=2;
							}
							else{
								side=1;
							}
						} // end if
						if (quad == 2)
						{
							//xN+=.0001;
							//yN+=.0001;
							//trace ("quad 2");
							a=(yN-hN)-(yB+hB);
							b=(xB+wB)-(xN-wN);
							tD2=Math.atan2(a,b)*rd;
							if(tD==tD2){
								
								tD-=1;
								if(tD<=90){
									tD+=2;
								}
							}
							if(tD<tD2){
								side=3;
							}
							else{
								side=2;
							}
							
						} // end if
						if (quad == 3)
						{
							//xN+=.0001;
							//yN-=.0001;
							
							a=(yB-hB)-(yN+hN);
							b=(xB+wB)-(xN-wN);
							tD2=Math.atan2(a*-1,b)*rd;
							if(tD2<=0){
								tD2+=360;							}
							
							if(tD==tD2){
								
								tD-=1;
								if(tD<=180){
									tD+=2;
								}
							}
							if(tD2<tD){
								side=3;
							}
							else{
								side=4;
							}
							
							
						} // end if
						if (quad == 4)
						{
							//xN-=.0001;
							//yN-=.0001;
							//trace ("quad 4");
							a=(yN+hN)-(yB-hB);
							b=(xB-wB)-(xN+wN);
							tD2=(Math.atan2(a,b)*rd)+360;
							if(tD==tD2){
								tD-=1;
								if(tD<=270){
									tD+=2;
								}
							}
							if(tD<tD2){
								side=1;
							}
							else{
								side=4;
							}
						} // end if
						
						
						if(objects[p].name=="object4"){
							//trace("side",side,tD,quad);
						}
						
						
						
						if (side == 1)
						{
							
							//trace ("side 1");
							a = yN - hN - (yB + hB);
							b = (xN + wN - (xB - wB)) * -1;
							low = Math.atan2(a, b) * rd;
							a = yN + hN - (yB - hB);
							high = Math.atan2(a, b) * rd;
							tD2 = tD;
							if (tD2 > 180)
							{
								tD2 = tD2 - 360;
							} // end if
							if (low < tD2 && tD2 < high)
							{
								g = 1 / Math.cos(tD * dg) * b;
								if (tV >= g)
								{
									g = 1 / Math.cos(tD*dg)*(b+x2);
																		
									x1=Math.cos(tD * dg) * (g);
									y1=Math.sin(tD * dg) * (g);
									if (tD2 < 0)
									{
										rebound= tD - (180 - 2 * (tD2 * -1));
									}
									else
									{
										rebound= tD + (180 - 2 * tD);
									} // end if
									tempArray=new Array(objects[p],x1,y1,g,rebound,side);
									hitArray.push(tempArray);
								} // end if
							} // end if
						} // end else if
						
						if (side == 2)
						{
							
							//trace ("side 2");
							a = yN - hN - (yB + hB);
							b = (xN - wN - (xB + wB)) * -1;
							low = Math.atan2(a, b) * rd;
							b = (xN + wN - (xB - wB)) * -1;
							high = Math.atan2(a, b) * rd;
							
							if (low < tD && tD < high)
							{
								
								g = 1 / Math.sin(tD * dg) * a;
								if (tV >= g)
								{
									g = 1 / Math.sin(tD*dg)*(a-y2);
									x1= Math.cos(tD * dg) * (g);
									y1= (Math.sin(tD * dg) * (g))*-1;
									if (tD < 90)
									{
										rebound= tD - 2 * tD;
									}
									else
									{
										rebound= tD + (180 - 2 * (tD - 90));
									} // end if
									tempArray=new Array(objects[p],x1,y1,g,rebound,side);
									hitArray.push(tempArray);
								} // end if
							} // end if
						} // end else if
						if (side == 3)
						{
							
							//trace ("side 3");
							a = yN + hN - (yB - hB);
							b = (xN - wN - (xB + wB)) * -1;
							low = Math.atan2(a, b) * rd;
							if (low < 0)
							{
								low = low + 360;
							} // end if
							a = yN - hN - (yB + hB);
							high = Math.atan2(a, b) * rd;
							if (high < 0)
							{
								high = high + 360;
							} // end if
							
							
							if (low <= tD && tD < high)
							{
								
								g = 1 / Math.cos(tD * dg) * b;
								if (tV >= g)
								{
									g= 1 / Math.cos(tD*dg) * (b+x2);
									x1= Math.cos(tD * dg) * (g);
									y1= Math.sin(tD * dg) * (g);
									
									if (tD < 180)
									{
										rebound= tD - (180 - 2 * (180 - tD));
									}
									else
									{
										rebound= tD + (180 - 2 * (tD - 180));
									} // end if
									
									if(quad==3){
										y1-=1;
									}
									
									tempArray=new Array(objects[p],x1,y1,g,rebound,side);
									hitArray.push(tempArray);
								} // end if
							} // end if
						} // end else if
						if (side == 4)
						{
							
							//trace ("side 4");
							a = yN + hN - (yB - hB);
							b = (xN + wN - (xB - wB)) * -1;
							low = Math.atan2(a, b) * rd;
							b = (xN - wN - (xB + wB)) * -1;
							high = Math.atan2(a, b) * rd;
							
							if (low <= 0)
							{
								low = low + 360;
							} // end if
							if (high <= 0)
							{
								high = high + 360;
							} // end if
							
							
							
							
							if (low < tD && tD < high)
							{
								
								g = 1 / Math.sin(tD * dg) * a;
								if (tV >= g)
								{
									g=1/Math.sin(tD*dg)* (a-y2);
									x1= Math.cos(tD * dg) * (g);
									y1= (Math.sin(tD * dg) * (g))*-1;
									
									if (tD < 270)
									{
										rebound= tD - (180 - 2 * (270 - tD));
										
									} // end if
									else{
										rebound= tD + (180 - 2 * (tD - 270));
									}
									
									tempArray=new Array(objects[p],x1,y1,g,rebound,side);
									hitArray.push(tempArray);
								} // end if
							} // end if
						} // end if
					} // end if
				} // end if
			} // end of for
			var winner:Number;
			var winNum:Number;
			var n:Array;
			if (hitArray.length > 0)
			{
				winner = 100000;
				for (p = 0; p < hitArray.length; p++)
				{
					if(hitArray[p][3]<winner)
					{
						winner = hitArray[p][3]
						winNum = p;
					} // end if
				} // end of for
				n=new Array(true,hitArray[winNum]);
				return(n);
			} // end if
			else{
				n=new Array(false,false);
				return(n);
			}
		}
		public static function detectCollisionCircle(dragging:Sprite, degrees:Number, speed:Number, objects:Array, rectObjects:Array = null):Array
		{
			/*
			
			if(degrees<0){
				do{
					degrees+=360;
				}while(degrees<0);
			}
			if(degrees>360){
				do{
					degrees-=360;
				}while(degrees>360);
			}
			
			var oldX:Number=dragging.x;
			var oldY:Number=dragging.y;
			var collision:Array=getCollisionCircle(dragging,degrees,speed,objects);
			var collision2:Array=getCollision(dragging,degrees,speed,rectObjects);
			
			var hitWall:Boolean=false;
			var hitCircle:Boolean=false;
			if(collision2[0]==true){
				if(collision[0]==true){
					if(collision2[1][3]<=collision[1][3]){
						//hit wall
						hitWall=true;
					}
					else{
						hitCircle=true;
					}
				}
				else{
					//hit wall
					hitWall=true;
				}
			}
			else{
				if(collision[0]==true){
					hitCircle=true;
				}
			}
			
			if(hitWall){
								
				dragging.x+=collision2[1][1];
				dragging.y-=collision2[1][2];
				
				dragging.degrees=collision2[1][4];
				return(collision2);
			}
			else if(hitCircle){
				
				dragging.x+=collision[1][1];
				dragging.y-=collision[1][2];
				
				//dragging.degrees=collision[1][4];
				var circle:Sprite=collision[1][0];//trace("degrees",degrees);
				if(circle.speed!=undefined){
					var x1:Number=dragging.x;//&&circle.speed>0
					var y1:Number=dragging.y;
					var dx:Number=circle.x-x1;
					var dy:Number=circle.y-y1;
					var dist=Math.sqrt((dx*dx)+(dy*dy));
					var normalX=dx/dist;
					var normalY=dy/dist;
					var xvel1:Number=Math.cos(degrees*dg)*speed;
					var yvel1:Number=(Math.sin(degrees*dg)*speed)*-1;
					var xvel2:Number=Math.cos(circle.degrees*dg)*circle.speed;
					var yvel2:Number=(Math.sin(circle.degrees*dg)*circle.speed)*-1;
					var dVector=((xvel1-xvel2)*normalX)+((yvel1-yvel2)*normalY);
					var dvx=dVector*normalX;
					var dvy=dVector*normalY;
					
					xvel1-=dvx;
					yvel1-=dvy;
					xvel2+=dvx;
					yvel2+=dvy;
					
					yvel1*=-1;
					yvel2*=-1;
					
					var d:Number=Math.atan2(yvel1,xvel1)*rd;
					dragging.degrees=d;
					var s:Number=Math.sqrt((yvel1*yvel1)+(xvel1*xvel1));
					dragging.speed=s;
					var d2:Number=Math.atan2(yvel2,xvel2)*rd;
					circle.degrees=d2;
					s=Math.sqrt((yvel2*yvel2)+(xvel2*xvel2));
					circle.speed=s;
					//trace(d,d2);
				}
				else{
					dragging.degrees=collision[1][4];
				}
				return(collision);
			}
			else{
				
				dragging.x+=Math.cos(degrees*dg)*speed;
				dragging.y-=Math.sin(degrees*dg)*speed;
				
				return(new Array(false,false));
			}
			*/
			return new Array();
		}
				
		public static function getCollisionCircle(dragging:Sprite, degrees:Number, speed:Number, objects:Array):Array
		{
			return new Array();
			/*
			var xN:Number;
			var yN:Number;
			var rN:Number;
			var xC:Number;
			var yC:Number;
			var rC:Number;
			
			var a:Number;
			var b:Number;
			var c:Number;
			var d:Number;
			var f:Number;
			var E:Number;
			var a1:Number;
			var b1:Number;
			var C1:Number;
			var D1:Number;
			var D:Number;
			var low:Number;
			var high:Number;
			var tD:Number;
			var tD2:Number;
			var D2:Number;
			var E1:Number;
			var e:Number;
			var F1:Number;
			var G1:Number;
			var g:Number;
			var tV:Number;
			
			var x1:Number;
			var y1:Number;
			var rebound:Number;
			
			var obj:Sprite;
			var bounds:Object;
			var bounds2:Object;
			
			var hitArray:Array=new Array();
			var array:Array;
			
			var temper:Number=.3;
			
			bounds=dragging.getBounds(dragging.parent);
			if(dragging.getChildByName("inside")!=null){
				bounds=dragging.getChildByName("inside").getBounds(dragging.parent);
			}
			
			xN=bounds.x+(bounds.width/2);
			yN=bounds.y+(bounds.height/2);
			rN=bounds.width/2;
			
			speed+=temper;
			
			xN+=(Math.cos((180-degrees)*dg)*temper);
			yN+=(Math.sin((180-degrees)*dg)*temper)*-1;
			
			
			for(var q:uint=0;q<objects.length;q++){
				obj=objects[q];
				if(obj!=dragging){
					bounds2=obj.getBounds(dragging.parent);
					if(obj.getChildByName("inside")!=null){
						bounds2=obj.getChildByName("inside").getBounds(dragging.parent);
					}
					xC=bounds2.x+(bounds2.width/2);
					yC=bounds2.y+(bounds2.height/2);
					rC=bounds2.width/2;
					
					a=yN-yC;
					b=xC-xN;
					a1=Math.abs(a);
					b1=Math.abs(b);
					c=Math.sqrt(Math.pow(a1,2)+Math.pow(b1,2));
					C1=Math.atan2(a,b)*rd;
					if(C1<0){
						C1+=360;
					}
					D=degrees-C1;
					
					D1=Math.abs(D);
					
					if(D1>180){
						D1-=360;
					}
					D1=Math.abs(D1);
					
					if(D1<90){
						d=Math.sin(D1*dg)*c;
						if(d<rN+rC){
							//in range
							
							e=Math.sqrt(Math.pow(c,2)-Math.pow(d,2));
							
							f=Math.sqrt(Math.pow(rN+rC,2)-Math.pow(d,2));
							
							g=e-f;
							
							
							if(speed>g){
								//hit
								//trace("hit");
								g+=temper;
								x1=Math.cos(degrees*dg)*(g);
								y1=Math.sin(degrees*dg)*(g);
								F1=Math.asin(d/(rN+rC))*rd;
								E=180-F1;
								if(D<0){
									rebound=degrees-180+(2*F1);
								}
								else{
									rebound=degrees+180-(2*F1);
								}
								if(rebound>360){
									do{
									   rebound-=360;
									}while(rebound>360);
								}
								if(rebound<0){
									do{
										rebound+=360;
									}while(rebound<0);
								}
									 
								array=new Array(obj,x1,y1,g-temper,rebound);
								
								//trace(degrees,rebound);
								hitArray.push(array);
							}
						}
						
					}
				}
			}
			if(hitArray.length>0){
				
				var winner=999999;
				var winNum:int=-1;
				for(q=0;q<hitArray.length;q++){
					if(hitArray[q][3]<winner){
						winNum=q;
						winner=hitArray[q][3];
					}
				}
				
				return(new Array(true,hitArray[winNum]));
			}
			else{
				return(new Array(false,false));
			}
			*/
		}
		public static function detectCollisionLinear(dragging:SpriteExtended, degrees:Number, speed:Number, objects:Array, reboundType:String = "rebound"):Array
		{
			while (degrees > 180)
			{
				degrees -= 360;
			}
			while (degrees < -180)
			{
				degrees += 360;
			}
			var collision1:Array = doDetectCollisionLinear(dragging,degrees,speed,objects);
			if(reboundType == "slide" && collision1[0]){
				var slideAngle:Number;// trace("diff1", collision1[1][2]);
				var angle:Number = collision1[1][3];
				while (angle > 180)
				{
					angle -= 360;
				}
				while (angle < -180)
				{
					angle += 360;
				}
				var diff1:Number = Math.abs(degrees - angle);
				var d2:Number = collision1[1][3] + 180;
				while (d2 > 180)
				{
					d2 -= 360;
				}
				while (d2 < -180)
				{
					d2 += 360;
				}
				var diff2:Number = Math.abs(degrees - d2);
				var tempDiff:Number;
				while (diff1 > 180)
				{
					diff1 -= 360;
				}
				while (diff1 < -180)
				{
					diff1 += 360;
				}
				
				while (diff2 > 180)
				{
					diff2 -= 360;
				}
				while (diff2 < -180)
				{
					diff2 += 360;
				}
				
				var newSpeed:Number = speed - collision1[1][2];
					
				if(Math.abs(diff1) < Math.abs(diff2)){
					slideAngle = angle;
					newSpeed = Math.cos(Math.abs(diff1) * dg) * (speed - collision1[1][2]);
				}
				else{
					slideAngle = d2;
					newSpeed = Math.cos(Math.abs(diff2) * dg) * (speed - collision1[1][2]);
				}
				
				
				var collision2:Array = doDetectCollisionLinear(dragging,slideAngle,newSpeed,objects,collision1[5],collision1[6]);
				return(collision1.concat(collision2));
				return([false,false]);
			}
			else{
				return(collision1);
			}
			
		}
		
		public static function doDetectCollisionLinear(dragging:*,degrees:Number,speed:Number,objects:Array,ignoreA:Point = null,ignoreB:Point = null):Array{
			if(speed == 0){
				return([false,false]);
			}
			while(degrees > 180){
				degrees -= 360;
			}
			while(degrees < -180){
				degrees += 360;
			}
			
			var backup:Number = 0;
			var winner:Number = speed;
			var winArray:Array = new Array();
			var obj:SpriteExtended;
			var scopedPoints:Array;
			var scopedLines:Array;
			var isHit:*;
			var g:Number;
			var x1:Number;
			var y1:Number;
			var A:Point;
			var B:Point;
			var C:Point;
			var D:Point;
			var slD1:Number;
			var slD2:Number
			var hit:Boolean;
			var lineAngle:Number;
			
			
			scopedPoints = dragging.getScopedPoints();
			
			
			
			// get the bounds of this whole thing so we can get the least amount of objects to compare to
			var minX:Number = scopedPoints[0].x;
			var maxX:Number = scopedPoints[0].x;
			var minY:Number = scopedPoints[0].y;
			var maxY:Number = scopedPoints[0].y;
			
			var xBackup:Number = Math.cos((degrees - 180) * dg) * backup;
			var yBackup:Number = (Math.sin((degrees - 180) * dg) * backup) * -1;
			
			var xDiff:Number = Math.cos(degrees * dg) * speed;
			var yDiff:Number = Math.sin(degrees * dg) * speed * -1;
			
			for (var a:int = 1; a < scopedPoints.length; a++)
			{
				minX = Math.min(minX, scopedPoints[a].x + xBackup);
				minX = Math.min(minX, scopedPoints[a].x + xDiff + xBackup);
				maxX = Math.max(maxX, scopedPoints[a].x + xBackup);
				maxX = Math.max(maxX, scopedPoints[a].x + xDiff + xBackup);
				minY = Math.min(minY, scopedPoints[a].y + yBackup);
				minY = Math.min(minY, scopedPoints[a].y + yDiff + yBackup);
				maxY = Math.max(maxY, scopedPoints[a].y + yBackup);
				maxY = Math.max(maxY, scopedPoints[a].y + yDiff + yBackup);
				
			}
			
			var minPoint:Point = new Point(minX, minY);
			var maxPoint:Point = new Point(maxX, maxY);
			
			
			
			
			for(var b:int = 0;b < objects.length;b++){
				obj = objects[b];
				if(obj != dragging){											
						
					scopedLines = obj.getScopedLines(minPoint, maxPoint);
					
					for(var c:int = 0;c < scopedLines.length;c++){
							
						C = scopedLines[c][0];
						D = scopedLines[c][1];
						//if (ignoreA == C && ignoreB == D) continue;// this is the second collision and this is the line that we are comparing to
						for (a = 0; a < scopedPoints.length; a++) {
							A = scopedPoints[a];
							A.x += xBackup;
							A.y += yBackup;
							B = new Point(A.x + xDiff, A.y + yDiff);
						
							
							//if (!inRange(A, B, C, D)) continue;
							
							lineAngle = (Math.atan2((scopedLines[c][1].y - scopedLines[c][0].y) * -1, scopedLines[c][1].x - scopedLines[c][0].x) * rd) - 180;
							slD1 = lineAngle;
							slD2 = lineAngle;		
							
							isHit = isIntercepting(A,B,C,D);
							if(isHit != false && isHit != "Infinity"){
																	
								g = findC(A.y - isHit.y,A.x - isHit.x);
								if(g < winner && g >= 0){
									winner = g;
									winArray = new Array(true,g,isHit,slD1,slD2,1,C,D,obj);
								}
							}							
						}						
					}						
				}				
			}
			var degrees2:Number;
			scopedLines = dragging.getScopedLines();
			
			minX = scopedLines[0][0].x;
			maxX = scopedLines[0][0].x;
			minY = scopedLines[0][0].y;
			maxY = scopedLines[0][0].y;
			
			xBackup = Math.cos((degrees - 180) * dg) * backup;
			yBackup = (Math.sin((degrees - 180) * dg) * backup) * -1;
			
			xDiff = Math.cos(degrees * dg) * speed;
			yDiff = Math.sin(degrees * dg) * speed * -1;
			
			var pointA:Point;
			var pointB:Point;
			
			for (a = 1; a < scopedLines.length; a++)
			{
				pointA = scopedLines[a][0];
				pointB = scopedLines[a][1];
				minX = Math.min(minX, pointA.x, pointA.x + xBackup + xDiff);
				minX = Math.min(minX, pointB.x, pointB.x + xBackup + xDiff);
				maxX = Math.max(maxX, pointA.x, pointA.x + xBackup + xDiff);
				maxX = Math.max(maxX, pointB.x, pointB.x + xBackup + xDiff);
				minY = Math.min(minY, pointA.y, pointA.y + yBackup + yDiff);
				minY = Math.min(minY, pointB.y, pointB.y + yBackup + yDiff);
				maxY = Math.max(maxY, pointA.y, pointA.y + yBackup + yDiff);
				maxY = Math.max(maxY, pointB.y, pointB.y + yBackup + yDiff);
				
			}
			
			minPoint = new Point(minX, minY);
			maxPoint = new Point(maxX, maxY);
			
			//trace("minX", minPoint.x, "minY", minPoint.y);
			//trace("maxX", maxPoint.x, "maxY", maxPoint.y);
			//trace("                              *********************************");
			
			for (b = 0; b < objects.length; b++)
			{
				obj = objects[b];
				if (obj != dragging)
				{
					
					scopedPoints = obj.getScopedPoints(minPoint, maxPoint);
					
					/*
					trace("************************");
					for (var i:int = 0; i < scopedPoints.length; i++)
					{
						trace("x", scopedPoints[i].x, "y", scopedPoints[i].y);
					}
					trace("************************");
					*/
					
					for(a = 0;a < scopedLines.length;a++){
						C = scopedLines[a][0];
						D = scopedLines[a][1];
						
						lineAngle = (Math.atan2((scopedLines[a][1].y - scopedLines[a][0].y) * -1, scopedLines[a][1].x - scopedLines[a][0].x) * rd) - 180;
						slD1 = lineAngle;
						slD2 = lineAngle;
						
						count++;
						
						for (c = 0; c < scopedPoints.length; c++) {
							
							A = scopedPoints[c];
							//if (A == ignoreA || A == ignoreB) continue;//this is the second collision point
							B = new Point(A.x + Math.cos((degrees - 180) * dg) * speed, A.y + (Math.sin((degrees - 180) * dg) * speed * -1));
							//trace("Cx", C.x, "Cy", C.y, "Dx", D.x, "Dy", D.y);
							//trace("Ax", A.x, "Ay", A.y, "Bx", B.x, "By", B.y);
							//trace("");
							//if (!inRange(A, B, C, D)) continue;
							
							
							//trace("c=",C," d=",D);
							isHit = isIntercepting(A,B,C,D);
							//trace(isHit);
							if(isHit != false && isHit != "Infinity"){
								g = findC(A.y - isHit.y,A.x - isHit.x);
								
								if(g < winner && g >= 0){
									winner = g;
									winArray = new Array(true,g,isHit,slD1,slD2,2,A,null,obj);
								}
							}
						}
					}
				}
			}
							
			if(winner < speed){
				if(winArray[2] == "Infinity"){
					//do nothing, slide?
					return([false,false]);
				}
				if(winner < .5 || ignoreA != null){
					return(new Array(true,[winArray[2].x,winArray[2].y,winArray[1],winArray[4],winArray[1],winArray[5],winArray[6],winArray[8]]));
				}
					
				
				//count++;
				//trace("hit count =",count);
				//trace("winner =", winArray[1]);
				//trace("y = ", winArray[2].y , " x =" , winArray[2].x);
				//trace("detection winner =",winArray[3]);
				dragging.x += Math.cos(degrees * dg) * (winArray[1]  - .3);
				dragging.y += Math.sin(degrees * dg) * (winArray[1] - .3) * -1;
				
				return(new Array(true,[winArray[2].x,winArray[2].y,winArray[1],winArray[4],winArray[1],winArray[5],winArray[6],winArray[8]]));
			}
			else{
				
				dragging.x += Math.cos(degrees * dg) * speed;
				dragging.y += Math.sin(degrees * dg) * speed * -1;
				
				return([false,false]);
			}
			
		}
		
		
		private static var count:int = 0;
		private static function getM(obj1:Point, obj2:Point):Number
		{			
			return(((obj1.y) - (obj2.y))/(obj1.x - obj2.x));
		}
		private static function getB(obj1:Point, obj2:Point):Number
		{
			return((obj1.y) - (getM(obj1,obj2) * obj1.x));
		}
		public static function isIntercepting(A:Point, B:Point, C:Point, D:Point):*
		{
			var	x1:Number = (getB(C,D) - getB(A,B)) / (getM(A,B) - getM(C,D));
			var y1:Number;
			var y2:Number;
			
			if (isNaN(x1))
			{
				if (A.x == B.x)
				{									
					x1 = A.x;
					y1 = (getM(C,D) * x1) + getB(C,D);					
				}
				else
				{
					x1 = C.x;
					y1 = (getM(A,B) * x1) + getB(A,B);
				}
			}
			else
			{
				y1 = (getM(A,B) * x1) + getB(A,B);
			}
			
			var xInBounds:Boolean = false;
			var zeroRiseSensitivity:Number = .5;
			if(A.x < B.x){
				if(x1 >= A.x && x1 <= B.x){
					xInBounds = true;
				}
			}
			else{
				if(x1 >= B.x && x1 <= A.x){
					xInBounds = true;
				}
			}
			if(A.x == B.x){
				if (x1 > A.x - zeroRiseSensitivity && x1 < A.x + zeroRiseSensitivity)
				{
					//xInBounds = true;
				}
			}			
				
			
			var yInBounds:Boolean = false;
			if(A.y < B.y){
				if(y1 >= A.y && y1 <= B.y){
					yInBounds = true;
				}
			}
			else{
				if(y1 >= B.y && y1 <= A.y){
					yInBounds = true;
				}
			}
			if(A.y == B.y){
				if(y1 > A.y - zeroRiseSensitivity && y1 < A.y + zeroRiseSensitivity){
					//yInBounds = true;
				}
			}		
			
			var xInBounds2:Boolean = false;
			if(C.x < D.x){
				if(x1 >= C.x && x1 <= D.x){
					xInBounds2 = true;
				}
			}
			else{
				if(x1 >= D.x && x1 <= C.x){
					xInBounds2 = true;
				}
			}
			if(C.x > D.x - zeroRiseSensitivity && C.x < D.x + zeroRiseSensitivity){
				if(x1 > C.x - zeroRiseSensitivity && x1 < C.x + zeroRiseSensitivity){
					xInBounds2 = true;
				}
			}	
			
			var yInBounds2:Boolean = false;
			if(C.y < D.y){
				if(y1 >= C.y && y1 <= D.y){
					yInBounds2 = true;
				}
			}
			else{
				if(y1 >= D.y && y1 <= C.y){
					yInBounds2 = true;
				}
			}
			if(C.y > D.y - zeroRiseSensitivity && C.y < D.y + zeroRiseSensitivity){
				//count++
				//trace("c = d",count);
				if(y1 > C.y - zeroRiseSensitivity && y1 < C.y + zeroRiseSensitivity){
					yInBounds2 = true;
				}
			}	
			
				
			
			if(x1 == Infinity || x1 == -Infinity || y1 == Infinity || y1 == -Infinity || isNaN(x1) || isNaN(y1)){
				return("Infinity");
					
			}
			
			if(xInBounds && yInBounds && xInBounds2 && yInBounds2){
				count++;
				//trace("inbounds ",count);
				return(new Point(x1, y1));
			}
			return(false);
		}
		private static function findC(y1:Number,x1:Number):Number{
			return(Math.sqrt(y1 * y1 + x1 * x1));
		}
		
		private static function inRange(A:Point, B:Point, C:Point, D:Point):Boolean
		{
			if (Math.min(C.x, D.x) > Math.max(A.x, B.x) || Math.max(C.x, D.x) < Math.min(A.x, B.x) || Math.min(C.y, D.y) > Math.max(A.y, B.y) || Math.max(C.y, D.y) < Math.min(A.y, B.y)) return false;
			return true;
		}
		
		public static function getAngle(point1:Point, point2:Point, returnDegrees:Boolean = true, reverseY:Boolean = false):Number
		{
			var yMultiplier:Number = 1;
			if (reverseY) yMultiplier = -1;
			var rads:Number = Math.atan2((point2.y * yMultiplier) - (point1.y * yMultiplier), point1.x - point2.x);
			if (returnDegrees)
			{
				return rads * rd;
			}
			else
			{
				return rads;
			}
		}
		
		public static function angleDiff(angle1:Number, angle2:Number, isDegrees:Boolean = true):Number
		{
			var diff:Number;
			diff = angle1 - angle2;
			if (isDegrees)
			{				
				while (diff > 180)
				{
					diff -= 180;
				}
				while (diff < -180)
				{
					diff += 180;
				}
				
			}
			else
			{
				
			}
			
			return diff;
		}
		
	}
}








    