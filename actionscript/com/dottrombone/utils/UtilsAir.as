package com.dottrombone 
{
	/**
	 * ...
	 * @author Rand Barnett
	 */
	
	import com.dottrombone.objects.Tracer;
	import flash.data.EncryptedLocalStore;
	import flash.system.Capabilities;
	import flash.utils.ByteArray;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	public class UtilsAir extends UtilsAirBase
	{
				
		public function UtilsAir() 
		{
			UtilsAirBase.instance = (this as UtilsAirBase);
		}
		
		public override function get usingAIR():Boolean
		{
			return true;
		}
		
		public override function getStoredValue(_var:String):String
		{
			//Tracer.instance.doTrace("getStoredValue(AIR) " + _var);
			
			var storedValue:ByteArray = EncryptedLocalStore.getItem(_var);			
			if (storedValue == null) return "";			
			return storedValue.readUTFBytes(storedValue.length);		
		}		
		
		public override function setStoredValue(_var:String, _value:String):void			
		{
			//Tracer.instance.doTrace("setStoredValue(AIR) " + _var + "=" + _value);
			
			var storedValue:ByteArray = new ByteArray();			
			storedValue.writeUTFBytes(_value);			
			EncryptedLocalStore.setItem(_var, storedValue);			
		}
		
		public function newFilename(extensionNoDot:String = "", prefix:String = "_"):String
		{
			var lastFileNumber:String = getStoredValue("lastFileNumber");
			var i:int = 1;
			if (lastFileNumber != "")
			{
				i = parseInt(lastFileNumber, 10);
				i++;
			}
			var iStr:String = i.toString();
			setStoredValue("lastFileNumber", iStr);
			return prefix + iStr + "." + extensionNoDot;
		}
		
		public override function saveFile(ba:ByteArray, extensionNoDot:String, name:String = ""):String
		{			
			if (name == "")
			{
				name = newFilename(extensionNoDot);
			}
			else
			{
				name += "." + extensionNoDot;
			}			

			var file:File = File.cacheDirectory.resolvePath(name);
			var stream:FileStream = new FileStream();
			stream.open(file, FileMode.WRITE);
			stream.writeBytes(ba);
			stream.close();

			return name;
		}
		
		public override function getFile(name:String):ByteArray
		{
			Tracer.instance.doTrace("getFile() - " + name);
			var ba:ByteArray = new ByteArray();
			var file:File = File.cacheDirectory.resolvePath(name);
			if (file.exists)
			{
				var stream:FileStream = new FileStream();
				stream.open(file, FileMode.READ);
				stream.readBytes(ba);
				stream.close();
				return ba;
			}
			
			Tracer.instance.doTrace("getFile() file does not exist!");
			
			return null;
		}
		
		public override function getLocalFileURL(localFilename:String):String
		{
			var file:File = File.cacheDirectory.resolvePath(localFilename);
			if (file.exists)
			{
				return file.url;
			}
			return "";
		}
		
		public override function get device():String
		{
			var osInfo:Array = Capabilities.os.split(" ");
			return osInfo[0];
		}
		
	}

}