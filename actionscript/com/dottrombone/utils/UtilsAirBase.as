package com.dottrombone 
{
	/**
	 * ...
	 * @author Rand Barnett
	 */
	
	import com.dottrombone.objects.Tracer;
	import flash.utils.ByteArray;
	
	public class UtilsAirBase 
	{
		// this class has to be used as a singleton so that it can be extended
		// this base class can be called on non-air projects
		// UtilsAir extends this to provide AIR functionality
		
		private static var mInstance:UtilsAirBase = null;
		private var localSavedFiles:Array = new Array();
		
		public function UtilsAirBase() 
		{
			
		}
		
		public static function set instance(utilsAirBaseInstance:UtilsAirBase):void
		{
			mInstance = utilsAirBaseInstance;
		}
		
		public static function get instance():UtilsAirBase
		{
			if (mInstance == null)
			{
				mInstance = new UtilsAirBase();
			}
			
			return mInstance;
		}
		
		public function get usingAIR():Boolean
		{
			return false;
		}
		
		public function getStoredValue(_var:String):String
		{
			// override for AIR
			// else use cookies
			Tracer.instance.doTrace("getStoredValue(browser)" + _var);						
			return "";		
		}		
		
		public function setStoredValue(_var:String, _value:String):void			
		{
			// override for AIR
			// else use cookies
			//Tracer.instance.doTrace("setStoredValue(browser) " + _var + "=" + _value);			
		}
		
		public function saveFile(ba:ByteArray, extensionNoDot:String, name:String = ""):String
		{
			localSavedFiles.push(ba);
			return (localSavedFiles.length - 1).toString();
		}
		
		public function getFile(name:String):ByteArray
		{
			if (name == "") return null;
			var i:int = parseInt(name, 10);
			if (localSavedFiles.length > i)
			{
				return localSavedFiles[i];
			}
			return null;
		}
		
		public function getLocalFileURL(localFilename:String):String
		{
			return "";
		}
		
		public function get device():String
		{
			return "browser";
		}
		
	}

}