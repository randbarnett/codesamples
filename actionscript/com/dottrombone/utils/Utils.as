package com.dottrombone 
{	
	import com.dottrombone.objects.DTMovieClip;
	import flash.display.*;
	import flash.text.*;
	import flash.geom.ColorTransform;
	import flash.geom.Transform;
	import flash.net.*;
	import flash.utils.getDefinitionByName;
	import flash.utils.ByteArray;
	import flash.media.*;
	import com.dottrombone.objects.Tracer;
	import flash.filters.GlowFilter;
	import fl.motion.Color;
	import flash.filters.ColorMatrixFilter;    
	import fl.motion.AdjustColor;
	import com.dottrombone.UtilsAirBase;
	import flash.filters.BitmapFilter;
    import flash.filters.BitmapFilterQuality;
    import flash.filters.DropShadowFilter;
	
	public class Utils
	{
		
		static public const dg:Number = Math.PI / 180;
		static public const rd:Number = 180 / Math.PI;
		
		static public function get usingAIR():Boolean
		{
			return UtilsAirBase.instance.usingAIR;
		}
		
		static public function getAsset(className:String):*
		{
			//trace("className = ", className);
			var c:Class = Class(getDefinitionByName(className));
			return new c();
		}
		
		static public function removeAllChildren(mc:MovieClip):void
		{
			if (mc == null) return;
			if (mc.numChildren > 0)
			{
				while (mc.numChildren > 0)
				{
					mc.removeChild((mc.getChildAt(0) as DisplayObject));
				}
			}
		}	
		
		static public function removeAllDisplayObjects(mrdo:DisplayObjectContainer):void
		{
			if (mrdo == null) return;
			if (mrdo.numChildren > 0)
			{
				while (mrdo.numChildren > 0)
				{
					mrdo.removeChild((mrdo.getChildAt(0) as DisplayObject));
				}
			}
		}		
		
		static public function getStoredValue(_var:String):String
		{
			return UtilsAirBase.instance.getStoredValue(_var);		
		}
		
		static public function setStoredValue(_var:String, _value:String):void			
		{
			UtilsAirBase.instance.setStoredValue(_var, _value);			
		}
		
		static public function getFileURL(filename:String):String
		{
			return UtilsAirBase.instance.getLocalFileURL(filename);
		}
		
		static public function get device():String
		{
			return UtilsAirBase.instance.device;
		}
		
		public static function tf(size:Number, color:String, align:String = "left"):TextField
		{
			var mtf:TextField = new TextField();
			mtf.embedFonts = true;
			mtf.autoSize = align;
			mtf.multiline = false;
			mtf.selectable = false;
			
			/*
			var fonts:Array = ( Font.enumerateFonts() );
			for each( var fo in fonts ){
			   trace ( fo.fontName ,":", fo.fontStyle )
			}
			*/
			
			/*
			var css:StyleSheet = new StyleSheet();
			var obj:Object = new Object();
			obj.fontSize = size.toString();
			obj.color = "#" + color;
			obj.fontFamily = "Lato";
			obj.textAlign = align;
			
			css.setStyle("bold" ,{ fontFamily:"Lato Bold" }); // you can catch the fontname in the list that was printed by the code above...
			css.setStyle("italic" , { fontFamily:"Lato Italic" } );
			css.setStyle(".defaultStyle", obj);
			mtf.styleSheet = css;
			//mtftxtfield.htmlText = "regular or<bold>bold font</bold>."
			*/
			
			
			var textFormat:TextFormat = new TextFormat();
			var font:Font = new Lato();
			textFormat.font = font.fontName;
			textFormat.size = size;
			textFormat.color = parseInt(color, 16);
			if (align == "center")
			{
				textFormat.align = TextFormatAlign.CENTER;
			}
			
			mtf.defaultTextFormat = textFormat;
			
			
			return mtf;
		}
		
		public static function htmlText(str:String, convertLineBreaks:Boolean = true):String
		{
			str = replaceAll(str, "<b>", '<font face="Lato Bold">');
			str = replaceAll(str, "</b>", '</font>');
			str = replaceAll(str, "<i>", '<font face="Lato Italic">');
			str = replaceAll(str, "</i>", '</font>');
			str = replaceAll(str, "<bi>", '<font face="Lato Bold Italic">');
			str = replaceAll(str, "</bi>", '</font>');
			
			if(convertLineBreaks) str = replaceAll(str, "\r\n", '<br>');
			
			return str;
		}
		
		
		public static function formatText(tf:TextField, color:String)
		{
			//tf.antiAliasType = "advanced";
			//tf.gridFitType = "subpixel";
			tf.textColor = parseInt(color, 16);
			//tf.wordWrap = true;
		}
		
		public static function tfColor(tf:TextField, color:String):void
		{
			tf.textColor = parseInt(color, 16);
		}
		
		public static function tfBold(tf:TextField):void
		{
			var format:TextFormat = tf.getTextFormat();
			format.bold = true;
			format.italic = false;
			tf.setTextFormat(format);
			tf.defaultTextFormat = format;
		}
		
		public static function tfItalic(tf:TextField):void
		{
			var format:TextFormat = tf.getTextFormat();
			format.italic = true;
			format.bold = false;
			tf.setTextFormat(format);
			tf.defaultTextFormat = format;
		}
		
		public static function tfNormal(tf:TextField):void
		{
			var format:TextFormat = tf.getTextFormat();
			format.italic = false;
			format.bold = false;
			tf.setTextFormat(format);
			tf.defaultTextFormat = format;
		}
		
		public static function mcColor(mc:Sprite, color:String):void
		{
			var colorTrans:ColorTransform = new ColorTransform();		
			colorTrans.color = parseInt(color, 16); 
						
			var trans:Transform = new Transform(mc);
			trans.colorTransform = colorTrans;
		}
		
		public static function mcTint(mc:Sprite, color:String, alpha:Number):void 
		{
			
			var colorInt:uint = parseInt(color, 16);
			var c:Color = new Color();
			c.tintColor = colorInt;
			c.tintMultiplier = alpha;
			mc.transform.colorTransform = c;
			
			
			return;
			
			var red:uint = parseInt(color.substr(0, 2), 16);
			var green:uint = parseInt(color.substr(2, 2), 16);
			var blue:uint = parseInt(color.substr(4, 2), 16);
			
			var colorTrans:ColorTransform = new ColorTransform();		
			//colorTrans.color = parseInt(color, 16); 
			colorTrans.alphaMultiplier = alpha;
			colorTrans.redMultiplier = red;
			colorTrans.greenMultiplier = green;
			colorTrans.blueMultiplier = blue;
			
			mc.transform.colorTransform = colorTrans;
			
		}
		
		public static function textfieldColor(tf:TextField, color:String = "000000"):void
		{
			var format:TextFormat = tf.getTextFormat();
			format.color = parseInt(color, 16);
			tf.setTextFormat(format);
			tf.defaultTextFormat = format;
		}
		public static function pictureAlignment(mc:MovieClip, xml:*):void
		{
			if (xml.scale != undefined && xml.scale != "")
			{
				mc.scaleX=Number(xml.scale)/100;
				mc.scaleY=Number(xml.scale)/100;
			}
			if (xml.xOffset != undefined && xml.xOffset != "")
			{
				mc.x=xml.xOffset;
			}
			if (xml.yOffset != undefined && xml.yOffset != "")
			{
				mc.y=xml.yOffset;
			}
		}
		public static function getXMLNum(xml:String):Number
		{
			if (xml != "")
			{
				return(Number(xml));
			}
			else
			{
				return(0);
			}
		}
		static public function randomInt(min:Number, max:Number):Number 
		{
			return Math.floor(Math.random() * (max - min + 1)) + min;
		}
	
		static public function randomFloat(min:Number, max:Number):Number
		{
			return (Math.random() * (max - min + 1)) + min;
		} 
			
		static public function getDistance(x1:Number, y1:Number, x2:Number, y2:Number):Number
		{
			return Math.sqrt(((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2)));
		}
		
		static public function shuffle(n:Number):Array
		{
			//n is the number to shuffle, sends back an array
			var a:Array=new Array(n);
			for(var b:int=0;b<n;b++){
				a[b]=b;
			}
			var r:Array=new Array();//return answer
			var c:Number=n;
			var i:Number;
			for(var d=0;d<n;d++){
				i=Math.floor(Math.random()*c);
				r.push(a[i]);
				a.splice(i,1);
				c--;
			}
			return(r);
		}
		static public function shuffleArray(g:Array):Array
		{
			var n:int=g.length;
			//n is the number to shuffle
			var a:Array=new Array(n);
			for(var b:int=0;b<n;b++){
				a[b]=b;
			}
			var r:Array=new Array();//return answer
			var c:int=n;
			var i:Number;
			for(var d:int=0;d<n;d++){
				i=Math.floor(Math.random()*c);
				r.push(a[i]);
				a.splice(i,1);
				c--;
			}
			var p:Array=new Array(n);
			for(b=0;b<n;b++){
				i=r[b];
				p[b]=g[i];
			}
			return(p);
		}
		static public function sortArrayByIndexDesc(array:Array, index:Number):Array
		{
			array.sortOn(index.toString(), Array.NUMERIC);
			array.reverse();
			return array;
		}
		static public function sortArrayByIndexAscending(array:Array, index:Number):Array
		{
			array.sortOn(index.toString(), Array.NUMERIC);
			
			return array;
		}	
		public static function sortArrayDesc(array:Array, index:uint):Array
		{
			return sortArrayByIndexDesc(array, (index as Number));
		}
		
		public static function mcBrightness(mc:MovieClip, b:Number):void
		{
			
			// this was erroring out.. 
			var t:Color = new Color();
			t.brightness = b;
			mc.transform.colorTransform = t;
		}
		static public function setVolume(chan:SoundChannel, vol:Number):void
		{
            var transform:SoundTransform = chan.soundTransform;
            transform.volume = vol;
            chan.soundTransform = transform;
		}
		static public function setMasterVolume(vol:Number):void
		{
			var transform:SoundTransform = new SoundTransform();
            transform.volume = vol;
            SoundMixer.soundTransform = transform;
		}
		static public function setPan(chan:SoundChannel,pan:Number):void{
			var transform:SoundTransform = chan.soundTransform;
            transform.pan = pan;
            chan.soundTransform = transform;
		}
		static public function fill(color:String = "00ff00"):DTMovieClip
		{
			var mc:DTMovieClip = new DTMovieClip();
			mc.graphics.beginFill(parseInt(color, 16));
			mc.graphics.moveTo(0, 0);
			mc.graphics.drawRect(0, 0, 100, 100);
			mc.graphics.endFill();
			return mc;
		}
		
		static public function xmlResult(txt:String):XML
		{
			var xmlString:String;
			var xmlnsPattern:RegExp;
			var namespaceRemovedXML:String;
			var responseXML:XML;

			
			// convert it to a string
			xmlString = txt;

			// define the regex pattern to remove the namespaces from the string
			xmlnsPattern = new RegExp("xmlns=[^\"]*\"[^\"]*\"", "gi");

			// remove the namespaces from the string representation of the XML
			namespaceRemovedXML = xmlString.replace(xmlnsPattern, "");

			// set the string rep. of the XML back to real XML
			responseXML = new XML(namespaceRemovedXML);

			// do something with the XML
			return responseXML;
		}
		
		static public function trim(str:String):String
		{
			return str.replace( /^([\s|\t|\n]+)?(.*)([\s|\t|\n]+)?$/gm, "$2" );
		}
		
		static public function strToVar(str:String):String
		{
			var returnStr:String = "";
			for (var a:int = 0; a < str.length; a++)
			{
				if (new RegExp("[A-Za-z0-9]").test(str.charAt(a)))
				{
					returnStr += str.charAt(a);
				}
			}
			return returnStr;
		}
		
		static public function addTextOutline(tf:TextField, color:uint = 0x000000, size:Number = 1):void
		{
			var outline:GlowFilter = new GlowFilter(color, 1, size, size, 999, 1, false, false);
			//outline.quality = BitmapFilterQuality.MEDIUM;
			tf.filters = [outline];
		}
		
		static public function isoToDate(value:String):Date 
		{
			var dateStr:String = value;
			dateStr = dateStr.replace(/\-/g, "/");
			dateStr = dateStr.replace("T", " ");
			dateStr = dateStr.replace("Z", " GMT-0000");

			return new Date(Date.parse(dateStr));
		}
		
		static public function bestFit(mc:DisplayObject, w:Number, h:Number, doCenter:Boolean = false):void
		{
			var w1:Number = mc.width;
			var h1:Number = mc.height;
			if (((w1 * h) / h1) / w < ((h1 * w) / w1) / h)
			{
				mc.height = h;
				mc.width = (w1 * h) / h1;
			}
			else
			{
				mc.width = w;
				mc.height = (h1 * w) / w1;
			}
			
			if (doCenter)
			{
				centerMC(mc, w, h);
			}
		}
		
		static public function crop(mc:DisplayObject, w:Number, h:Number, doCenter:Boolean = false):void
		{
			var w1:Number = mc.width;
			var h1:Number = mc.height;
			
			mc.scaleX = mc.scaleY = 1;
			
			if ((w * h1) / w1 < h)
			{
				// use height as 100% and crop width
				mc.height = h;
				mc.width = (h * w1) / h1;
			}
			else
			{
				// use width at 100% and crop height
				mc.width = w;
				mc.height = (w * h1) / w1;
			}
			
			if (doCenter)
			{
				centerMC(mc, w, h);
			}
		}
		
		static public function centerMC(mc:DisplayObject, w:Number, h:Number):void
		{
			if (w != 0)
			{
				mc.x = (w / 2) - (mc.width / 2);
			}
			if (h != 0)
			{
				mc.y = (h / 2) - (mc.height / 2);
			}
		}
		
		static public function fileExtension(str:String):String
		{
			var array:Array = str.split(".");
			return (array.pop() as String).toLowerCase();
		}
		
		static public function scaleToWidth(mc:DisplayObject, w:Number):void
		{
			mc.scaleX = mc.scaleY = 1;
			var w2:Number = mc.width;
			var h2:Number = mc.height;
			
			mc.width = w;
			mc.height = (w * h2) / w2;
		}
		
		static public function scaleToHeight(mc:DisplayObject, h:Number):void
		{
			var w2:Number = mc.width;
			var h2:Number = mc.height;
			mc.height = h;
			mc.width = (h * w2) / h2;
		}
		
		static public function mobileDeviceType(sw:Number, sh:Number, defaultDevice:String = "iPhone5"):String
		{
			if ((sw == 640 || sh == 640) && (sw == 960 || sh == 960))
			{
				// iphone 4
				return "iPhone4";
			}
			if ((sw == 640 || sh == 640) && (sw == 1136 || sh == 1136))
			{
				// iphone 5
				return "iPhone5";
			}
			if ((sw == 750 || sh == 750) || (sw == 1334 || sh == 1334))
			{
				// iphone 6
				return "iPhone6";
			}
			if ((sw == 828 || sh == 828) || (sw == 1472 || sh == 1472))
			{
				// iphone 6 plus
				return "iPhone6+";
			}
			
			return defaultDevice;
		}
		
		static public function fontSize(tf:TextField, size:Number):void
		{
			var format:TextFormat = tf.getTextFormat();
			format.size = size;
			tf.setTextFormat(format);
			tf.defaultTextFormat = format;
		}
		
		static public function replaceAll(haystack:String, needle:String, replace:String):String
		{
			return haystack.split(needle).join(replace);
		}
		
		static public function cdata(str:String):String
		{
			return '<![CDATA[' + Utils.replaceAll(str, ']]>', ']]]]><![CDATA[>') + ']]>';
		}
		
		static public function getLoadParameter(name:String, stage:Stage):String
		{
			var obj:Object = LoaderInfo(stage.loaderInfo).parameters;
			if (obj == null) return "";
			
			var n:* = obj[name];
			
			if (n == null || n == undefined) return "";
			return String(n);
		}
		
		static public function objectsAreSame(a:*, b:*):Boolean
		{
			if (a === null || a is Number || a is int || a is Boolean || a is String)
			{
				// Compare primitive values.
				var val:Boolean = (a === b);
				//if (val == false) trace("not equal: " + a);
				return val;
			}
			else
			{
				var p:*;
				for (p in a)
				{
					// Check if a and b have different values for p.
					if (!objectsAreSame(a[p], b[p]))
					{
						return false;
					}
				}
				for (p in b)
				{
					// Check if b has a value which a does not.
					if (!objectsAreSame(b[p], a[p]))
					{
						return false;
					}
				}
				return true;
			}
			return false;
		}
		
		static public function addDropShadow(mc:MovieClip, pColor:String = "000000"):void
		{
			var color:Number = parseInt(pColor, 16);
            var angle:Number = 45;
            var alpha:Number = 0.8;
            var blurX:Number = 8;
            var blurY:Number = 8;
            var distance:Number = 15;
            var strength:Number = 0.65;
            var inner:Boolean = false;
            var knockout:Boolean = false;
            var quality:Number = BitmapFilterQuality.HIGH;
            var f:DropShadowFilter = new DropShadowFilter(distance,
                                        angle,
                                        color,
                                        alpha,
                                        blurX,
                                        blurY,
                                        strength,
                                        quality,
                                        inner,
                                        knockout);
										
			var filters:Array = mc.filters
			filters.push(f);
			mc.filters = filters;
		}
		
		static public function addGlow(mc:MovieClip, pColor:String = "000000", pWidth:int = 35, pStrength:Number = 2, pAlpha:Number = .8):void
		{		
            var color:Number = parseInt(pColor, 16);
            var alpha:Number = pAlpha;
            var blurX:Number = pWidth;
            var blurY:Number = pWidth;
            var strength:Number = pStrength;
            var inner:Boolean = false;
            var knockout:Boolean = false;
            var quality:Number = BitmapFilterQuality.HIGH;

            var f:GlowFilter = new GlowFilter(color,
                                  alpha,
                                  blurX,
                                  blurY,
                                  strength,
                                  quality,
                                  inner,
                                  knockout);
								  
			var filters:Array = mc.filters
			filters.push(f);
			mc.filters = filters;
        }
		
		static public function mcBlackAndWhite(mc:MovieClip):void
		{
			var color : AdjustColor;
			var colorMatrix : ColorMatrixFilter;
			var matrix : Array;
			var filterBW : Array;
			 
			color = new AdjustColor();
			color.brightness = 20;
			color.contrast = 20;
			color.hue = 0;
			color.saturation = -100;
			 
			matrix = color.CalculateFinalFlatArray();
			colorMatrix = new ColorMatrixFilter(matrix);
			filterBW = [colorMatrix];
			
			mc.filters = filterBW;
		}
		
		static public function tfLeading(tf:TextField, i:int):void
		{
			var format:TextFormat = tf.defaultTextFormat;
			format.leading = i;
			tf.setTextFormat(format);
			tf.defaultTextFormat = format;
		}
		
		static public function tfLetterSpacing(tf:TextField, n:Number):void
		{
			var format:TextFormat = tf.defaultTextFormat;
			format.letterSpacing = n;
			tf.setTextFormat(format);
			tf.defaultTextFormat = format;
		}
	}
}





