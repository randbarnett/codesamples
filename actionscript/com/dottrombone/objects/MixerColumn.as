﻿package com.dottrombone.objects{
	
	import flash.display.*;
	import flash.events.*;
	import flash.media.*;
	import com.dottrombone.Utils;
	
	public class MixerColumn extends MovieClip{
		
		public static const VOLUME_CHANGE:String = "volumeChange";
		public static const PAN_CHANGE:String = "panChange";
		
		private var solo:Boolean = false;
		private var mute:Boolean = false;
		private var volumePercentage:Number = 0;
		public var channel:SoundChannel;//assign the sound channel to this and the level meter works
		private var isManual:Boolean = false;
		
		public function MixerColumn():void
		{
			channel = new SoundChannel();
			txtGain.visible = false;
			soloBtn.gotoAndStop(1);
			muteBtn.gotoAndStop(1);
			levelMeter.gotoAndStop(1);
			muteBtn.addEventListener(MouseEvent.MOUSE_DOWN,doMouseDown);
			soloBtn.addEventListener(MouseEvent.MOUSE_DOWN,doMouseDown);
			volumeSlider.addEventListener(VerticalSlider.LEVEL_CHANGE,doVolumeChange);
			panKnob.addEventListener(Knob.LEVEL_CHANGE,doPanChange);
			addEventListener(Event.ENTER_FRAME,doEnterFrame);
		}
		
		public function set manual(bool:Boolean):void
		{
			isManual = bool;
		}
		
		public function showGain():void
		{
			txtGain.visible = true;
		}
		public function init():void
		{
			Utils.setVolume(channel,volume);
			Utils.setPan(channel,pan);
		}		
		
		private function doMouseDown(e:Event):void
		{
			switch(e.currentTarget.name)
			{
				case "soloBtn":
					if (solo)
					{
						solo = false;
						soloBtn.gotoAndStop(1);
					}
					else{
						solo = true;
						soloBtn.gotoAndStop(2);
					}
					break;
				case "muteBtn":
					if (mute)
					{
						mute = false;
						muteBtn.gotoAndStop(1);
					}
					else
					{
						mute = true;
						muteBtn.gotoAndStop(2);
					}
					Utils.setVolume(channel, volume);
					dispatchEvent(new Event(VOLUME_CHANGE));
					break;
			}
		}
		private function doVolumeChange(e:Event):void
		{
			volumePercentage = volumeSlider.percentage;
			dispatchEvent(new Event(VOLUME_CHANGE));
			Utils.setVolume(channel, volume);
		}
		private function doPanChange(e:Event = null):void
		{
			dispatchEvent(new Event(PAN_CHANGE));
			Utils.setPan(channel, pan);
		}
		private function doEnterFrame(e:Event):void
		{
			if (!isManual)
			{
				var p:Number = channel.leftPeak;
				if (channel.rightPeak > p)
				{
					p = channel.rightPeak;
				}
				setLevelMeter(p);
			}
		}
		public function setLevelMeter(p:Number)
		{
			var f:int = Math.floor(p * (levelMeter.totalFrames - 1)) + 1;
			levelMeter.gotoAndStop(f);
		}
		public function get volume():Number
		{
			if (mute)
			{
				return(0);
			}
			else
			{
				return(volumePercentage);
			}
		}
		public function set volume(n:Number):void
		{
			volumeSlider.volume = n;
			volumePercentage = n;
			dispatchEvent(new Event(VOLUME_CHANGE));
		}
		public function get pan():Number
		{
			return((panKnob.level * 2) - 1);
		}
		public function set pan(n:Number):void
		{
			panKnob.level = (n + 1) / 2;
			doPanChange();
		}
		
		public function get gain():Number
		{
			return (pan + 1) / 2;
		}
		
		public function set gain(n:Number):void
		{
			pan = (n * 2) - 1;
		}
	}
}