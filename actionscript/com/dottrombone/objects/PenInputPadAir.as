package com.dottrombone.objects 
{
	/**
	 * ...
	 * @author Rand Barnett
	 */
	
	import com.dottrombone.objects.PenInputPad;
	import flash.events.TouchEvent;
	import flash.utils.ByteArray;
	import flash.geom.Point;
	
	public class PenInputPadAir extends PenInputPad
	{
		
		public function PenInputPadAir(initGraphics:Boolean = true) 
		{
			super(initGraphics);
		}
		
		public override function onTouchBegin(e:TouchEvent)
		{
			Tracer.instance.doTrace("onTouchBegin");
			if (e.touchIntent == "pen")
			{
				stylusTouchID = e.touchPointID;
				Tracer.instance.doTrace("stylus touch found");// = " + stylusTouchID);
				//isDrawing = true;
			}
			//myTextField.text = "touch begin" + event.touchPointID; 
			
			activeTouchPoints.push(new Array(e.touchPointID, e));
		} 
		public override function onTouchMove(e:TouchEvent)
		{
			Tracer.instance.doTrace("onTouchMove()");
			//myTextField.text = "touch move" + event.touchPointID; 
			if ((e.type == TouchEvent.TOUCH_MOVE || e.type == TouchEvent.PROXIMITY_MOVE))// && e.touchEventIntent != TouchEventIntent.UNKNOWN)
			{
				//Tracer.instance.doTrace("onTouchMove() action!");
				var samples:ByteArray = new ByteArray();
				e.getSamples(samples, true);
				samples.position = 0;     // rewind to beginning of array before reading
        
				var xCoord:Number, yCoord:Number, pressure:Number;
				
				var lastSampleEvent:TouchEvent = getLastTouchEvent(e.touchPointID);
				if (lastSampleEvent != null)
				{
					var startingPoint:Point = drawingHolder.globalToLocal(new Point(lastSampleEvent.stageX, lastSampleEvent.stageY));
					drawingHolder.graphics.moveTo(startingPoint.x, startingPoint.y);
					Tracer.instance.doTrace("startingPoint = " + startingPoint.x + "," + startingPoint.y);
				}
				
				Tracer.instance.doTrace("samples = " + samples.length);
				Tracer.instance.doTrace("pressue = " + e.pressure);
				
				while( samples.bytesAvailable > 0 )
				{
					xCoord = samples.readFloat();
					yCoord = samples.readFloat();
					pressure = samples.readFloat();
					//do something with the sample data
					setLineStylePressure(pressure);
					drawingHolder.graphics.lineTo(xCoord, yCoord);
					Tracer.instance.doTrace("sampleDraw = " + xCoord + "," + yCoord + " pressure = " + pressure);
				}
				
				setLineStylePressure(pressure);
				
				var thisPoint:Point = drawingHolder.globalToLocal(new Point(e.stageX, e.stageY));
				drawingHolder.graphics.lineTo(thisPoint.x, thisPoint.y);
				
				setLastTouchEvent(e.touchPointID, e);
				
			}
		}
		
	}

}