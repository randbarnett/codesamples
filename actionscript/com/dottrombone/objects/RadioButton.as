package com.dottrombone.objects 
{
	/**
	 * ...
	 * @author Rand Barnett
	 */
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import com.dottrombone.Utils;
	import com.dottrombone.assets.DTEvent;
	import flash.events.MouseEvent;
	import flash.text.TextFieldAutoSize;
	
	public class RadioButton extends MovieClip
	{
		public var otherButtons:Array = new Array();
		public var isOn:Boolean = false;
		
		public function RadioButton() 
		{
			mcInside.visible = false;
			
			tf.autoSize = TextFieldAutoSize.LEFT;
			text = "";
			
			addEventListener(MouseEvent.CLICK, doClick);
		}
		
		public function set text(str:String):void
		{
			tf.text = str;
		}
		
		public function get text():String
		{
			return tf.text;
		}
		
		public function color(textColorStr:String, buttonBackgroundColorStr:String, ringColorStr:String):void
		{
			Utils.textfieldColor(tf, textColorStr);
			Utils.mcColor(mcInsideOff, buttonBackgroundColorStr);
			Utils.mcColor(mcOutside, ringColorStr);
		}
		
		public function switchOff():void
		{
			isOn = false;
			mcInside.visible = false;
		}
		
		public function doClick(e:Event = null):void
		{
			if (isOn)
			{
				switchOff();
			}
			else
			{
				isOn = true;
				mcInside.visible = true;
			}
			
			for (var a:int = 0; a < otherButtons.length; a++)
			{
				if (otherButtons[a] != this)
				{
					(otherButtons[a] as RadioButton).switchOff();
				}
			}
		}
		
		public function setSelected(str:String):void
		{
			for (var a:int = 0; a < otherButtons.length; a++)
			{
				if ((otherButtons[a] as RadioButton).text == str)
				{
					(otherButtons[a] as RadioButton).doClick();
				}
			}
		}
		
		public function getSelection():String
		{
			for (var a:int = 0; a < otherButtons.length; a++)
			{
				if ((otherButtons[a] as RadioButton).isOn)
				{
					return (otherButtons[a] as RadioButton).text;
				}
			}
			
			return "";
		}
		
		public function destroy():void
		{
			removeEventListener(MouseEvent.CLICK, doClick);
			otherButtons = null;
		}
		
	}

}