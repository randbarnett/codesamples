package com.dottrombone.objects 
{
	/**
	 * ...
	 * @author Rand Barnett
	 */
	
	import com.dottrombone.applications.DocClassBase;
	import com.dottrombone.assets.DTEvent;
	import com.dottrombone.objects.DTTouchButton;
	import com.dottrombone.Utils;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.utils.getTimer;
	
	public class ButtonFromImage extends DTTouchButton
	{
		public var imgOff:MovieClip;
		public var imgOn:MovieClip;
		private var _w:Number = 0;
		private var _h:Number = 0;
		
		public function ButtonFromImage(mImgOff:MovieClip, mImgOn:MovieClip, backgroundColor:String = "0", mIsToggle:Boolean = false) 
		{
			isToggle = mIsToggle;
			
			imgOff = mImgOff;
			imgOn = mImgOn;
			imgOn.visible = false;
			
			_w = mImgOff.width;
			_h = mImgOff.height;
			
			addChild(imgOff);
			addChild(imgOn);
			
		}
		
		
		
		public override function doMouseUp(e:MouseEvent):void
		{
			imgOff.visible = true;
			imgOn.visible = false;
			
			super.doMouseUp(e);
		}
		
		public override function turnOn(bool:Boolean):void
		{
			if (!bool)
			{
				isOn = false;
				imgOff.visible = true;
				imgOn.visible = false;
			}
			else
			{
				imgOff.visible = false;
				imgOn.visible = true;
			}
		}
		
		public function scaleToHeight(n:Number):void
		{
			Utils.scaleToHeight(this, n);
		}
		
		public override function set width(n:Number):void
		{
			_w = n;
			imgOff.width = n;
			imgOn.width = n;
		}
		
		public override function get width():Number
		{
			return imgOff.width;
		}
		
		public override function set height(n:Number):void
		{
			_h = n;
			imgOff.height = n;
			imgOn.height = n;
		}
		
		public override function get height():Number
		{
			return imgOff.height;
		}
		
		public override function destroy():void
		{
			imgOn = null;
			imgOff = null;
			
			super.destroy();
		}
		
	}

}