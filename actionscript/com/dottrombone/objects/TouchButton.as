package com.dottrombone.objects 
{
	/**
	 * ...
	 * @author Rand Barnett
	 */
	
	import com.dottrombone.bluetooth.Utils;
	import flash.display.MovieClip;
	import flash.events.*;
	import flash.text.TextField;
	
	public class TouchButton extends MovieClip
	{
		
		public function TouchButton() 
		{
			gotoAndStop(1);
			addEventListener(Event.ADDED_TO_STAGE, doAddedToStage);
		}
		
		private function doAddedToStage(e:Event):void
		{
			stage.addEventListener(MouseEvent.MOUSE_UP, doMouseUp);
			(parent as MovieClip).addEventListener(MouseEvent.MOUSE_DOWN, doMouseDown);
		}
		
		private function doMouseUp(e:Event):void
		{
			gotoAndStop(1);
		}
		
		private function doMouseDown(e:Event):void
		{
			gotoAndStop(2);
		}
		
	}

}