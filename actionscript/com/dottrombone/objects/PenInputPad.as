package com.dottrombone.objects 
{
	/**
	 * ...
	 * @author Rand Barnett
	 */
	
	import com.dottrombone.applications.DocClassBase;
	import com.dottrombone.Utils;
	import com.dottrombone.UtilsAirBase;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import com.adobe.images.PNGEncoder;
	import flash.geom.Rectangle;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLLoaderDataFormat;
	import flash.events.IOErrorEvent;
	import flash.net.URLRequestMethod;
	import flash.system.Capabilities;
	import flash.text.TextField;
	import flash.utils.ByteArray;
	import flash.events.HTTPStatusEvent;
	import flash.events.TouchEvent;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	import com.dottrombone.assets.DTEvent;
	import com.dottrombone.assets.CollisionDetection;
	
	public class PenInputPad extends MovieClip
	{
		private var saveToFilesystem:Boolean = true;
		public var filename:String = "";
		private var pressureSensitivity:Number = 100;
		public static var defaultLineThickness:int = 1;
		public var background:MovieClip;
		private var isDrawing:Boolean = false;
		public var btnClose:MovieClip;
		public var btnSave:MovieClip;
		public var btnClear:MovieClip;
		public var btnStylus:MovieClip;
		public var drawingHolder:MovieClip;
		public var drawingLayer:MovieClip;
		private var lastPoint:Point;
		private var lastLastPoint:Point;
		private var loader:URLLoader;
		private var uploadURL:String;
		private var txtStatus:TextField;
		public var usingStylus:Boolean = false;
		public var stylusTouchID:int = -1;
		public var activeTouchPoints:Array = new Array();
		public var currentColor:String = "000000";
		
		private var drawCurves:Boolean = true;
		private var curveLengthTolerance:Number = .01;
		private var lastControlPoint:Point = null;
		
		public function PenInputPad(initGraphics:Boolean = true) 
		{
			uploadURL = DocClassBase.globals.baseURL + "/app/imageUpload.php";
			
			background = Utils.fill("cccccc");
			addChild(background);
			
			drawingHolder = new MovieClip();
			addChild(drawingHolder);			
			drawingLayer = new MovieClip();
			drawingHolder.addChild(drawingLayer);
			setLineStyle(defaultLineThickness);
			
			txtStatus = Utils.tf(25, "000000");
			addChild(txtStatus);
			txtStatus.text = "";
			
			if (initGraphics)
			{
				btnClose = new CloseButton();
				btnClose.addEventListener(MouseEvent.CLICK, doClose);
				addChild(btnClose);
				
				btnSave = new SaveButton();
				btnSave.addEventListener(MouseEvent.CLICK, doSave);
				addChild(btnSave);
				
				btnClear = new ClearButton();
				btnClear.addEventListener(MouseEvent.CLICK, doClear);
				addChild(btnClear);
				
				if (Capabilities.touchscreenType == "none")
				{
					btnStylus = new MovieClip;
				}
				else
				{
					usingStylus = false;
					btnStylus = new TouchStylusButton();
					btnStylus.gotoAndStop(2);
					btnStylus.addEventListener(MouseEvent.CLICK, doStylusClick);
					addChild(btnStylus);
				}
				setInputListeners();
				
				resize();
			}
			
			DocClassBase.instance.addEventListener(DTEvent.RESIZE, resize);						
			addEventListener(Event.ENTER_FRAME, doEnterFrame);			
		}
		
		private function setLineStyle(thickness:int = 1, colorStr:String = ""):void
		{
			if (colorStr == "")
			{
				colorStr = currentColor;
			}
			drawingLayer.graphics.lineStyle(thickness, parseInt(colorStr, 16));
		}
		
		public function setLineStylePressure(pressure:Number, thickness:int = 1):void
		{
			thickness += pressure * pressureSensitivity;
			setLineStyle(thickness);// + Math.round(pressure * pressureSensitivity));
		}
		
		public function doMouseDown(e:Event):void
		{
			lastPoint = new Point(drawingHolder.mouseX, drawingHolder.mouseY);
			lastLastPoint = null;
			lastControlPoint = null;
			setLineStyle(0);
			drawingLayer.graphics.beginFill(parseInt(currentColor, 16));
			drawingLayer.graphics.drawCircle(lastPoint.x, lastPoint.y, defaultLineThickness / 2);
			drawingLayer.graphics.endFill();
			isDrawing = true;
		}
		
		private function doMouseUp(e:Event):void
		{
			isDrawing = false;
		}
		
		public function onTouchBegin(e:TouchEvent)
		{
			Tracer.instance.doTrace("onTouchBegin");			
			activeTouchPoints.push(new Array(e.touchPointID, e));
			
			var thisPoint:Point = drawingHolder.globalToLocal(new Point(e.stageX, e.stageY));
			setLineStyle(0);
			drawingLayer.graphics.beginFill(parseInt(currentColor, 16));
			drawingLayer.graphics.drawCircle(thisPoint.x, thisPoint.y, defaultLineThickness / 2);
			drawingLayer.graphics.endFill();
		} 
		public function onTouchMove(e:TouchEvent)
		{
			Tracer.instance.doTrace("onTouchMove()");
			//myTextField.text = "touch move" + event.touchPointID; 
			if ((e.type == TouchEvent.TOUCH_MOVE || e.type == TouchEvent.PROXIMITY_MOVE))// && e.touchEventIntent != TouchEventIntent.UNKNOWN)
			{
				
        
				var xCoord:Number, yCoord:Number, pressure:Number;
				
				Tracer.instance.doTrace("pressure = " + e.pressure);
				
				if (e.pressure == 1)
				{
					setLineStylePressure(0, defaultLineThickness);
				}
				else
				{
					setLineStylePressure(e.pressure, defaultLineThickness);
				}
				
				
				var thisPoint:Point = drawingHolder.globalToLocal(new Point(e.stageX, e.stageY));
				var lastSampleEvent:TouchEvent = getLastTouchEvent(e.touchPointID);
				
				if (lastSampleEvent != null)
				{
					var startingPoint:Point = drawingHolder.globalToLocal(new Point(lastSampleEvent.stageX, lastSampleEvent.stageY));
					drawingLayer.graphics.moveTo(startingPoint.x, startingPoint.y);
					Tracer.instance.doTrace("startingPoint = " + startingPoint.x + "," + startingPoint.y);
					drawingLayer.graphics.lineTo(thisPoint.x, thisPoint.y);
				}
				else
				{
					// throw this one out because we don't have a starting point
				}			
				
				setLastTouchEvent(e.touchPointID, e);
				
			}
		}
		public function getLastTouchEvent(id:int):TouchEvent
		{
			for (var a:int = 0; a < activeTouchPoints.length; a++)
			{
				if (activeTouchPoints[a][0] == id)
				{
					return activeTouchPoints[a][1];
				}
			}
			return null;
		}
		public function setLastTouchEvent(id:int, e:TouchEvent):void
		{
			for (var a:int = 0; a < activeTouchPoints.length; a++)
			{
				if (activeTouchPoints[a][0] == id)
				{
					activeTouchPoints[a][1] = e;
					return;
				}
			}
		}
		private function onTouchEnd(e:TouchEvent)
		{ 
			//myTextField.text = "touch end" + event.touchPointID;
			if (e.touchPointID == stylusTouchID)
			{
				isDrawing = false;
			}
			
			for (var a:int = 0; a < activeTouchPoints.length; a++)
			{
				if (activeTouchPoints[a][0] == e.touchPointID)
				{
					activeTouchPoints.splice(a, 1);
					a--;
				}
			}
		}
		
		private var enterFrameCount:int = 0;
		
		private function doEnterFrame(e:Event):void
		{
			if (isDrawing)
			{
				enterFrameCount++;
				if (true || enterFrameCount > 5)
				{
					enterFrameCount = 0;
				}
				else
				{
					return;
				}
				var xmn:Number = drawingHolder.mouseX;
				var ymn:Number = drawingHolder.mouseY;
				var nowPoint:Point = new Point(xmn, ymn);
				
				if (lastPoint != null && lastPoint.x == xmn && lastPoint.y == ymn) return;
				
				setLineStyle(defaultLineThickness);
				
				
				if (!drawCurves || lastLastPoint == null || (lastPoint != null && lastPoint.x == nowPoint.x && lastPoint.y == nowPoint.y))
				{
					drawingLayer.graphics.moveTo(lastPoint.x, lastPoint.y);
					drawingLayer.graphics.lineTo(xmn, ymn);
				}
				else
				{
					// draw a curve
					var mainAngle:Number;
					var lastPointAngle:Number;
					//mainAngle = CollisionDetection.getAngle(new Point(0, 0), new Point( -10, -10), true, true);
					mainAngle = CollisionDetection.getAngle(lastPoint, nowPoint, true, true);
					lastPointAngle = CollisionDetection.getAngle(lastLastPoint, lastPoint, true, true);
					
					var angleDiff:Number = CollisionDetection.angleDiff(lastPointAngle, mainAngle);
					
					var xDiff:Number = nowPoint.x - lastPoint.x;
					var yDiff:Number = nowPoint.y - lastPoint.y;
					
					var length:Number = Utils.getDistance(lastPoint.x, lastPoint.y, nowPoint.x, nowPoint.y);				
					//trace(length);
					
					length *= 50;
					
					var oldLength:Number = Utils.getDistance(lastLastPoint.x, lastLastPoint.y, lastPoint.x, lastPoint.y);
					
					var lastPoint2:Point = new Point(lastPoint.x - (Math.cos(lastPointAngle * CollisionDetection.dg) * length), lastPoint.y - (Math.sin(lastPointAngle * CollisionDetection.dg) * length));
					
					var nowAngle:Number = CollisionDetection.getAngle(nowPoint, lastPoint, true, true);
					
					var nowPoint2:Point = new Point(lastPoint.x - (Math.cos((nowAngle - angleDiff) * CollisionDetection.dg) * length), lastPoint.y - (Math.sin((nowAngle - angleDiff) * CollisionDetection.dg) * length));
					
					var interception:* = CollisionDetection.isIntercepting(lastPoint, lastPoint2, nowPoint, nowPoint2);
					
					
					var foundInterception:Boolean = false;
					if (interception == false)
					{
						trace("not intercepting");
					}
					else if (interception == "Infinity")
					{
						trace("interception infinity");
					}
					else
					{
						foundInterception = true;
					}
					
					drawingLayer.graphics.moveTo(lastPoint.x, lastPoint.y);
					
					// && lastPoint2.y != lastPoint.y
					
					if (true && lastLastPoint != null && foundInterception && length > curveLengthTolerance)// && oldLength > curveLengthTolerance)
					{
						if (false && lastControlPoint != null)
						{
							drawingLayer.graphics.cubicCurveTo(lastControlPoint.x, lastControlPoint.y, interception.x, interception.y, nowPoint.x, nowPoint.y);
						}
						else
						{
							drawingLayer.graphics.curveTo(interception.x, interception.y, nowPoint.x, nowPoint.y);
						}
						
						if (false)
						{
							setLineStyle(1, "ff0000");
							drawingLayer.graphics.drawCircle(lastPoint2.x, lastPoint2.y, 2);
							drawingLayer.graphics.moveTo(lastPoint.x, lastPoint.y);
							drawingLayer.graphics.lineTo(lastPoint2.x, lastPoint2.y);
						}
						if (false)
						{
							setLineStyle(1, "00ff00");
							drawingLayer.graphics.drawCircle(nowPoint2.x, nowPoint2.y, 2);
							drawingLayer.graphics.moveTo(nowPoint.x, nowPoint.y);
							drawingLayer.graphics.lineTo(nowPoint2.x, nowPoint2.y);
						}
						
						lastControlPoint = interception;
					}
					else
					{					
						drawingLayer.graphics.lineTo(xmn, ymn);
						lastControlPoint = null;
					}
					
					
						
					
					
					
					
				}
				
				lastLastPoint = new Point(lastPoint.x, lastPoint.y);
				
				lastPoint = new Point(xmn, ymn);
			}
		}
		
		public function doClose(e:Event):void
		{
			dispatchEvent(new Event(Event.COMPLETE));
			destroy();
		}
		
		public function doSaveComplete():void
		{
			// override me
			dispatchEvent(new Event(Event.COMPLETE));
			txtStatus.text = "Save successful";
			destroy();
		}
		
		public function doSave(e:Event):void
		{
			var rect:Rectangle = this.getBounds(drawingHolder);
			var rect2:Rectangle = new Rectangle(0, 0, rect.width - rect.x, rect.height - rect.y);
			trace(rect.width, rect.height);
			var bmd:BitmapData = new BitmapData(rect.width, rect.height, true, 0xffffff);
			bmd.draw(drawingHolder);
			var bytes:ByteArray = PNGEncoder.encode(bmd);// jpgencoder.encode(bmd);// PNGEncoder.encode(bmd);
			//var ba:ByteArray = new ByteArray();
			//var rect:Rectangle = new Rectangle(0, 0, _camera.width, _camera.height);
			//_camera.copyToByteArray(rect, ba);
			
			if (saveToFilesystem)
			{
				filename = UtilsAirBase.instance.saveFile(bytes, "png");
				if (filename != "")
				{
					doSaveComplete();
					return;
				}
				else
				{
					Tracer.instance.doTrace("Save image failed.");
					return;
				}
			}
			
			txtStatus.text = "uploading to server...";
			
			var request:URLRequest = new URLRequest(uploadURL + "?nocache=" + (new Date().getTime()));			
			request.contentType = "application/octet-stream";
			request.method = URLRequestMethod.POST;
			request.data = bytes;
			
			destroyLoader();
			
			loader = new URLLoader();
			loader.dataFormat = URLLoaderDataFormat.BINARY;
			loader.addEventListener(Event.COMPLETE, doUploadComplete);
			loader.addEventListener(IOErrorEvent.IO_ERROR, doUploadError);
			//loader.addEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, doStatusEvent);
			loader.load(request);		
		}
		
		private function doUploadComplete(e:Event):void
		{
			var obj:Object = JSON.parse(e.target.data);
			txtStatus.text = (obj.success ? "Success!" : "Failue.");
			
			//saveLocalFile(obj.filename);
		}
		
		private function doUploadError(e:IOErrorEvent):void
		{
			txtStatus.text = e.text;
		}
		
		private function doStatusEvent(e:HTTPStatusEvent):void
		{
			Tracer.instance.doTrace(e.toString());
		}
		
		public function doClear(e:Event = null):void
		{
			isDrawing = false;
			drawingLayer.graphics.clear();
			setLineStyle(defaultLineThickness);
		}
		
		public function doStylusClick(e:Event):void
		{
			if (btnStylus.currentFrame == 1)
			{
				// enable touch
				usingStylus = false;
				btnStylus.gotoAndStop(2);
			}
			else
			{
				// enable stylus
				usingStylus = true;
				btnStylus.gotoAndStop(1);
			}
			
			setInputListeners();
		}
		
		public function setInputListeners():void
		{
			if (usingStylus)
			{
				Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
				DocClassBase.instance.stage.removeEventListener(MouseEvent.MOUSE_UP, doMouseUp);			
				background.removeEventListener(MouseEvent.MOUSE_DOWN, doMouseDown);
				drawingHolder.removeEventListener(MouseEvent.MOUSE_DOWN, doMouseDown);
				
				background.addEventListener(TouchEvent.TOUCH_BEGIN, onTouchBegin);
				drawingHolder.addEventListener(TouchEvent.TOUCH_BEGIN, onTouchBegin); 
				DocClassBase.instance.stage.addEventListener(TouchEvent.TOUCH_MOVE, onTouchMove); 
				DocClassBase.instance.stage.addEventListener(TouchEvent.TOUCH_END, onTouchEnd);
			}
			else
			{
				Multitouch.inputMode = MultitouchInputMode.NONE;
				background.removeEventListener(TouchEvent.TOUCH_BEGIN, onTouchBegin);
				drawingHolder.removeEventListener(TouchEvent.TOUCH_BEGIN, onTouchBegin); 
				DocClassBase.instance.stage.removeEventListener(TouchEvent.TOUCH_MOVE, onTouchMove); 
				DocClassBase.instance.stage.removeEventListener(TouchEvent.TOUCH_END, onTouchEnd);
				
				DocClassBase.instance.stage.addEventListener(MouseEvent.MOUSE_UP, doMouseUp);			
				background.addEventListener(MouseEvent.MOUSE_DOWN, doMouseDown);
				drawingHolder.addEventListener(MouseEvent.MOUSE_DOWN, doMouseDown);
			}
		}
		
		public function resize(e:Event = null):void
		{			
			background.width = DocClassBase.instance.stage.stageWidth;
			background.height = DocClassBase.instance.stage.stageHeight;
			
			btnClose.x = DocClassBase.instance.stage.stageWidth - btnClose.width - 10;
			btnClose.y = 10;
			
			btnSave.x = btnClose.x - btnSave.width - 10;
			btnSave.y = 10;
			
			btnStylus.x = btnSave.x - btnStylus.width - 10;
			btnStylus.y = 10;
			
			btnClear.x = btnStylus.x - btnClear.width - 10;
			btnClear.y = 10;
			
			txtStatus.x = 10;
			txtStatus.y = 10;
		}
		
		private function destroyLoader():void
		{
			if (loader != null)
			{
				loader.removeEventListener(Event.COMPLETE, doUploadComplete);
				loader.removeEventListener(IOErrorEvent.IO_ERROR, doUploadError);
				loader = null;
			}
		}
		
		public function destroy():void
		{
			destroyLoader();
			
			if (usingStylus)
			{				
				Multitouch.inputMode = MultitouchInputMode.NONE;
				background.removeEventListener(TouchEvent.TOUCH_BEGIN, onTouchBegin);
				drawingHolder.removeEventListener(TouchEvent.TOUCH_BEGIN, onTouchBegin); 
				DocClassBase.instance.stage.removeEventListener(TouchEvent.TOUCH_MOVE, onTouchMove); 
				DocClassBase.instance.stage.removeEventListener(TouchEvent.TOUCH_END, onTouchEnd);
			}
			else
			{
				DocClassBase.instance.stage.removeEventListener(MouseEvent.MOUSE_UP, doMouseUp);			
				background.removeEventListener(MouseEvent.MOUSE_DOWN, doMouseDown);
				drawingHolder.removeEventListener(MouseEvent.MOUSE_DOWN, doMouseDown);
			}
			
			removeEventListener(Event.ENTER_FRAME, doEnterFrame);
			btnClose.removeEventListener(MouseEvent.CLICK, doClose);
			
			background.removeEventListener(MouseEvent.MOUSE_DOWN, doMouseDown);
			drawingHolder.removeEventListener(MouseEvent.MOUSE_DOWN, doMouseDown);
			
			DocClassBase.instance.stage.removeEventListener(Event.RESIZE, resize);
			DocClassBase.instance.stage.removeEventListener(MouseEvent.MOUSE_UP, doMouseUp);
			
			btnClose.removeEventListener(MouseEvent.CLICK, doClose);
			btnSave.removeEventListener(MouseEvent.CLICK, doSave);
			btnClear.removeEventListener(MouseEvent.CLICK, doClear);
			
			Utils.removeAllChildren(this);
			if(parent != null) parent.removeChild(this);
		}	
		
	}

}