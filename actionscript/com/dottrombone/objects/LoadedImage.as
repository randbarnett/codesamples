package com.dottrombone.objects 
{
	/**
	 * ...
	 * @author Rand Barnett
	 */
	
	import com.dottrombone.Utils;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.IOErrorEvent;
	import flash.net.URLRequest;
	import flash.system.LoaderContext;
	import flash.events.Event;
	
	public class LoadedImage extends Sprite
	{
		private var imageLoader:Loader;
		private var isLoaded:Boolean = false;
		private var mRotation:Number = 0;
		private var mBmd:BitmapData = null;
		
		public function LoadedImage() 
		{
			imageLoader = new Loader();
		}
		
		public function load(url:String):void
		{
			var request:URLRequest = new URLRequest(url);
			var loaderContext:LoaderContext = new LoaderContext();
			loaderContext.allowCodeImport = true;
			imageLoader.load(request, loaderContext);
			imageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, doLoadComplete);
			imageLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, doLoadFailed);
		}
		
		private function doLoadFailed(e:Event):void
		{
			dispatchEvent(new Event(Event.CANCEL));
		}
		
		private function doLoadComplete(e:Event):void
		{
			isLoaded = true;
			addChild(imageLoader);
			doRotate();
			dispatchEvent(new Event(Event.COMPLETE));
		}
		
		public override function set rotation(n:Number):void
		{
			mRotation = n;
			doRotate();
		}
		
		public override function get rotation():Number
		{
			return mRotation;
		}
		
		public function get bitmapData():BitmapData
		{
			if (mBmd != null) return mBmd;
			return (imageLoader.contentLoaderInfo.content as Bitmap).bitmapData;
		}
		
		public function set bitmapData(bmd:BitmapData):void
		{
			Utils.removeAllDisplayObjects(this);
			addChild(new Bitmap(bmd));
			mBmd = bmd;
		}
		
		public function doRotate():void
		{
			if (!isLoaded) return;
			imageLoader.rotation = rotation;
			switch(imageLoader.rotation)
			{
				case 90:
					imageLoader.x = imageLoader.width;
					imageLoader.y = 0;
					break;
				case 180:
				case -180:
					imageLoader.x = imageLoader.width;
					imageLoader.y = imageLoader.height;
					break;
				case 270:
				case -90:
					imageLoader.x = 0;
					imageLoader.y = imageLoader.height;
					break;
				case 360:
				case 0:
					imageLoader.x = 0;
					imageLoader.y = 0;
					break;
			}
		}
		
	}

}