package com.dottrombone.objects 
{
	
	/**
	 * ...
	 * @author Rand Barnett
	 */
	
	import flash.display.MovieClip;
	import com.dottrombone.applications.DocClassBase;
	import com.dottrombone.assets.DTEvent;
	import com.dottrombone.Utils;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.utils.getTimer;
	import flash.utils.setTimeout;
	import flash.utils.clearTimeout;
	
	public class DTTouchButton extends DTMovieClip
	{
		public var clickTiming:int = 1000;
		public var holdTiming:int = 1500;
		public var doubleClickTiming:int = 300;
		public var clickStartTime:int = 0;
		public var isToggle:Boolean = false;
		public var isOn:Boolean = false;
		public var isEnabled:Boolean = true;
		
		private var lastClickTime:int = 0;
		private var clickTimeout:uint = 0;
		private var clickTimeoutTime:int = 300;
		
		public function DTTouchButton() 
		{
			addListeners();
		}
		
		public function doMouseDown(e:Event = null):void
		{
			clickStartTime = getTimer();
			
			//addEventListener(Event.ENTER_FRAME, doEnterFrame);
			
			turnOn(true);
		}
		
		private function doStageMouseUp(e:Event):void
		{
			// not sure how necessary this step is
			if (!isToggle && isOn)
			{
				turnOn(false);
			}
			
			removeEventListener(Event.ENTER_FRAME, doEnterFrame);
		}
		
		public function doMouseUp(e:MouseEvent):void
		{			
			//trace('mouseup');
			var globalPoint:Point = new Point(e.stageX, e.stageY);
						
			//this.visible = false;// good for troubleshooting
			if (getTimer() < clickStartTime + clickTiming)
			{
				doClick();
			}
			else
			{
				turnOn(false);
			}
			removeEventListener(Event.ENTER_FRAME, doEnterFrame);
		}
		
		public function doReleaseOutside(e:Event):void
		{
			if (!isToggle)
			{
				turnOn(false);
			}
			removeEventListener(Event.ENTER_FRAME, doEnterFrame);
		}
		
		private function doEnterFrame(e:Event):void
		{
			if (getTimer() > clickStartTime + holdTiming)
			{
				doHold();
				removeEventListener(Event.ENTER_FRAME, doEnterFrame);
			}
		}
		
		public function doHold():void
		{
			trace("doHold()");
			turnOn(false);
			dispatchEvent(new DTEvent(DTEvent.HOLD));
		}
		
		public function doClick():void
		{
			trace("doClick()");
			if (isToggle)
			{
				if (isOn)
				{
					turnOn(false);
				}
				else
				{
					turnOn(true);
					isOn = true;
				}
			}
			else
			{
				turnOn(false);
			}
			
			trace("dispatching event for: " + name);
			
			var thisClickTime:int = getTimer();
			if (thisClickTime - lastClickTime < doubleClickTiming)
			{
				clearTimeout(clickTimeout);
				lastClickTime = 0;
				dispatchEvent(new DTEvent(DTEvent.DOUBLE_CLICK));
			}
			else
			{
				clearTimeout(clickTimeout);
				clickTimeout = setTimeout(doClickTimeout, clickTimeoutTime);
				lastClickTime = getTimer();
			}
		}
		
		private function doClickTimeout():void
		{
			dispatchEvent(new DTEvent(DTEvent.CLICK));
		}
		
		public function turnOn(bool:Boolean):void
		{
			// override me
			if (!bool)
			{
				isOn = false;				
			}
			else
			{
				
			}
		}
		
		private function addListeners():void
		{
			addEventListener(MouseEvent.MOUSE_DOWN, doMouseDown);			
			addEventListener(MouseEvent.MOUSE_UP, doMouseUp);
			addEventListener(MouseEvent.RELEASE_OUTSIDE, doReleaseOutside);
			//DocClassBase.instance.stage.addEventListener(MouseEvent.MOUSE_UP, doStageMouseUp);
		}
		
		private function removeListeners():void
		{
			removeEventListener(MouseEvent.MOUSE_DOWN, doMouseDown);			
			removeEventListener(MouseEvent.MOUSE_UP, doMouseUp);
			removeEventListener(MouseEvent.RELEASE_OUTSIDE, doReleaseOutside);
			//DocClassBase.instance.stage.removeEventListener(MouseEvent.MOUSE_UP, doStageMouseUp);
		}
		
		public override function set enabled(bool:Boolean):void
		{
			super.enabled = bool;
			isEnabled = bool;
			if (bool)
			{
				addListeners();
			}
			else
			{
				removeListeners();
			}
		}
		
		public override function get enabled():Boolean
		{
			return isEnabled;
		}
		
		public override function destroy():void
		{
			
			removeListeners();
			
			removeEventListener(Event.ENTER_FRAME, doEnterFrame);
			
			Utils.removeAllChildren(this);
		}
		
	}

}