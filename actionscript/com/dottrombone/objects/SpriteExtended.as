package com.dottrombone.objects 
{
	/**
	 * ...
	 * @author Rand Barnett
	 */
	
	import flash.display.Sprite;
	import flash.geom.Point;
	
	public class SpriteExtended extends Sprite
	{
		
		public function SpriteExtended() 
		{
			
		}
		
		public function getScopedLines(A:Point = null, B:Point = null):Array
		{
			// override me
			return null;
		}
		
		public function getScopedPoints(A:Point = null, B:Point = null):Array
		{
			// override me
			return null;
		}
	}

}