package com.dottrombone.objects 
{
	import flash.events.AsyncErrorEvent;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	/**
	 * ...
	 * @author Rand Barnett
	 */
	public class DTVideo extends Video
	{
		
		private var mURL:String = "";
		private var mNetConnection:NetConnection;
		private var mNetStream:NetStream;
		
		public function DTVideo(pWidth:int = 1920, pHeight:int = 1080) 
		{
			super(pWidth, pHeight);
			mNetConnection = new NetConnection();
			mNetConnection.connect(null);
			
			mNetStream = new NetStream(mNetConnection);
			mNetStream.addEventListener(AsyncErrorEvent.ASYNC_ERROR, asyncErrorHandler);
			attachNetStream(mNetStream);
		}
		
		private function asyncErrorHandler(e:AsyncErrorEvent):void
		{
			//Tracer.instance.doTrace("Video error: " + e.error.message);
		}
		
		public function load(pURL:String):void
		{
			// this url could be external or local
			mURL = pURL;
		}
		
		public function play():void
		{
			mNetStream.play(mURL);
		}
		
	}

}