package com.dottrombone.objects 
{
	import com.dottrombone.Utils;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author Rand Barnett
	 */
	public class Checkbox extends MovieClip
	{
		
		public function Checkbox() 
		{
			chk.stop();
			text = "hey";
			addEventListener(MouseEvent.CLICK, doClick);
		}
		
		private function doClick(e:Event):void
		{
			if (checked)
			{
				checked = false;
			}
			else
			{
				checked = true;
			}
		}
		
		public function set text(str:String):void
		{
			txt.autoSize = "left";
			txt.text = str;
		}
		
		public function setTextColor(color:String):void
		{
			Utils.textfieldColor(txt, color);
		}
		
		public function set checked(bool:Boolean):void
		{
			chk.gotoAndStop((bool ? 2 : 1));
		}
		
		public function get checked():Boolean
		{
			return (chk.currentFrame == 1 ? false : true);
		}
		
	}

}