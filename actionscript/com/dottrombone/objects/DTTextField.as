package com.dottrombone.objects 
{
	/**
	 * ...
	 * @author Rand Barnett
	 */
	
	import com.dottrombone.Utils;
	import flash.geom.Point;
	import flash.text.Font;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	public class DTTextField extends TextField
	{
		public var snapToPixel:Boolean = true;
		private var mX:Number = 0;
		private var mY:Number = 0;
		
		private var storedPosition:Point = null;
		
		public function DTTextField(pSize:Number, pColor:String, pAlign:String = "left", pFont:Font = null)
		{
			
			if (pFont == null)
			{
				// default font for this project
				pFont = new Lato();
			}
			
			embedFonts = true;
			autoSize = pAlign;
			multiline = false;
			selectable = false;
			
			var textFormat:TextFormat = new TextFormat();
			
			textFormat.font = pFont.fontName;
			textFormat.size = pSize;
			textFormat.color = parseInt(pColor, 16);
			textFormat.letterSpacing = 1;
			
			if (pAlign == "center")
			{
				textFormat.align = TextFormatAlign.CENTER;
			}
			if (pAlign == "right")
			{
				textFormat.align = TextFormatAlign.RIGHT;
			}
			
			defaultTextFormat = textFormat;
		}
		
		public function set letterSpacing(n:Number):void
		{
			Utils.tfLetterSpacing(this, n);
		}
		
		public function set dummyText(str:String):void
		{
			embedFonts = false;
			var str2:String = "";
			for (var a:int = 0; a < str.length / 2; a++)
			{
				str2 += "█";
			}
			
			text = str;
		}
		
		// overriding x and y so we can always snap the graphic to the pixel for crispness (if we want to)
		public override function set x(n:Number):void
		{
			mX = n;
			if (snapToPixel)
			{
				super.x = Math.round(n);
			}
			else
			{
				super.x = n;
			}
		}
		
		public override function get x():Number
		{
			return mX;
		}
		
		public override function set y(n:Number):void
		{
			mY = n;
			if (snapToPixel)
			{
				super.y = Math.round(n);
			}
			else
			{
				super.y = n;
			}
		}
		
		public override function get y():Number
		{
			return mY;
		}
		
		public function storePosition():void
		{
			storedPosition = new Point(x, y);
		}
		
		public function restorePosition():void
		{
			if (storedPosition != null)
			{
				x = storedPosition.x;
				y = storedPosition.y;
			}
		}
		
		public function advancedFormat():void
		{
			antiAliasType = "advanced";
			gridFitType = "subpixel";
			
			sharpness = 200;// -400 to 400
			thickness = 100;// -200 to 200
		}
		
	}

}