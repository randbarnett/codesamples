package com.dottrombone.objects 
{
	/**
	 * ...
	 * @author Rand Barnett
	 */
	
	import com.dottrombone.bluetooth.Utils;
	import flash.display.MovieClip;
	import flash.events.*;
	import flash.text.TextField;
	
	public class GenericButton extends MovieClip
	{
		
		public function GenericButton() 
		{
			
		}
		
		public function set text(str:String):void
		{
			var tf:TextField = Utils.tf(20, "333333");
			tf.text = str;
			tf.x = (width / 2) - (tf.width / 2);
			tf.y = (height / 2) - (tf.height / 2);
			addChild(tf);
		}
		
	}

}