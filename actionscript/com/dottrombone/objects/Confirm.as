package com.dottrombone.objects 
{
	
	/**
	 * ...
	 * @author Rand Barnett
	 */
	
	import com.dottrombone.applications.DocClassBase;
	import com.dottrombone.applications.tdg.enterprise.Colors;
	import com.dottrombone.applications.tdg.enterprise.TDGLoginButton;
	import com.dottrombone.assets.DTEvent;
	import com.dottrombone.Utils;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class Confirm extends Alert
	{
		
		public var btnYes:MovieClip;
		public var btnNo:MovieClip;
		
		public function Confirm(str:String = "", buildButtons:Boolean = true) 
		{
			super(str);
			
			removeEventListener(MouseEvent.CLICK, doClick);
			
			if (buildButtons)
			{
				var buttonHeight:int = 25;
				btnNo = new TDGLoginButton() as MovieClip;
				(btnNo as TDGLoginButton).text = "Cancel";
				Utils.scaleToHeight(btnNo, DocClassBase.instance.stage.stageHeight / buttonHeight);
				btnNo.addEventListener(MouseEvent.CLICK, doNoClick);
				buttonHolder.addChild(btnNo);
				
				btnYes = new TDGLoginButton() as MovieClip;
				(btnYes as TDGLoginButton).text = "OK";
				Utils.scaleToHeight(btnYes, DocClassBase.instance.stage.stageHeight / buttonHeight);
				btnYes.x = btnNo.width + 50;
				btnYes.addEventListener(MouseEvent.CLICK, doYesClick);
				buttonHolder.addChild(btnYes);
				
				resize();
			}
			
		}
		
		public function doYesClick(e:Event):void
		{
			dispatchEvent(new DTEvent(DTEvent.COMPLETE));
			destroy();
		}
		
		public function doNoClick(e:Event):void
		{
			dispatchEvent(new DTEvent(DTEvent.CANCEL));
			destroy();
		}
		
		public override function destroy():void
		{
			btnNo.removeEventListener(MouseEvent.CLICK, doNoClick);
			btnYes.removeEventListener(MouseEvent.CLICK, doYesClick);
			super.destroy();
		}
		
	}

}