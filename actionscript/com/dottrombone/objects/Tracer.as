﻿package com.dottrombone.objects 
{
	/**
	 * ...
	 * @author Rand Barnett
	 */
	
	import fl.containers.ScrollPane;
	import flash.display.MovieClip;
	import flash.events.*;
	import flash.utils.getTimer;
	import com.dottrombone.applications.DocClassBase;
	import flash.text.TextField;
	
	public class Tracer extends MovieClip
	{
		
		public var isVisible:Boolean = false;// will only show on stage if true
		public var isPaused:Boolean = false;
		
		private static var _instance:Tracer = null;
		private var holder:MovieClip;
		private var dragging:Boolean = false;
		private var tf:TextField;
		private var firstClickTime:int = 0;
		private var resizing:Boolean = false;
		private var lastMouseX:Number;
		private var lastMouseY:Number;
		private var mScale:Number = 1;
		
		public function Tracer() 
		{
			
			y = 200;
			visible = isVisible;
			holder = new TracerMC();
			holder.scaleX = holder.scaleY = mScale;
			holder.visible = true;
			addChild(holder);
			
			holder.btnClose.addEventListener(MouseEvent.CLICK, doClose);
			holder.btnPause.addEventListener(MouseEvent.CLICK, doPause);
			holder.btnDelete.addEventListener(MouseEvent.CLICK, doDelete);
			holder.btnMinimize.addEventListener(MouseEvent.CLICK, doCollapse);
			holder.btnResize.addEventListener(MouseEvent.MOUSE_DOWN, doResize);
			
			holder.btnMinimize.gotoAndStop(1);
			holder.btnPause.gotoAndStop(1);
			
			holder.btnDrag.addEventListener(MouseEvent.MOUSE_DOWN, doStartDrag);
			holder.btnDrag.addEventListener(MouseEvent.DOUBLE_CLICK, doCollapse);
			
			addEventListener(Event.ADDED_TO_STAGE, setListeners);
			addEventListener(Event.ENTER_FRAME, doEnterFrame);
			
			var mc:MovieClip = new tracerTF();
			
			tf = mc.txt;
			tf.autoSize = "left";
			
			holder.txtName.text = "Debugging";
			holder.txtName.mouseEnabled = false;
			
			holder.daScrollPane.source = mc;
		}
		
		public function set title(str:String):void
		{
			holder.txtName.text = str;
		}
		
		public static function get instance():Tracer
		{
			if (_instance == null)
			{
				_instance = new Tracer();
				DocClassBase.instance.addChild(_instance);
			}
			
			return _instance;
		}
		
		private function setListeners(e:Event):void
		{
			stage.addEventListener(MouseEvent.MOUSE_UP, doMouseUp);
		}
		
		private function doClose(e:Event):void
		{
			isVisible = false;
			holder.visible = false;
		}
		
		public function hide():void
		{
			isVisible = true;
			visible = false;
		}
		
		public function unhide():void
		{
			holder.visible = true;
			visible = true;
		}
		
		public function show():void
		{
			unhide();
		}
		
		public function doTrace(str:String):void
		{
			trace(str);
			
			
			
			if (!isPaused)
			{
				var t:String = tf.text;
				t = str + '\n' + t;
				if (t.length > 10000) t = t.substr(0, 10000);
				tf.text = t;
				DocClassBase.instance.setChildIndex(this, DocClassBase.instance.numChildren - 1);
				(holder.daScrollPane as ScrollPane).update();
			}
		}
		
		private function doResize(e:Event):void
		{
			lastMouseX = mouseX;
			lastMouseY = mouseY;
			resizing = true;
		}
		
		private function doStartDrag(e:Event):void
		{
			var time:int = getTimer();
			if (time - firstClickTime < 300)
			{
				// double click
				doCollapse();
			}
			else
			{
				firstClickTime = time;
			}
			this.startDrag();
			dragging = true;
		}
		
		private function doMouseUp(e:Event):void
		{
			if (dragging)
			{
				dragging = false;
				this.stopDrag();
			}
			if (resizing)
			{
				resizing = false;
			}
		}
		
		private function doCollapse(e:Event = null):void
		{
			trace("Tracer doCollapse()");
			if (holder.mBackground.visible)
			{
				holder.mBackground.visible = false;
				holder.daScrollPane.visible = false;
				holder.btnMinimize.gotoAndStop(2);
				holder.btnResize.visible = false;
			}
			else
			{
				holder.mBackground.visible = true;
				holder.daScrollPane.visible = true;
				holder.btnMinimize.gotoAndStop(1);
				holder.btnResize.visible = true;
			}
		}
		
		private function doPause(e:Event):void
		{
			trace("doPause");
			if (isPaused)
			{
				isPaused = false;
				holder.btnPause.gotoAndStop(1);
			}
			else
			{
				isPaused = true;
				holder.btnPause.gotoAndStop(2);
			}
		}
		
		private function doDelete(e:Event):void
		{
			tf.text = "";
			holder.daScrollPane.update();
		}
		
		private function doEnterFrame(e:Event = null):void
		{
			
			if (resizing)
			{
				var xDiff:Number = mouseX - lastMouseX;
				var yDiff:Number = mouseY - lastMouseY;
				
				xDiff /= mScale;
				yDiff /= mScale;				
				
				if ((holder.width / mScale + xDiff) < 100) xDiff = 0;
				if ((holder.height / mScale + yDiff) < 50) yDiff = 0;
				
				holder.btnClose.x += xDiff;
				holder.btnPause.x += xDiff;
				holder.btnDelete.x += xDiff;
				holder.btnMinimize.x += xDiff;
				holder.btnResize.x += xDiff;
				holder.btnResize.y += yDiff;
				holder.btnDrag.width += xDiff;
				holder.mBackground.width += xDiff;
				holder.mBackground.height += yDiff;
				
				holder.daScrollPane.width += xDiff;
				holder.daScrollPane.height += yDiff;
				holder.daScrollPane.update();
				lastMouseX = mouseX;
				lastMouseY = mouseY;
			}
			
		}
	}

}