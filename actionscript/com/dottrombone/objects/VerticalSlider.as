﻿package com.dottrombone.objects{
	
	import flash.display.*;
	import flash.events.*;
	
	public class VerticalSlider extends MovieClip{
		
		public static const LEVEL_CHANGE:String="levelChange";
		
		public var percentage=0;
		private var drag:Boolean=false;
		 
		public function VerticalSlider():void{
			addEventListener(Event.ADDED_TO_STAGE,setListeners);
			volume=0;
			
		}
		
		private function setListeners(e:Event):void{
			sliderLever.addEventListener(MouseEvent.MOUSE_DOWN,doMouseDown);
			sliderLever.stage.addEventListener(MouseEvent.MOUSE_UP,doMouseUp);
		}
		private function doMouseDown(e:Event=null):void{
			if(drag){
				drag=false;
				removeEventListener(Event.ENTER_FRAME,doEnterFrame);
			}
			else{
				drag=true;
				addEventListener(Event.ENTER_FRAME,doEnterFrame);
			}
		}
		private function doMouseUp(e:Event=null):void{
			drag=false;
			removeEventListener(Event.ENTER_FRAME,doEnterFrame);
		}
		private function doEnterFrame(e:Event):void{
			if(drag){
				var ymn:Number=this.mouseY;
				if(ymn>sliderLine.y){
					ymn=sliderLine.y;
				}
				if(ymn<sliderLine.y-sliderLine.height){
					ymn=sliderLine.y-sliderLine.height;
				}
				sliderLever.y=ymn;
				percentage=(sliderLine.y-sliderLever.y)/sliderLine.height;
				dispatchEvent(new Event(VerticalSlider.LEVEL_CHANGE));
			}				
		}
		private function refreshSlider():void{
			sliderLever.y=sliderLine.y-(percentage*sliderLine.height);
		}
		public function set volume(n:Number){
			percentage=n;
			refreshSlider();
			dispatchEvent(new Event(VerticalSlider.LEVEL_CHANGE));
		}
		public function get volume(){
			return (percentage);
		}
	}
}





