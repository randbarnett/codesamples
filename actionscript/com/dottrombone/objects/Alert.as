package com.dottrombone.objects 
{
	/**
	 * ...
	 * @author Rand Barnett
	 */
	
	import com.dottrombone.applications.DocClassBase;
	import com.dottrombone.applications.tdg.enterprise.Colors;
	import com.dottrombone.Utils;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import com.dottrombone.assets.DTEvent;
	
	public class Alert extends MovieClip
	{		
		
		public var fontSize:int = 20;
		public var fontColor:String = "ffffff";
		public var boxColor:String = "515151";
		public var background:MovieClip;
		public var box:MovieClip;
		public var tf:TextField;
		public var boxPadding:int = 50;
		
		public var buttonHolder:MovieClip;
		
		public function Alert(str:String) 
		{
			// this will display an alert on the screen
			// override to style for each app
			
			background = Utils.fill("000000");
			background.alpha = .75;
			addChild(background);
			
			box = Utils.fill(boxColor);
			addChild(box);
			
			tf = Utils.tf(Colors.fontSizeConversion(fontSize), fontColor);
			tf.multiline = true;
			tf.htmlText = str;
			tf.selectable = false;
			addChild(tf);
			
			buttonHolder = new MovieClip();
			addChild(buttonHolder);
			
			addEventListener(MouseEvent.CLICK, doClick);
			DocClassBase.instance.addEventListener(DTEvent.RESIZE, resize);
			DocClassBase.instance.addChild(this);
			
			resize();
		}
		
		public function doClick(e:Event):void
		{
			dispatchEvent(new DTEvent(DTEvent.COMPLETE));
			destroy();
		}
		
		public function resize(e:Event = null):void
		{
			var w:Number = DocClassBase.instance.stage.stageWidth;
			var h:Number = DocClassBase.instance.stage.stageHeight;
			
			background.width = w;
			background.height = h;
			
			tf.width = w / 2;
			tf.x = (w / 2) - (tf.width / 2);
			tf.y = (h / 2) - (tf.height / 2);
			
			var buttonHolderHeight:Number = buttonHolder.height;
			if (buttonHolderHeight > 0) buttonHolderHeight += buttonHolder.height;
			buttonHolder.y = tf.y + tf.height + buttonHolder.height;
			
			box.width = tf.width + (boxPadding * 2);			
			if (box.width < buttonHolder.width)
			{
				box.width = buttonHolder.width * 1.1;
			}			
			box.height = tf.height + (boxPadding * 2) + buttonHolderHeight;
			box.x = tf.x - boxPadding;
			box.y = tf.y - boxPadding;
			
			buttonHolder.x = box.x + (box.width / 2) - (buttonHolder.width / 2);
			
		}
		
		public function destroy():void
		{			
			removeEventListener(MouseEvent.CLICK, doClick);
			DocClassBase.instance.stage.removeEventListener(Event.RESIZE, resize);
			DocClassBase.instance.removeChild(this);
		}
		
	}

}