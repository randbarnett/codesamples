package com.dottrombone.objects 
{
	/**
	 * ...
	 * @author Rand Barnett
	 */
	
	import com.dottrombone.applications.DocClassBase;
	import com.dottrombone.assets.DTEvent;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class GenericSlider extends MovieClip
	{
		
		private var isFlipped:Boolean = false;
		
		public function GenericSlider() 
		{
			mcKnob.addEventListener(MouseEvent.MOUSE_DOWN, doMouseDown);
		}
		
		private function doMouseDown(e:Event):void
		{
			doStartDrag();
		}
		
		private function doStopDrag(e:Event):void
		{
			removeEventListener(Event.ENTER_FRAME, doEnterFrame);
			DocClassBase.instance.stage.removeEventListener(MouseEvent.MOUSE_UP, doStopDrag);
		}
		
		private function doStartDrag():void
		{
			addEventListener(Event.ENTER_FRAME, doEnterFrame);
			DocClassBase.instance.stage.addEventListener(MouseEvent.MOUSE_UP, doStopDrag);
		}
		
		private function doEnterFrame(e:Event):void
		{
			var yP:Number = mouseY;
			if (yP > mcSliderBack.y + mcSliderBack.height) yP = mcSliderBack.y + mcSliderBack.height;
			if (yP < mcSliderBack.y) yP = mcSliderBack.y;
			
			mcKnob.y = yP;
			
			dispatchEvent(new DTEvent(DTEvent.CHANGE));
		}
		
		public function flip():void
		{
			if (isFlipped)
			{
				// back to vertical
				this.rotation = 0;
				mcMinus.rotation = 0;
				isFlipped = false;
			}
			else
			{
				if (this.rotation != 90) this.rotation = 90;
				mcMinus.rotation = 90;
				mcMinus.rotation = 90;
				isFlipped = true;
			}
		}
		
		public function get value():Number
		{
			return (mcSliderBack.height - (mcKnob.y - mcSliderBack.y)) / mcSliderBack.height;
		}
		
		public function set value(n:Number):void
		{
			mcKnob.y = mcSliderBack.height + mcSliderBack.y - (n * mcSliderBack.height)
		}
		
		public function destroy():void
		{
			mcKnob.removeEventListener(MouseEvent.MOUSE_DOWN, doMouseDown);
		}
		
	}

}