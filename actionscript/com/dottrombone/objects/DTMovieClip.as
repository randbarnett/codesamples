package com.dottrombone.objects 
{
	/**
	 * ...
	 * @author Rand Barnett
	 */
	
	import com.dottrombone.Utils;
	import com.dottrombone.assets.DTEvent;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.utils.setTimeout;
	
	public class DTMovieClip extends MovieClip
	{
		
		public var snapToPixel:Boolean = true;
		private var mX:Number = 0;
		private var mY:Number = 0;
		
		private var mWidthToSet:Number = 0;
		private var mHeightToSet:Number = 0;
		
		private var mcMask:DTMovieClip = null;
		private var mCropWidthToSet:Number = 0;
		private var mCropHeightToSet:Number = 0;
		
		public var isLoading:Boolean = false;
		public var isLoaded:Boolean = true;
		
		private var storedPosition:Point = null;
		
		public function DTMovieClip() 
		{
			
		}
		
		public override function set x(n:Number):void
		{
			mX = n;
			if (snapToPixel)
			{
				super.x = Math.round(n);
			}
			else
			{
				super.x = n;
			}
			
			if (mcMask != null)
			{
				mcMask.x = n;
				super.x = n + (mcMask.width / 2) - (width / 2);
			}
		}
		
		public override function get x():Number
		{
			return mX;
		}
		
		public override function set y(n:Number):void
		{
			mY = n;
			if (snapToPixel)
			{
				super.y = Math.round(n);
			}
			else
			{
				super.y = n;
			}
			
			if (mcMask != null)
			{
				mcMask.y = n;
				super.y = n + (mcMask.height / 2) - (height / 2);
			}
		}
		
		public override function get y():Number
		{
			return mY;
		}
		
		public function doLoadComplete(e:Event = null):void
		{
			
			
			if (mWidthToSet != 0 && mHeightToSet != 0 && isLoaded)
			{
				scaleX = scaleY = 1;
				width = mWidthToSet;
				height = mHeightToSet;
				
				mWidthToSet = 0;
				mHeightToSet = 0;
			}
			
			if (mCropWidthToSet != 0 && mCropHeightToSet != 0)
			{
				if (parent == null)
				{
					addEventListener(Event.ADDED_TO_STAGE, doLoadComplete);
				}
				else
				{
					crop(mCropWidthToSet, mCropHeightToSet);
				}
			}
			
			if (e != null)
			{
				removeEventListener(Event.ADDED_TO_STAGE, doLoadComplete);
			}
			
			dispatchEvent(new DTEvent(DTEvent.LOAD_COMPLETE));
		}
		
		public function doLoadCompleteWait():void
		{
			// waiting because I'm having problems with the image really being ready to resize
			// wasn't there an init event on loader?
			//setTimeout(doLoadComplete, 100);
			doLoadComplete();
		}
		
		public function setWidthAndHeight(w:Number, h:Number):void
		{
			if (isLoading)
			{
				mWidthToSet = w;
				mHeightToSet = h;
				// this is kind of a hack onload listener, might have to come back and clean this up
				//addEventListener(Event.ENTER_FRAME, doLoadEnterFrame);
			}
			else
			{
				width = w;
				height = h;
				
				mWidthToSet = 0;
				mHeightToSet = 0;
			}
		}
		
		private function doLoadEnterFrame(e:Event):void
		{
			if (width != 0 && height != 0)
			{
				width = mWidthToSet;
				height = mHeightToSet;
				removeEventListener(Event.ENTER_FRAME, doLoadEnterFrame);
			}
		}
		
		public function crop(w:Number, h:Number):void
		{
			if (width == 0 || height == 0)
			{
				mCropWidthToSet = w;
				mCropHeightToSet = h;
				return;
			}
			
			if (parent == null)
			{
				mCropWidthToSet = w;
				mCropHeightToSet = h;
				addEventListener(Event.ADDED_TO_STAGE, doLoadComplete);
				return;
			}
			
			if (mask == null)
			{
				mcMask = Utils.fill();
				parent.addChild(mcMask);
				mask = mcMask;
			}
			
			Utils.crop(this, w, h);
			mcMask.width = w;
			mcMask.height = h;
			
			x = mX;
			y = mY;
			
			addEventListener(Event.ADDED_TO_STAGE, doAddedToStageCrop);
		}
		
		private function doAddedToStageCrop(e:Event):void
		{
			if (mcMask != null)
			{
				if (mcMask.parent == null)
				{
					parent.addChild(mcMask);
				}
			}
		}
		
		public function clone():DTMovieClip
		{
			var mc:DTMovieClip = new DTMovieClip();
			
			if (width == 0 || height == 0)
			{
				// this is going to fail
				// send back null so that we know it failed
				return null;
			}
			var bitmapData:BitmapData = new BitmapData(this.width, this.height);
			bitmapData.draw(this);
			
			var bitmap:Bitmap = new Bitmap(bitmapData);
			mc.addChild(bitmap);
			
			return mc;
		}
		
		public function storePosition():void
		{
			storedPosition = new Point(x, y);
		}
		
		public function restorePosition():void
		{
			if (storedPosition != null)
			{
				x = storedPosition.x;
				y = storedPosition.y;
			}
		}
		
		public function destroy():void
		{
			if (mcMask != null)
			{
				removeEventListener(Event.ADDED_TO_STAGE, doLoadComplete);
				removeEventListener(Event.ADDED_TO_STAGE, doAddedToStageCrop);
				if (mcMask.parent != null)
				{
					mcMask.parent.removeChild(mcMask);
				}
			}
			
			removeEventListener(Event.ENTER_FRAME, doLoadEnterFrame);
		}
		
	}

}