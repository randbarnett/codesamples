﻿package com.dottrombone.objects 
{
	/**
	 * ...
	 * @author Rand Barnett
	 */
	
	//import com.dottrombone.assets.Downloader;
	import com.dottrombone.applications.DocClassBase;
	import com.dottrombone.assets.DTEvent;
	import com.dottrombone.Utils;
	import com.dottrombone.UtilsAir;
	import com.dottrombone.UtilsAirBase;
	import fl.accessibility.DataGridAccImpl;
	import fl.controls.ProgressBar;
	import flash.display.MovieClip;
	import flash.events.*;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLLoaderDataFormat;
	import flash.utils.ByteArray;
	import com.dottrombone.objects.MixerColumn;
	import flash.media.Microphone;
	import flash.utils.getTimer;
	import fr.kikko.lab.ShineMP3Encoder;
	import org.bytearray.micrecorder.encoder.WaveEncoder;
	
	public class AudioRecorder extends MovieClip
	{
		public var saveToFilesystem:Boolean = true;
		public var autoSave:Boolean = false;
		public var saveAsWav:Boolean = true;
		
		public var soundFilename:String = "";
		
		private var isRecording:Boolean = false;
		private var mIsPlaying:Boolean = false;
		private var mic:Microphone;
		private var channel:SoundChannel;
		private var recordingBytes:ByteArray = new ByteArray();
		private var sound:Sound;
		private var mp3Encoder:ShineMP3Encoder;
		private var waveEncoder:WaveEncoder;
		private var savedSound:Sound;
		private var background:MovieClip;
		private var loader:URLLoader;
		private var uploadURL:String;
		public var progress:Number = 0;
		public var mp3bytes:ByteArray = null;
		
		public function AudioRecorder() 
		{			
			uploadURL = DocClassBase.globals.baseURL + "/app/mp3upload.php";
			
			mic = Microphone.getMicrophone();
			mic.setSilenceLevel(0); 
			mic.gain = 70; 
			mic.rate = 44;
			
			mcMixer.manual = true;
			mcMixer.gain = .7;
			mcMixer.showGain();
			mcMixer.volume = 1;
			mic.addEventListener(SampleDataEvent.SAMPLE_DATA, micSampleDataHandler);
			
			sound = new Sound();
			sound.addEventListener(SampleDataEvent.SAMPLE_DATA, playbackSampleHandler);
			channel = new SoundChannel();
			channel.addEventListener( Event.SOUND_COMPLETE, playbackComplete );
			
			progressBar.visible = false;
			(progressBar as ProgressBar).mode = "manual";
			
			//txtFilename.text = "my recording";
			
			btnRecord.addEventListener(MouseEvent.CLICK, doRecordClick);
			btnSave.addEventListener(MouseEvent.CLICK, doSaveClick);
			btnStop.addEventListener(MouseEvent.CLICK, doStopClick);
			btnPlay.addEventListener(MouseEvent.CLICK, doPlayClick);
			btnClose.addEventListener(MouseEvent.CLICK, doCloseClick);
			
			background = Utils.fill("ffffff");
			background.width = width + 50;
			background.height = height + 30;
			addChildAt(background, 0);
			
			addEventListener(Event.ENTER_FRAME, doEnterFrame);
		}
		
		private function doRecordClick(e:Event):void
		{
			txtStatus.text = "";
			if (isRecording)
			{
				doStopClick();
				return;
			}
			recordingBytes = new ByteArray();
			isRecording = true;
			 
		}
		
		public function doSaveComplete():void
		{
			// override me
			dispatchEvent(new Event(Event.COMPLETE));
			txtStatus.text = "Save successful";
		}
		
		private function doSaveClick(e:Event):void
		{
			waveEncoder = new WaveEncoder();
			var bytes:ByteArray = waveEncoder.encode(recordingBytes, 2);
			
			if (saveToFilesystem)
			{
				if (saveAsWav)
				{
					soundFilename = UtilsAirBase.instance.saveFile(bytes, "wav");
					if (soundFilename != "")
					{
						doSaveComplete();
						return;
					}
				}
			}
			else
			{
				encode(bytes);
			}
			
		}
		
		public function encodeFromFile(filename:String):void
		{
			var ba:ByteArray = UtilsAirBase.instance.getFile(filename);
			if (ba == null)
			{
				var e:DTEvent = new DTEvent(DTEvent.ERROR);
				e.message = "File not found.";
				dispatchEvent(e);
			}
			else
			{
				encode(ba);
			}
		}
		
		public function encode(wavBytes:ByteArray):void
		{
			destroyMP3Encoder();
			
			mp3bytes = null;
			progress = 0;
			
			txtStatus.text = "Encoding mp3";
			progressBar.visible = true;
			(progressBar as ProgressBar).setProgress(0, 1);
			mp3Encoder = new ShineMP3Encoder(wavBytes);
            mp3Encoder.addEventListener(Event.COMPLETE, mp3EncodeComplete);
            mp3Encoder.addEventListener(ProgressEvent.PROGRESS, mp3EncodeProgress);
            mp3Encoder.addEventListener(ErrorEvent.ERROR, mp3EncodeError);
            mp3Encoder.start();
		}
		
		private function mp3EncodeComplete(e:Event):void
		{
			txtStatus.text = "mp3 encoding complete, saving...";
			(progressBar as ProgressBar).setProgress(1, 1);
			
			//var fullname:String = Downloader.saveFile(mp3Encoder.mp3Data, txtFilename.text);
			txtStatus.text = "file saved, creating sound...";
			//var request:URLRequest = new URLRequest(fullname);
			savedSound = new Sound();
			//savedSound.addEventListener(Event.COMPLETE, readyToOpenSound);
			//savedSound.load(request);
			//var ba:ByteArray = mp3Encoder.mp3Data;
			//ba.position = 0;
			//savedSound.loadCompressedDataFromByteArray(ba, ba.length);
			readyToOpenSound();
		}
		
		private function readyToOpenSound(e:Event = null):void
		{
			mp3bytes = mp3Encoder.mp3Data;
			if (saveToFilesystem)
			{
				dispatchEvent(new DTEvent(DTEvent.COMPLETE));
				return;
			}
			txtStatus.text = "uploading to server...";
			
			var request:URLRequest = new URLRequest(uploadURL);			
			request.contentType = "application/octet-stream";
			request.method = URLRequestMethod.POST;
			request.data = mp3Encoder.mp3Data;
			
			destroyLoader();
			
			loader = new URLLoader();
			loader.dataFormat = URLLoaderDataFormat.BINARY;
			loader.addEventListener(Event.COMPLETE, doUploadComplete);
			loader.addEventListener(IOErrorEvent.IO_ERROR, doUploadError);
			loader.load(request);		
		}
		
		private function doUploadComplete(e:Event):void
		{
			var obj:Object = JSON.parse(e.target.data);
			txtStatus.text = (obj.success ? "Success!" : "Failue.");			
			//saveLocalFile(obj.filename);
			
			var e2:DTEvent;
			if (obj.success)
			{
				e2 = new DTEvent(DTEvent.COMPLETE);
				e2.message = "File upload successful.";
			}
			else
			{
				e2 = new DTEvent(DTEvent.ERROR);
				e2.message = "File upload failed (on server).";
			}
			dispatchEvent(e2);
		}
		
		private function saveLocalFile(filename:String):void
		{
			var mp3Bytes:ByteArray = mp3Encoder.mp3Data;
		}
		
		private function doUploadError(e:IOErrorEvent):void
		{
			txtStatus.text = e.text;
			var e2:DTEvent = new DTEvent(DTEvent.ERROR);
			e2.message = "File upload failed (not 200).";
			dispatchEvent(e2);
		}
		
		private function mp3EncodeProgress(e:ProgressEvent):void
		{
			progress = e.bytesLoaded / e.bytesTotal;
			(progressBar as ProgressBar).setProgress(e.bytesLoaded / e.bytesTotal, 1);
			dispatchEvent(new DTEvent(DTEvent.PROGRESS));
		}
		
		private function mp3EncodeError(e:ErrorEvent):void
		{
			txtStatus.text = e.text;
			var e2:DTEvent = new DTEvent(DTEvent.ERROR);
			e2.message = "MP3 encoding error.";
			dispatchEvent(e2);
		}
		
		private function doStopClick(e:Event = null):void
		{
			if (isRecording)
			{
				isRecording = false;
			}
			
			if (mIsPlaying)
			{
				mIsPlaying = false;
				channel.stop();
			}
			
			btnRecord.dot.visible = true;
		}
		
		private function doPlayClick(e:Event):void
		{
			doStopClick();
			mIsPlaying = true;
			
			recordingBytes.position = 0; 
			  
			channel = sound.play();
		}
		
		private function playbackComplete(e:Event):void
		{
			doStopClick();
		}
		
		public function doEnterFrame(e:Event):void
		{
			//trace(mic.activityLevel);
			mic.gain = mcMixer.gain * 100;
			if (!mIsPlaying)
			{
				mcMixer.setLevelMeter(mic.activityLevel / 100);
			}
			else
			{
				mcMixer.setLevelMeter((channel.leftPeak + channel.rightPeak) / 2);
			}
			
			if (isRecording)
			{
				var time:int = getTimer();
				time = Math.round(time / 500);
				if (time % 2 == 0)
				{
					btnRecord.dot.visible = false;
				}
				else
				{
					btnRecord.dot.visible = true;
				}
			}
			if (mIsPlaying)
			{
				Utils.setVolume(channel, mcMixer.volume);
			}
		}	
		
		private function micSampleDataHandler(e:SampleDataEvent):void
		{
			//trace("sample data event");
			if (isRecording)
			{
				while(e.data.bytesAvailable) 
				{ 
					var sample:Number = e.data.readFloat(); 
					recordingBytes.writeFloat(sample);
					recordingBytes.writeFloat(sample); 
				} 
			}
		}
		
		private function playbackSampleHandler(e:SampleDataEvent):void 
		{ 
			for (var i:int = 0; i < 8192 && recordingBytes.bytesAvailable > 0; i++) 
			{ 
				//trace(sample); 
				var sample:Number = recordingBytes.readFloat(); 
				e.data.writeFloat(sample); 
			} 
		}
		
		private function doCloseClick(e:Event):void
		{
			doStopClick();
			destroy();
			parent.removeChild(this);
		}
		
		private function destroyLoader():void
		{
			if (loader != null)
			{
				loader.removeEventListener(Event.COMPLETE, doUploadComplete);
				loader.removeEventListener(IOErrorEvent.IO_ERROR, doUploadError);
				loader = null;
			}
		}
		
		private function destroyMP3Encoder():void
		{
			if (mp3Encoder != null)
			{
				mp3Encoder.removeEventListener(Event.COMPLETE, mp3EncodeComplete);
				mp3Encoder.removeEventListener(ProgressEvent.PROGRESS, mp3EncodeProgress);
				mp3Encoder.removeEventListener(ErrorEvent.ERROR, mp3EncodeError);
				mp3Encoder = null;
			}
		}
		
		public function destroy():void
		{
			destroyMP3Encoder();
			destroyLoader();
			
			mic.removeEventListener(SampleDataEvent.SAMPLE_DATA, micSampleDataHandler);
			sound.removeEventListener(SampleDataEvent.SAMPLE_DATA, playbackSampleHandler);
			channel.removeEventListener( Event.SOUND_COMPLETE, playbackComplete );
			
			btnRecord.removeEventListener(MouseEvent.CLICK, doRecordClick);
			btnSave.removeEventListener(MouseEvent.CLICK, doSaveClick);
			btnStop.removeEventListener(MouseEvent.CLICK, doStopClick);
			btnPlay.removeEventListener(MouseEvent.CLICK, doPlayClick);
			btnClose.removeEventListener(MouseEvent.CLICK, doCloseClick);
			
			removeEventListener(Event.ENTER_FRAME, doEnterFrame);
		}
	}
	
}