package com.dottrombone.objects 
{
	/**
	 * ...
	 * @author Rand Barnett
	 */
	
	import com.dottrombone.applications.DocClassBase;
	import com.dottrombone.Utils;
	import flash.display.ActionScriptVersion;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.media.Camera;
	import flash.media.Video;
	import com.adobe.images.JPGEncoder;
	import com.adobe.images.PNGEncoder;
	import flash.utils.ByteArray;
	import com.dottrombone.UtilsAirBase;
	import com.dottrombone.assets.DTEvent;
	
	public class Camera extends MovieClip
	{		
		public var saveToFilesystem:Boolean = true;
		public var autoSave:Boolean = false;
		
		public var imageFilename:String = "";
		public var imageFileBytes:ByteArray = null;
		public var pictureData:BitmapData = null;
		
		public var _camera:flash.media.Camera = null;
		private var _video:Video = null;
		private var picture:MovieClip = null;		
		private var currentCamera:int = -1;
		
		public var background:MovieClip;
		private var btnCamera:MovieClip;
		private var btnClose:MovieClip;
		private var btnFlip:MovieClip;
		private var btnSave:MovieClip;
		private var btnDiscard:MovieClip;
		
		public function Camera(newSkin:Boolean = false) 
		{
			background = Utils.fill("000000");
			addChild(background);
			
			if (!newSkin)
			{
				_video = new Video();
				addChildAt(_video, 1);
				
				picture = new MovieClip();
				addChildAt(picture, 2);
				
				btnCamera = new CameraButton();
				addChild(btnCamera);
				Utils.scaleToWidth(btnCamera, 150);
				btnCamera.alpha = .5;
				
				btnFlip = new FlipButton();
				addChild(btnFlip);
				Utils.scaleToWidth(btnFlip, 150);
				btnFlip.alpha = .5;
				
				btnClose = new CloseButton();
				addChild(btnClose);
				
				btnSave = new SaveButton();
				addChild(btnSave);
				
				btnDiscard = new DiscardButton();
				addChild(btnDiscard);
			}
			
			btnCamera.addEventListener(MouseEvent.CLICK, doCameraClick);
			btnFlip.addEventListener(MouseEvent.CLICK, doFlipClick);
			btnClose.addEventListener(MouseEvent.CLICK, doCloseClick);
			btnSave.addEventListener(MouseEvent.CLICK, doSaveClick);
			btnDiscard.addEventListener(MouseEvent.CLICK, doDiscardClick);
			
			// connect to camera
			var camToConnectTo:flash.media.Camera = null;
			var storedCamera:String = Utils.getStoredValue("currentCamera");
			var cameraToConnect:int = 0;
			var didConnectCamera:Boolean = false;
			if (storedCamera != "")
			{
				cameraToConnect = parseInt(storedCamera, 10);
				if (flash.media.Camera.names.length > cameraToConnect)
				{
					_camera = flash.media.Camera.getCamera(cameraToConnect.toString());
					Utils.setStoredValue("lastConnectedCamera", cameraToConnect.toString());
					currentCamera = cameraToConnect;
					didConnectCamera = true;
				}
			}
			
			if (!didConnectCamera)
			{
				for(var i:int = 0; i < flash.media.Camera.names.length; i++)
				{
					_camera = flash.media.Camera.getCamera(i.toString());
					currentCamera = i;
					Utils.setStoredValue("lastConnectedCamera", i.toString());
					break;
				}
			}
			
			if (_camera != null)
			{
				displayVideo();
			}
			
			resize();
			DocClassBase.instance.addEventListener(DTEvent.RESIZE, resize);
		}
		
		private function displayVideo():void
		{
			trace("displayVideo()");
			
			// Attach camera
			removeChild(_video);
			focusCamera();
			_video = new Video(_camera.width, _camera.height);
			_video.attachCamera(_camera);
			addChildAt(_video, 1);
			Utils.bestFit(_video, DocClassBase.instance.stage.stageWidth, DocClassBase.instance.stage.stageHeight);
		}
		
		public function focusCamera():void
		{
			_camera.setMode(DocClassBase.instance.stage.stageWidth, DocClassBase.instance.stage.stageHeight, 30);
		}
		
		private function doCameraClick(e:Event):void
		{			
			if (_camera == null) return;
			
			pictureData = new BitmapData(_camera.width, _camera.height, false, 0x000000);
			_camera.drawToBitmapData(pictureData);
			
			if (autoSave)
			{
				
			}
			
			Utils.removeAllChildren(picture);
			picture.addChild(createBitmap(pictureData));
			Utils.bestFit(picture, DocClassBase.instance.stage.stageWidth, DocClassBase.instance.stage.stageHeight, true);
			
			_video.visible = false;
			picture.visible = true;
			
			/*
			var bytes:ByteArray = bmd.getPixels(bmd.rect);
			request.data = bytes;
			request.url = URL + "?width=" + _camera.width + "&height=" + _camera.height;
			loader.load(request);
			
			
			
			 * var bytes:ByteArray = PNGEncoder.encode(bmd);// jpgencoder.encode(bmd);// PNGEncoder.encode(bmd);
			//var ba:ByteArray = new ByteArray();
			//var rect:Rectangle = new Rectangle(0, 0, _camera.width, _camera.height);
			//_camera.copyToByteArray(rect, ba);
			request.data = bytes;
			loader.load(request);
			*/
		}
		
		private function createBitmap(bmd:BitmapData):Bitmap
		{
			var bitmap:Bitmap = new Bitmap(bmd);
			bitmap.smoothing = true;
			//bitmap.scaleX = bitmap.scaleY = 0.2;
			return bitmap;
		}
		
		private function doFlipClick(e:Event):void
		{
			currentCamera++;
			if (currentCamera >= flash.media.Camera.names.length) currentCamera = 0;
			if (flash.media.Camera.names.length > currentCamera)
			{
				_camera = flash.media.Camera.getCamera(currentCamera.toString());
				Utils.setStoredValue("lastConnectedCamera", currentCamera.toString());
				displayVideo();
			}
					
		}
		
		private function doCloseClick(e:Event):void
		{
			destroy();
			parent.removeChild(this);
		}
		
		private function doSaveClick(e:Event = null):void
		{
			if (pictureData == null)
			{
				return;
			}
			
			imageFileBytes = PNGEncoder.encode(pictureData);// jpgencoder.encode(bmd);// PNGEncoder.encode(bmd);
			
			if (saveToFilesystem)
			{
				
				imageFilename = UtilsAirBase.instance.saveFile(imageFileBytes, "png");
			
				if (imageFilename != "")
				{
					// save complete
					// dispatch an event
					doSaveComplete();
					
					// all done, display the camera again, but don't discard the data
					picture.visible = false;
					_video.visible = true;
					return;
				}
				else
				{
					Tracer.instance.doTrace("Save camera image failed.");
					
					// give up and hope the tracer message was seen
					picture.visible = false;
					_video.visible = true;
					return;
				}
			}
			
			doSaveComplete();
			
		}
		
		public function doSaveComplete():void
		{
			// override me or catch the complete event
			dispatchEvent(new Event(Event.COMPLETE));
		}
		
		private function doDiscardClick(e:Event):void
		{
			pictureData = null;
			picture.visible = false;
			_video.visible = true;
		}
		
		public function resize(e:Event = null):void
		{
			var w:Number = DocClassBase.instance.stage.stageWidth;
			var h:Number = DocClassBase.instance.stage.stageHeight;
			
			background.width = w;
			background.height = h;
			
			btnCamera.x = w - btnCamera.width - 20;
			btnCamera.y = (h / 2) + btnCamera.height + 20;
			
			btnFlip.x = w - btnFlip.width - 20;
			btnFlip.y = (h / 2) - btnFlip.height - 20;
			
			btnClose.x = w - btnClose.width - 20;
			btnClose.y = 20;
			
			btnSave.x = btnClose.x - 20 - btnSave.width;
			btnSave.y = 20;
			
			btnDiscard.x = btnSave.x - 20 - btnDiscard.width;
			btnDiscard.y = 20;
			
		}
		
		private function destroy():void
		{
			btnCamera.removeEventListener(MouseEvent.CLICK, doCameraClick);
			btnFlip.removeEventListener(MouseEvent.CLICK, doFlipClick);
			btnClose.removeEventListener(MouseEvent.CLICK, doCloseClick);
			btnSave.removeEventListener(MouseEvent.CLICK, doSaveClick);
			btnDiscard.removeEventListener(MouseEvent.CLICK, doDiscardClick);
			
			DocClassBase.instance.stage.removeEventListener(Event.RESIZE, resize);
			
			imageFileBytes = null;
			pictureData = null;
		}
		
	}

}