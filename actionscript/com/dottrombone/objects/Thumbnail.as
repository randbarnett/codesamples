package com.dottrombone.objects 
{
	/**
	 * ...
	 * @author Rand Barnett
	 */
	
	import com.dottrombone.applications.DocClassBase;
	import com.dottrombone.objects.Tracer;
	import com.dottrombone.Utils;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLRequest;
	
	public class Thumbnail extends MovieClip
	{
		
		public var isLoaded:Boolean = false;
		public var mWidth:Number = 0;
		public var mHeight:Number = 0;
		
		public var loader:Loader;
		public var url:String;
		
		public var doubleSize:Boolean = false;
		
		public function Thumbnail(mURL:String, w:Number = 0, h:Number = 0, mDoubleSize:Boolean = false) 
		{
			doubleSize = mDoubleSize;
			load(mURL, w, h);
			
		}
		
		public function load(mURL:String, w:Number = 0, h:Number = 0):void
		{
			trace("loading: " + mURL);
			url = mURL;
			mWidth = w;
			mHeight = h;
			
			destroyLoader();
			
			isLoaded = false;
			loader = new Loader();
			addChild(loader);			
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, doLoadComplete);
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, doLoadError);
			loader.load(new URLRequest(url));
		}
		
		private function doLoadComplete(e:Event):void
		{
			//trace("doLoadComplete");
			isLoaded = true;
			resize();
		}
		
		private function doLoadError(e:Event):void
		{
			Tracer.instance.doTrace("Thumbnail load failed.");
		}
		
		public function setSize(w:Number = 0, h:Number = 0):void
		{
			mWidth = w;
			mHeight = h;
			resize();
		}
		
		public function resize(e:Event = null):void
		{
			if (!isLoaded) return;
			
			var multiplier:Number = 1;
			
			if (mWidth == 0 && mHeight == 0)
			{
				if (doubleSize)
				{
					Utils.bestFit(this, DocClassBase.instance.stage.stageWidth, DocClassBase.instance.stage.stageHeight);
				}
				return;
			}
			
			scaleX = scaleY = 1;
			
			
			if (mHeight == 0)
			{
				Utils.scaleToWidth(this, mWidth * multiplier);
			}
			else if (mWidth == 0)
			{
				Utils.scaleToHeight(this, mHeight * multiplier);
			}
			else
			{
				width = mWidth * multiplier;
				height = mHeight * multiplier;
			}
		}
		
		public function destroyLoader():void
		{
			if (loader != null)
			{
				loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, doLoadComplete);
				loader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, doLoadError);
				if(!isLoaded) loader.close();
				removeChild(loader);
				loader = null;
			}
		}
		
		public function destroy():void
		{
			destroyLoader();
		}
		
	}

}