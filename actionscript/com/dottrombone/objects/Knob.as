﻿package com.dottrombone.objects{
	
	import flash.display.*;
	import flash.events.*;
	
	public class Knob extends MovieClip{
		
		public static const LEVEL_CHANGE:String="panChange";
		private var startingDegrees:Number=-150;
		private var range:Number=-1*(startingDegrees*2);
		private var _level2:Number=0;
		private var yO:Number;
		private var startingLevel:Number;
		private var sensitivity:Number=.01;
		
		public function Knob(){
			level=.5;
			addEventListener(Event.ADDED_TO_STAGE,setListeners);
		}
		private function setListeners(e:Event):void{
			addEventListener(MouseEvent.MOUSE_DOWN,doMouseDown);
			stage.addEventListener(MouseEvent.MOUSE_UP,doMouseUp);
		}
		public function set level(n:Number):void{
			if(n<0){
				n=0;
			}
			if(n>1){
				n=1;
			}
			_level2=n;
			
			rotation=startingDegrees+(n*range);
			dispatchEvent(new Event(LEVEL_CHANGE));
		}
		public function get level():Number{
			return(_level2);
		}
		private function doMouseDown(e:Event):void{
			addEventListener(Event.ENTER_FRAME,doEnterFrame);
			yO=parent.mouseY;
			startingLevel=level;
		}
		private function doMouseUp(e:Event):void{
			removeEventListener(Event.ENTER_FRAME,doEnterFrame);
		}
		private function doEnterFrame(e:Event):void{
			var n:Number=startingLevel+((yO-parent.mouseY)*sensitivity);
			level=n;
			//trace(level);
		}
	}
}









