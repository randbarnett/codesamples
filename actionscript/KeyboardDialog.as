package com.dottrombone.applications.conflict_simulator.dialogs 
{
	/**
	 * ...
	 * @author Rand Barnett
	 */
	
	import com.dottrombone.Utils;
	import com.dottrombone.applications.DocClassBase;
	import com.dottrombone.applications.conflict_simulator.AppData;
	import com.dottrombone.applications.conflict_simulator.DocClass;
	import com.dottrombone.applications.conflict_simulator.Settings;
	import com.dottrombone.applications.conflict_simulator.dialogs.PopUpFullscreenBase;
	import com.dottrombone.applications.conflict_simulator.objects.Button;
	import com.dottrombone.applications.conflict_simulator.objects.Checkbox;
	import com.dottrombone.applications.conflict_simulator.objects.TestOverlay;
	import com.dottrombone.applications.conflict_simulator.objects.UIButton;
	import com.dottrombone.assets.DTEvent;
	import com.dottrombone.objects.DTMovieClip;
	import com.dottrombone.objects.DTTextField;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class KeyboardDialog extends PopUpFullscreenBase
	{
		
		public var email:String = "";
		
		private var keys:Array = new Array([["Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P"], ["A", "S", "D", "F", "G", "H", "J", "K", "L"], ["backspace", "Z", "X", "C", "V", "B", "N", "M", "backspace"], ["_123", "@", "space", ".", ".COM"]], [["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"], ["`", "~", "!", "#", "$", "%", "^", "&", "*"], ["+", "=", "{", "}", "|", "'", "?", "/", "backspace"], ["ABC", "-", "space", "_", ".COM"]]);
		private var numberKeys:Array = new Array();
		
		private var contentHolder:DTMovieClip// everything goes in here so we can center it
		private var keyboardHolder:DTMovieClip;
		private var normalKeyboard:DTMovieClip;
		private var specialKeyboard:DTMovieClip;
		public var checkbox:Checkbox;
		private var mcKeyboardStuff:DTMovieClip;
		private var mcThankYouStuff:DTMovieClip;
		
		private var normalKeyArray:Array = new Array();// so we can destroy the listeners
		private var specialKeyArray:Array = new Array();// multidimensional array holding special key/functions
		
		private var tfInput:DTTextField;
		private var btnBack:Button;
		private var btnNext:Button;
		private var btnStartOver:Button;
		private var btnClose:UIButton;
		private var firstResize:Boolean = true;
		
		public function KeyboardDialog() 
		{
			blocker.addEventListener(MouseEvent.CLICK, doBlockerClick);
			
			contentHolder = new DTMovieClip();
			addChild(contentHolder);
			
			// going to swap keyboard and thankyou out in contentholder
			mcKeyboardStuff = new DTMovieClip();
			contentHolder.addChild(mcKeyboardStuff);
			
			mcThankYouStuff = new DTMovieClip();
			
			
			var tf:DTTextField = new DTTextField(15, Settings.yellow, "left", new BrandonGrotesqueBlack());
			tf.text = "SEND AN EMAIL WITH YOUR RESULTS TO VIEW AND SHARE:";
			mcKeyboardStuff.addChild(tf);
			
			tfInput = new DTTextField(30, Settings.offWhite, "left", new VitesseLight());
			tfInput.text = " ";
			tfInput.y = tf.y + (tf.height * 2);			
			mcKeyboardStuff.addChild(tfInput);
			
			var hr:DTMovieClip = Utils.fill(Settings.offWhite);
			hr.height = Settings.pinLineSize;
			hr.y = tfInput.y + tfInput.height;
			// set width at end of function
			mcKeyboardStuff.addChild(hr);
			
			keyboardHolder = new DTMovieClip();
			keyboardHolder.y = tfInput.y + (tfInput.height * 2);
			mcKeyboardStuff.addChild(keyboardHolder);
			
			normalKeyboard = new DTMovieClip();
			keyboardHolder.addChild(normalKeyboard);
			
			specialKeyboard = new DTMovieClip();
			keyboardHolder.addChild(specialKeyboard);
			specialKeyboard.visible = false;
			
			var mc:UIButton;
			var xO:int = 0;
			var yO:int = 0;
			
			var keyHeight:int = 71 - 13;// got this from tracing the key height, need for key min width
			var keyWidth:int = keyHeight;
			var keyPadding:int = 10 - 2;
			var keyText:String;
			var isSpecialKey:Boolean;
			
			var mcKeyboard;
			
			for (var c:int = 0; c < keys.length; c++)
			{
				if (c == 0)
				{
					mcKeyboard = normalKeyboard;
				}
				if (c == 1)
				{
					mcKeyboard = specialKeyboard;
				}
				
				yO = 0;
			
				for (var a:int = 0; a < keys[c].length; a++)
				{
					if (a == 1)
					{
						xO = (keyWidth / 2) + keyPadding;
					}
					else
					{
						xO = 0;
					}
					if (a != 0)
					{
						yO += mc.height + keyPadding
					}
					for (var b:int = 0; b < keys[c][a].length; b++)
					{
						if (b != 0)
						{
							xO += keyWidth + keyPadding;
						}
						
						isSpecialKey = false;
						
						keyWidth = keyHeight;
						keyText = keys[c][a][b];
						switch(keyText)
						{
							case "shift":
								keyWidth = (keyWidth / 2) + keyPadding + keyHeight;
								keyText = " ";
								isSpecialKey = true;
								break;
							case "+":
								keyWidth = (keyWidth / 2) + keyPadding + keyHeight;
								break;
							case "_123":
								keyWidth = (keyWidth / 2) + keyPadding + keyHeight;
								isSpecialKey = true;
								break;
							case "ABC":
								keyWidth = (keyWidth / 2) + keyPadding + keyHeight;
								isSpecialKey = true;
								break;
							case "space":
								keyWidth = (5 * (keyWidth + keyPadding)) - keyPadding;
								keyText = " ";
								break;
							case "backspace":
								keyWidth = (keyWidth / 2) + keyPadding + keyHeight;
								keyText = " ";
								isSpecialKey = true;
								break;
							case ".COM":
								keyWidth = (keyWidth / 2) + keyPadding + keyHeight;
								break;
						}
						
						mc = new UIButton(keyText, 18, "4D544F", Settings.offWhite, 2, new BrandonGrotesqueBlack(), keyWidth, keyHeight);// trace("key height = " + mc.height);
						mc.x = xO;
						mc.y = yO;
						mcKeyboard.addChild(mc);
						
						if (keys[c][a][b] == "backspace")
						{
							mc.addImage(new KeyboardArrowBackspace());
						}
						
						if (keys[c][a][b] == "shift")
						{
							mc.addImage(new KeyboardArrowShift());
						}
						
						if (!isSpecialKey)
						{
							mc.addEventListener(DTEvent.CLICK, doKeyClick);
							normalKeyArray.push(mc);						
						}
						else
						{
							specialKeyArray.push(new Array(mc, keys[c][a][b]));
							mc.addEventListener(DTEvent.CLICK, doSpecialKeyClick);
							
							if (keys[c][a][b] == "shift")
							{
								// no need for a shift key
								mc.visible = false;
							}
						}
						
					}
				}
			}
			
			hr.width = contentHolder.width;
			
			checkbox = new Checkbox("Sign up to receive FHC collection updates and information about Fly Days<br>and other special events.");
			checkbox.addGraphics(new CheckboxChecked(), new CheckboxUnchecked());
			checkbox.y = contentHolder.height + checkbox.height;
			mcKeyboardStuff.addChild(checkbox);
			
			btnBack = new Button(new BtnBackOff(), new BtnBackOn());
			btnBack.y = checkbox.y + (checkbox.height * 2);
			mcKeyboardStuff.addChild(btnBack);
			btnBack.addEventListener(DTEvent.CLICK, doBackClick);
			
			btnNext = new Button(new BtnSubmitOff(), new BtnSubmitOn());
			btnNext.mcDisabled = new BtnSubmitDisabled();
			btnNext.y = checkbox.y + (checkbox.height * 2);
			btnNext.x = hr.width - btnNext.width;
			mcKeyboardStuff .addChild(btnNext);
			btnNext.addEventListener(DTEvent.CLICK, doNextClick);
			btnNext.enabled = false;
			
			resize();
			
			var closeBtnYOffset:int = 30;
			var closeBtnXOffset:int = 45;
			
			btnClose = new UIButton("CLOSE", 12, "4D544F", Settings.offWhite);
			btnClose.x = btnNext.x + btnNext.width - btnClose.width + closeBtnXOffset;
			btnClose.y = tf.y - closeBtnYOffset;
			mcThankYouStuff.addChild(btnClose);
			btnClose.addEventListener(DTEvent.CLICK, doCloseClick);
			
			btnStartOver = new Button(new BtnStartOverOff(), new BtnStartOverOn());
			btnStartOver.addEventListener(DTEvent.CLICK, doStartOverClick);
			mcThankYouStuff.addChild(btnStartOver);
			btnStartOver.y = btnNext.y;
			
			tf = new DTTextField(50, "DFD7BF", "left", new VitesseMedium());
			tf.text = "Thank you!";
			tf.x = (mcThankYouStuff.width / 2) - (tf.width / 2);
			tf.y = (mcThankYouStuff.height / 2) - (tf.height);
			mcThankYouStuff.addChild(tf);
			
			tf = new DTTextField(28, "DFD7BF", "left", new VitesseBook());
			tf.text = "Your email has been sent";
			tf.x = (mcThankYouStuff.width / 2) - (tf.width / 2);
			tf.y = (mcThankYouStuff.height / 2);// - (tf.height);
			mcThankYouStuff.addChild(tf);
			
			btnStartOver.x = (mcThankYouStuff.width / 2) - (btnStartOver.width / 2);
			
		}
		
		public override function set visible(bool:Boolean):void
		{
			if (visible == false && bool == true)
			{
				if (AppData.instance.settingsObj.showOverlays)
				{
					var testMC:TestOverlay = new TestOverlay(new MCKeyboardOverlay());
					testMC.alpha = .2;
					testMC.x = 364 + 91;
					testMC.y = 19;
				}
				
				// make sure we are on the keyboard dialog (reset)
				Utils.removeAllChildren(contentHolder);
				contentHolder.addChild(mcKeyboardStuff);
				//resize();
			}
			super.visible = bool;
		}
		
		private function doKeyClick(e:Event):void
		{
			var btn:UIButton = e.currentTarget as UIButton;
			
			var str:String = tfInput.text;
			
			str += btn.mText;
			
			setInputText(str);
			
			(DocClassBase.instance as DocClass).setAreYouStillThereTimeout();
		}
		
		private function doSpecialKeyClick(e:Event):void
		{
			var btn:UIButton = e.currentTarget as UIButton;
			
			var str:String = tfInput.text;
			
			for (var a:int = 0; a < specialKeyArray.length; a++)
			{
				if (specialKeyArray[a][0] == btn)
				{
					switch(specialKeyArray[a][1])
					{
						case "backspace":
							str = str.substr(0, str.length - 1);
							setInputText(str);
							return;
						case "_123":
							specialKeyboard.visible = true;
							normalKeyboard.visible = false;
							return;
						case "ABC":
							specialKeyboard.visible = false;
							normalKeyboard.visible = true;
							return;
					}
				}
			}
			
			(DocClassBase.instance as DocClass).setAreYouStillThereTimeout();
		}
		
		private function setInputText(str:String):void
		{
			if (str == " ") str = "";
			tfInput.text = str.toLowerCase();
			if (str == "")
			{
				btnNext.enabled = false;
			}
			else
			{
				btnNext.enabled = true;
			}
		}
		
		private function doBackClick(e:Event = null):void
		{
			dispatchEvent(new DTEvent(DTEvent.CLOSE));
			(DocClassBase.instance as DocClass).setAreYouStillThereTimeout();
		}
		
		private function doNextClick(e:Event):void
		{
			email = tfInput.text;
			tfInput.text = "";
			
			dispatchEvent(new DTEvent(DTEvent.COMPLETE));
			(DocClassBase.instance as DocClass).setAreYouStillThereTimeout();
		}
		
		private function doBlockerClick(e:Event):void
		{
			doBackClick();
		}
		
		private function doStartOverClick(e:Event):void
		{
			(DocClassBase.instance as DocClass).doStartOverClick(null);
		}
		
		private function doCloseClick(e:Event):void
		{
			doBackClick();
		}
		
		public function showThankYou():void
		{
			//contentHolder.visible = false;
			Utils.removeAllChildren(contentHolder);
			contentHolder.addChild(mcThankYouStuff);
			//resize();
			
			(DocClassBase.instance as DocClass).setAreYouStillThereTimeout();
		}
		
		public override function resize():void
		{
			super.resize();
			
			if (firstResize)
			{
				// design is not completely centered on the screen
				firstResize = false;
				popupBackground.y += 49 - 14;
			}
			contentHolder.x = popupBackground.x + (popupBackground.width / 2) - (contentHolder.width / 2);
			contentHolder.y = popupBackground.y + (popupBackground.height / 2) - (contentHolder.height / 2);
		}
		
		public override function destroy():void
		{
			// destroy will get called by ConflictComplete
			
			btnBack.removeEventListener(DTEvent.CLICK, doBackClick);
			btnNext.removeEventListener(DTEvent.CLICK, doNextClick);
			
			btnClose.removeEventListener(DTEvent.CLICK, doCloseClick);
			btnStartOver.removeEventListener(DTEvent.CLICK, doStartOverClick);
			
			// need to remove listeners on keyboard keys
			
			var mc:UIButton;
			
			for (var a:int = 0; a < normalKeyArray.length; a++)
			{
				mc = normalKeyArray[a];
				mc.removeEventListener(DTEvent.CLICK, doKeyClick);
			}
			
			for (a = 0; a < specialKeyArray.length; a++)
			{
				mc = specialKeyArray[a][0];
				mc.removeEventListener(DTEvent.CLICK, doSpecialKeyClick);
			}
			
			normalKeyArray = null;
			specialKeyArray = null;
			
			super.destroy();
		}
	}

}