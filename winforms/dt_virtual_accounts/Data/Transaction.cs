﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DT_Virtual_Accounts.Data
{
    public class Transaction
    {
        public int Id;
        public string Name;
        public decimal Payment;
        public decimal Deposit;
        public int AccountId;
        public int CategoryId;
        public DateTime Date;
        public string Memo;
        public string Num;
        public int TransferAccountId;
        public bool IsTransfer = false;
        public bool Clear = false;
    }
}
