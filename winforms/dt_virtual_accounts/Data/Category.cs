﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DT_Virtual_Accounts.Data
{
    public class Category
    {
        public int Id;
        public string Name;
        public bool IsBaseCategory;
        public int BaseCategoryId;
    }
}
