﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DT_Virtual_Accounts.Data
{
    public class Account
    {
        public int Id;
        public string Name;
        public bool IsVirtualAcount;
        public int VirtualAccountId;
        public decimal Value = 0;
        public int DisplayOrder = 0;
        public bool Active = true;
    }
}
