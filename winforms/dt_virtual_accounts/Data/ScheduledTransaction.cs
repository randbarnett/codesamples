﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DT_Virtual_Accounts.Data
{
    public class ScheduledTransaction
    {
        public int Id;
        public string Name;
        public DateTime StartDateTime;
        public string Frequency;
        public decimal Payment;
        public decimal Deposit;
        public string Num;
        public int AccountId;
        public string Memo;
    }
}
