﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DT_Virtual_Accounts.Data
{
    public class AllData
    {
        public List<Account> Accounts = new List<Account>();

        public List<Category> Categories = new List<Category>();

        public List<Transaction> Transactions = new List<Transaction>();

        public List<ScheduledTransaction> ScheduledTransactions = new List<ScheduledTransaction>();
    }
}
