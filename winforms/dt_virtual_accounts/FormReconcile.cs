﻿using DT_Virtual_Accounts.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace DT_Virtual_Accounts
{
    public partial class FormReconcile : Form
    {
        public static FormReconcile instance;

        public Form1 mainForm;
        public Account account;
        public List<int> accountIds;
        public List<Transaction> transactions = new List<Transaction>();
        private decimal previouslyCleared = 0;

        public FormReconcile()
        {
            InitializeComponent();
            instance = this;
        }

        private void FormReconcile_Load(object sender, EventArgs e)
        {
            dgLineItems.CellMouseDown += DoDGMouseDown;            
            tfEndingBalance.TextChanged += DoEndingBalanceChanged;
        }

        public void Init(Form1 pMainForm, Account pAccount)
        {
            mainForm = pMainForm;
            account = pAccount;

            // just account isn't good enough
            // we need to look at the virtual accounts based on this account too
            // deposits could have gone directly into a virtual account and need to be reconciled with the master account
            accountIds = new();
            accountIds.Add(account.Id);
            foreach (Account a in mainForm.allData.Accounts)
            {
                if(a.Id != account.Id && a.IsVirtualAcount == true && a.VirtualAccountId == account.Id)
                {
                    accountIds.Add(a.Id);
                }
            }

            RefreshTransactions();
        }

        public void RefreshTransactions()
        {
            List<Transaction> realTransactions = GetRealTransactions();

            Transaction transaction;
            List<Transaction> newTransactions = new List<Transaction>();

            // we're going to map the real transactions into the transactions variable
            if(realTransactions.Count > 0)
            {
                foreach(Transaction t in realTransactions)
                {
                    // create a new transaction (we only want to change the real transactions when the finish button is clicked)
                    transaction = new Transaction();
                    transaction.Id = t.Id;
                    transaction.Date = t.Date;
                    transaction.Name = t.Name;
                    transaction.Payment = t.Payment;
                    transaction.Deposit = t.Deposit;
                    transaction.Clear = t.Clear;// paranoia, these should all be false!
                    transaction.IsTransfer = t.IsTransfer;
                    transaction.TransferAccountId = t.TransferAccountId;

                    Transaction t2 = transactions.FirstOrDefault(r => r.Id == t.Id);
                    if (t2 != null) transaction.Clear = t2.Clear;

                    newTransactions.Add(transaction);
                }
            }

            transactions = newTransactions;

            RefreshDG();
            UpdateTotals();
        }

        private void RefreshDG()
        {
            dgLineItems.Rows.Clear();
            dgLineItems.Refresh();

            if (transactions.Count > 0)
            {
                decimal payment;
                decimal deposit;
                foreach (Transaction t in transactions)
                {
                    if(t.IsTransfer && t.TransferAccountId == account.Id)
                    {
                        // need to swap these if it is a transfer
                        payment = t.Deposit;
                        deposit = t.Payment;
                    }
                    else
                    {
                        payment = t.Payment;
                        deposit = t.Deposit;
                    }
                    dgLineItems.Rows.Add(new Object[] { t.Id, t.Clear ? "X" : "", t.Date.ToString("yyyy/MM/dd"), t.Name, Form1.MoneyFormat(payment), Form1.MoneyFormat(deposit) });
                }
            }
        }

        private List<Transaction> GetRealTransactions(bool previouslyCleared = false)
        {
            // we need to exclude transfers between virtual accounts
            // they'll never be reconcilled anyway and will eventually hog up all the room in the dg
            //List<Transaction> realTransactions = mainForm.allData.Transactions.Where(r => (r.Clear == previouslyCleared) && ((r.IsTransfer == false && accountIds.Contains(r.AccountId)) || ((r.IsTransfer && (!accountIds.Contains(r.AccountId) || !accountIds.Contains(r.TransferAccountId)))))).ToList();
            List<Transaction> realTransactions = mainForm.allData.Transactions.Where(r => (r.Clear == previouslyCleared) && ((r.IsTransfer == false && accountIds.Contains(r.AccountId)) || ((r.IsTransfer && ((r.AccountId == account.Id && !accountIds.Contains(r.TransferAccountId)) || (r.TransferAccountId == account.Id && !accountIds.Contains(r.AccountId))))))).ToList();

            return realTransactions;
        }

        private void UpdateTotals()
        {
            List<Transaction> realTransactions = GetRealTransactions(true);

            previouslyCleared = 0;

            if(realTransactions.Count > 0)
            {
                foreach(Transaction t in realTransactions)
                {
                    if (t.IsTransfer && t.TransferAccountId == account.Id)
                    {
                        // need to swap these if it is a transfer
                        previouslyCleared -= t.Deposit - t.Payment;
                    }
                    else
                    {
                        previouslyCleared += t.Deposit - t.Payment;
                    }
                }
            }

            decimal totalCleared = 0;
            if(transactions.Count > 0)
            {
                foreach (Transaction t in transactions)
                {
                    if (t.Clear)
                    {
                        if(t.IsTransfer && t.TransferAccountId == account.Id)
                        {
                            totalCleared -= t.Deposit - t.Payment;
                        }
                        else
                        {
                            totalCleared += t.Deposit - t.Payment;
                        }                        
                    }                    
                }
            }

            tfClearedBalance.Text = Form1.MoneyFormat(totalCleared);

            decimal userEnteredTarget = 0;
            decimal.TryParse(tfEndingBalance.Text, out userEnteredTarget);

            Debug.WriteLine(userEnteredTarget.ToString() + " user entered target");

            tfDifference.Text = Form1.MoneyFormat(userEnteredTarget - (previouslyCleared + totalCleared));
        }

        private void DoDGMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex < 0) return;
            string idStr = dgLineItems.Rows[e.RowIndex].Cells[0].Value.ToString();
            int id = int.Parse(idStr);

            if (e.Button == MouseButtons.Right)
            {
                if (transactions.Count > 0)
                {
                    foreach (Transaction t in transactions)
                    {
                        if (t.Id == id)
                        {
                            //MessageBox.Show(dgLineItems.Rows[e.RowIndex].Cells[3].Value.ToString());
                            mainForm.EditTransaction(t.Id);
                        }
                    }
                }
            }
            else
            {
                if (transactions.Count > 0)
                {
                    foreach (Transaction t in transactions)
                    {
                        if (t.Id == id)
                        {
                            t.Clear = !t.Clear;
                            dgLineItems.Rows[e.RowIndex].Cells[1].Value = (t.Clear ? "X" : "");
                        }
                    }
                }
                UpdateTotals();
            }
        }

        private void DoEndingBalanceChanged(object sender, EventArgs e)
        {
            UpdateTotals();
        }

        private void btnFinished_Click(object sender, EventArgs e)
        {
            List<Transaction> realTransactions = GetRealTransactions();

            string errorMessage = "";

            if (realTransactions.Count == 0)
            {
                errorMessage = "Failed.  No real transactions found.";
            }
            else if (realTransactions.Count != transactions.Count)
            {
                errorMessage = "Failed.  Real transaction count does not match reconciled transaction count.";
            }
            else
            {
                foreach (Transaction t in transactions)
                {
                    foreach(Transaction realTransaction in realTransactions)
                    {
                        if(t.Id == realTransaction.Id)
                        {
                            if (!realTransaction.Clear && t.Clear) realTransaction.Clear = true;
                        }
                    }
                }

                MessageBox.Show("All done!");
                mainForm.RefreshTransactionList();
                instance = null;
                this.Close();
                return;
            }

            MessageBox.Show(errorMessage);
        }

        private void btnMarkAll_Click(object sender, EventArgs e)
        {
            MarkAll(true);
        }

        private void btnClearAll_Click(object sender, EventArgs e)
        {
            MarkAll(false);
        }

        private void MarkAll(bool hit)
        {
            if(transactions.Count > 0)
            {
                foreach(Transaction t in transactions)
                {
                    t.Clear = hit;
                }

                RefreshDG();
                UpdateTotals();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            instance = null;
            Close();
        }
    }
}
