﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using DT_Virtual_Accounts.Data;
using System.Media;
using DT_Virtual_Accounts.ViewObjects;

namespace DT_Virtual_Accounts
{
    public partial class Form1 : Form
    {
        // by Rand Barnett
        // www.dottrombone.com/apps

        // This program is a checkbook register similar to Quicken.

        public AllData allData;// All data is stored here and then saved to a json file that is selected in settings.
        public List<string> allPayees = new List<string>();

        private DialogSave dialogFile;
        private DialogCategories dialogCategories;
        private DialogAccounts dialogAccounts;

        public int currentAccountId = -1;
        public bool ShowAccountReorderButtons = false;
        private int editTransactionId = 0;

        private List<TextBox> AccountButtons = new List<TextBox>();
        private List<TextBox> AccountTotalButtons = new List<TextBox>();
        private List<AccountButtons> AccountReorderingButtons = new List<AccountButtons>();

        private SoundPlayer soundPlayer;
 
        public Form1()
        {
            InitializeComponent();

            soundPlayer = new SoundPlayer();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Point location = Properties.Settings.Default.Location;
            if (!(location.X == 0 && location.Y == 0)) this.Location = location;

            allData = new AllData();

            // add the default categories
            string[] cats = new string[] { "Loan Payment", "Interest Expense", "Interest Income", "Groceries", "Medical", "Bills", "Discretionary", "Income", "Taxes" };
            
            foreach(string catStr in cats)
            {
                Category cat = new Category();
                cat.Id = Uniqid();
                cat.BaseCategoryId = cat.Id;
                cat.IsBaseCategory = true;
                cat.Name = catStr;
                allData.Categories.Add(cat);
            }

            // create dialogs
            dialogFile = new DialogSave();
            dialogCategories = new DialogCategories();


            if (Properties.Settings.Default.SaveFile == "" || !File.Exists(Properties.Settings.Default.SaveFile))
            {
                if(!File.Exists(Properties.Settings.Default.SaveFile))
                {
                    Properties.Settings.Default.SaveFile = "";
                    Properties.Settings.Default.Save();
                }
                OpenDialogFile();
            }
            else
            {
                ImportData();
                currentAccountId = Properties.Settings.Default.OpeningAccountId;
                UpdateAllAccountValues();
                RefreshAccountList();
                RefreshTransactionList();
                CreateAllPayeesList();
                RefreshData();// populates the category combo box
            }
                        
            SetTabOrder();
            dtDate.Value = DateTime.Now;

            dgTransactions.CellMouseDown += DoTransactionsMouseDown;
        }

        private void SetTabOrder()
        {
            dtDate.TabIndex = 1;
            cbPayee.TabIndex = 2;
            tfPayment.TabIndex = 3;
            tfDeposit.TabIndex = 4;
            tfNum.TabIndex = 5;
            cbCategory.TabIndex = 6;
            cbTransfer.TabIndex = 7;
            tfMemo.TabIndex = 8;
            btnSaveTransaction.TabIndex = 9;
        }

        private bool AccountHasTransactions(Account account)
        {
            List<Transaction> transactions = allData.Transactions.Where(r => r.AccountId == account.Id).ToList();
            if (transactions.Count > 0) return true;

            transactions = allData.Transactions.Where(r => r.IsTransfer && r.TransferAccountId == account.Id).ToList();
            if (transactions.Count > 0) return true;

            if (!account.IsVirtualAcount)
            {
                List<Account> accounts = allData.Accounts.Where(r => r.IsVirtualAcount && r.VirtualAccountId == account.Id).ToList();
                if(accounts.Count > 0)
                {
                    foreach(Account account2 in accounts)
                    {
                        transactions = allData.Transactions.Where(r => r.AccountId == account2.Id).ToList();
                        if (transactions.Count > 0) return true;
                    }
                }
            }

            return false;
        }

        public void RefreshTransactionList()
        {
            if(dgTransactions.Rows.Count > 0) dgTransactions.Rows.Clear();

            Transaction record;
            string transferAccountName;
            Account transferAccount;
            decimal payment, deposit;
            Category cat, baseCat;

            List<Transaction> transactions = allData.Transactions.Where(r => (r.AccountId == currentAccountId) || ((r.IsTransfer) && (r.TransferAccountId == currentAccountId))).ToList();
            transactions = transactions.OrderByDescending(r => r.Date).ToList();

            for(int a = 0; a < transactions.Count; a++)
            {                
                record = transactions[a];
                transferAccountName = "";
                payment = record.Payment;
                deposit = record.Deposit;

                if(record.IsTransfer && record.TransferAccountId == currentAccountId)
                {
                    // this is the account that the record was transfered to
                    // for the transfer account, show the account the transfer originated on
                    transferAccount = GetAccount(record.AccountId);
                    if(transferAccount != null)
                    {
                        // reverse the amounts
                        payment = record.Deposit;
                        deposit = record.Payment;
                    }
                }
                else
                {
                    transferAccount = GetAccount(record.TransferAccountId);
                }
                
                if (transferAccount != null) transferAccountName = transferAccount.Name;

                cat = GetCategory(record.CategoryId);
                baseCat = cat;
                if (!cat.IsBaseCategory) baseCat = GetCategory(cat.BaseCategoryId);

                dgTransactions.Rows.Add(record.Id, record.Date.ToString("yyyy/MM/dd"), record.Clear ? "X" : "", record.Name, MoneyFormat(payment), MoneyFormat(deposit), cat.Name, baseCat.Name , transferAccountName, record.Num, record.Memo);
            }
        }

        private void OpenDialogCategories()
        {
            if (dialogCategories == null || dialogCategories.IsDisposed) dialogCategories = new DialogCategories();

            dialogCategories.StartPosition = FormStartPosition.CenterScreen;
            dialogCategories.TopMost = true;
            dialogCategories.Show();
            dialogCategories.MainForm = this;
            dialogCategories.Init();
        }

        private void OpenDialogFile()
        {
            if (dialogFile == null || dialogFile.IsDisposed) dialogFile = new DialogSave();

            dialogFile.StartPosition = FormStartPosition.CenterScreen;
            dialogFile.TopMost = true;
            dialogFile.Show();
            dialogFile.mainForm = this;
            dialogFile.SetActiveFile(Properties.Settings.Default.SaveFile);
        }

        private void OpenDialogAccounts()
        {
            if (dialogAccounts == null || dialogAccounts.IsDisposed) dialogAccounts = new DialogAccounts();

            dialogAccounts.StartPosition = FormStartPosition.CenterScreen;
            dialogAccounts.TopMost = true;
            dialogAccounts.Show();
            dialogAccounts.MainForm = this;
            dialogAccounts.Init();
        }

        public void SetSaveFile(string path)
        {
            Properties.Settings.Default.SaveFile = path;
            Properties.Settings.Default.Save();

            PlaySuccessSound();

            if(File.Exists(path))
            {
                // read the file into memory
                ImportData();
            }
            else
            {
                // create the file
                File.WriteAllText(Properties.Settings.Default.SaveFile, "");
                SaveToFile();                
            }

            RefreshAccountList();
            RefreshTransactionList();
            CreateAllPayeesList();
            RefreshData();// populates the category combo box
        }

        private void setSaveFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenDialogFile();
        }

        private void SaveToFile()
        {
            if(File.Exists(Properties.Settings.Default.SaveFile))
            {
                string data = Serialize(allData);
                File.WriteAllText(Properties.Settings.Default.SaveFile, data);
            }
        }

        private void ImportData()
        {
            if (File.Exists(Properties.Settings.Default.SaveFile))
            {
                string fileContents = File.ReadAllText(Properties.Settings.Default.SaveFile);
                allData = Deserialize<AllData>(fileContents);
            }
        }

        public int Uniqid()
        {
            int lastId = Properties.Settings.Default.Uniqid;
            lastId++;
            Properties.Settings.Default.Uniqid = lastId;
            Properties.Settings.Default.Save();
            return lastId;
        }

        public int FindBaseCategoryId(string catName)
        {
            if(allData.Categories.Count > 0)
            {
                foreach(Category cat in allData.Categories)
                {
                    if (cat.Name == catName) return cat.Id;
                }
            }

            return -1;
        }

        public static string MoneyFormat(decimal n)
        {
            return "$" + string.Format("{0:n}", n);
        }

        public static string Serialize(object obj)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(obj);
        }

        public static T Deserialize<T>(string str)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(str);
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if (false && MessageBox.Show("Are you sure you want to exit?", "DT Virtual Accounts", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                // Cancel the Closing event from closing the form.
                e.Cancel = true;
                // Call method to save file...
            }
            else
            {
                SaveToFile();

                Properties.Settings.Default.Location = this.Location;
                Properties.Settings.Default.OpeningAccountId = currentAccountId;
                Properties.Settings.Default.Save();
            }
        }

        private void btnSaveTransaction_Click(object sender, EventArgs e)
        {
            Transaction transaction = null;
            if(editTransactionId != 0)
            {
                transaction = GetTransaction(editTransactionId);
            }

            Transaction newTransaction = new();

            if (transaction != null)
            {
                transaction = newTransaction = transaction;
            }

            newTransaction.Id = Uniqid();
            newTransaction.AccountId = currentAccountId;
            newTransaction.Date = dtDate.Value;
            newTransaction.Name = cbPayee.Text;
            if(tfPayment.Text == "")
            {
                newTransaction.Payment = 0;
            }
            else
            {
                newTransaction.Payment = Decimal.Parse(tfPayment.Text);
            }
            if (tfDeposit.Text == "")
            {
                newTransaction.Deposit = 0;
            }
            else
            {
                newTransaction.Deposit = Decimal.Parse(tfDeposit.Text);
            }
            newTransaction.Num = tfNum.Text;
            newTransaction.CategoryId = GetCategoryId(cbCategory.Text);
            newTransaction.Memo = tfMemo.Text;

            decimal transactionAmount = newTransaction.Deposit - newTransaction.Payment;

            if (cbTransfer.Text != "")
            {
                Account transferAccount = GetAccount(GetAccountId(cbTransfer.Text));
                if(transferAccount != null)
                {
                    newTransaction.IsTransfer = true;
                    newTransaction.TransferAccountId = transferAccount.Id;
                    //transferAccount.Value -= transactionAmount;
                }
                else
                {
                    MessageBox.Show("Couldn't find account to transfer to.");
                }
            }

            if(transaction != null)
            {
                // this was an edit
                // newTransaction should have already altered the data
                if (FormReconcile.instance != null) FormReconcile.instance.RefreshTransactions();
            }
            else
            {
                allData.Transactions.Add(newTransaction);
            }

            editTransactionId = 0;
            gbEntry.Text = "New Transaction";
            cbPayee.SelectedIndex = -1;
            tfPayment.Text = "";
            tfDeposit.Text = "";
            tfNum.Text = "";
            tfMemo.Text = "";
            cbCategory.SelectedIndex = -1;

            PlaySuccessSound();

            if(!allPayees.Contains(newTransaction.Name))
            {
                allPayees.Add(newTransaction.Name);
            }
            UpdateAllAccountValues();
            UpdateAccountValues();
            RefreshTransactionList();
            RefreshData();
            this.ActiveControl = cbPayee;
        }

        public void PlaySuccessSound()
        {
            //SystemSounds.Exclamation.Play();
            soundPlayer.SoundLocation = AppDomain.CurrentDomain.BaseDirectory + "\\Sounds\\click2.wav";
            //player.SoundLocation = Properties.Resources.'Sounds\button_clk_02.mp3';
            soundPlayer.Play();
        }

        private void dgTransactions_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void categoriesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenDialogCategories();
        }

        public List<string> GetCategoryComboBoxList(bool justBaseCategories = false, bool showBlankRecord = false)
        {
            List<string> records = new List<string>();
            if(showBlankRecord) records.Add("");

            if (allData.Categories.Count > 0)
            {
                foreach (Category cat in allData.Categories)
                {
                    if (justBaseCategories && cat.IsBaseCategory)
                    {
                        records.Add(cat.Name);
                    }
                    else if(!justBaseCategories)
                    {
                        records.Add(cat.Name);
                    }
                }
            }

            records = records.OrderBy(r => r).ToList();

            return records;
        }

        public void CreateAllPayeesList()
        {
            allPayees = new List<string>();
            if(allData.Transactions.Count > 0)
            {
                foreach(Transaction record in allData.Transactions)
                {
                    allPayees.Add(record.Name);
                }
            }

            allPayees = allPayees.OrderBy(r => r).ToList();
        }

        public List<string> GetPayeeComboBoxList()
        {
            List<string> records = new List<string>();
            records.Add("");
            if(allPayees.Count > 0)
            {
                foreach(string record in allPayees)
                {
                    records.Add(record);
                }
            }
            return records;            
        }

        public List<string> GetAccountsComboBoxList(bool includeVirtualAccounts = true, bool includeActiveAccountVirtualAccounts = false)
        {
            List<string> records = new List<string>();
            records.Add("");
            List<Account> accounts = allData.Accounts.OrderBy(r => r.Name).ToList();
            if (accounts.Count > 0)
            {
                foreach (Account record in accounts)
                {
                    if(includeVirtualAccounts || record.IsVirtualAcount == false || (includeActiveAccountVirtualAccounts && record.IsVirtualAcount && record.VirtualAccountId == currentAccountId)) records.Add(record.Name);
                }
            }
            return records;
        }

        public Category GetCategory(int id)
        {
            return allData.Categories.FirstOrDefault(r => r.Id == id);
        }

        public int GetCategoryId(string name)
        {
            if(allData.Categories.Count > 0)
            {
                foreach(Category cat in allData.Categories)
                {
                    if (cat.Name == name) return cat.Id;
                }
            }
            return -1;
        }

        public string GetCategoryName(int id)
        {
            if (allData.Categories.Count > 0)
            {
                foreach (Category cat in allData.Categories)
                {
                    if (cat.Id == id) return cat.Name;
                }
            }
            return "";
        }

        public int GetAccountId(string name)
        {
            if (allData.Accounts.Count > 0)
            {
                foreach (Account record in allData.Accounts)
                {
                    if (record.Name == name) return record.Id;
                }
            }
            return -1;
        }

        public Account GetAccount(int id)
        {
            return allData.Accounts.FirstOrDefault(r => r.Id == id);
        }

        public Transaction GetTransaction(int id)
        {
            return allData.Transactions.FirstOrDefault(r => r.Id == id);
        }

        private void AddToActiveAccountTotal(decimal n)
        {
            Account account = GetAccount(currentAccountId);
            account.Value += n;
            UpdateAccountValues();
        }

        public void RefreshAccountList(bool justActiveState = false)
        {
            if(justActiveState)
            {
                // we're just going to toggle the active button color
                if(AccountButtons.Count > 0)
                {
                    foreach(TextBox box in AccountButtons)
                    {
                        int id = GetAccountId(box.Text);
                        if(id == currentAccountId)
                        {
                            box.BackColor = AccountActiveColor;
                        }
                        else
                        {
                            box.BackColor = AccountInactiveColor;
                        }
                    }
                }
                return;
            }
            if(AccountButtons.Count > 0)
            {
                foreach(TextBox tf2 in AccountButtons)
                {
                    panelAccounts.Controls.Remove(tf2);
                    tf2.Dispose();
                }
            }
            AccountButtons = new List<TextBox>();

            if (AccountTotalButtons.Count > 0)
            {
                foreach (TextBox tf2 in AccountTotalButtons)
                {
                    panelAccounts.Controls.Remove(tf2);
                    tf2.Dispose();
                }
            }
            AccountTotalButtons = new List<TextBox>();

            if (AccountReorderingButtons.Count > 0)
            {
                foreach (AccountButtons btns in AccountReorderingButtons)
                {
                    if(btns.UpButton != null)
                    {
                        panelAccounts.Controls.Remove(btns.UpButton);
                        btns.UpButton.Dispose();
                    }
                    if (btns.DownButton != null)
                    {
                        panelAccounts.Controls.Remove(btns.DownButton);
                        btns.DownButton.Dispose();
                    }
                    if (btns.RemoveButton != null)
                    {
                        panelAccounts.Controls.Remove(btns.RemoveButton);
                        btns.RemoveButton.Dispose();
                    }
                }
            }
            AccountReorderingButtons = new List<AccountButtons>();

            TextBox tf;
            int yO = 30;

            List<Account> accounts = allData.Accounts.Where(r => r.IsVirtualAcount == false).OrderBy(r => r.DisplayOrder).ToList();

            if(accounts.Count > 0)
            {
                foreach(Account account in accounts)
                {
                    if (!ShowAccountReorderButtons && !account.Active) continue;
                    if(!account.IsVirtualAcount)
                    {
                        tf = AccountTextBox(account, yO);
                        AccountTextBox(account, yO, 0, true);
                        yO += tf.Height + 10;

                        List<Account> virtualAccounts = allData.Accounts.Where(r => (r.IsVirtualAcount == true) && (r.VirtualAccountId == account.Id)).OrderBy(r => r.DisplayOrder).ToList();
                        if(virtualAccounts.Count > 0)
                        {
                            foreach (Account virtualAccount in virtualAccounts)
                            {
                                if (!ShowAccountReorderButtons && !virtualAccount.Active) continue;
                                tf = AccountTextBox(virtualAccount, yO, 20);
                                AccountTextBox(account, yO, 0, true);
                                yO += tf.Height + 10;
                            }
                        }
                    }                    
                }
            }

            UpdateAccountValues();
        }

        private Color AccountActiveColor
        {
            get
            {
                return Color.LightGreen;
            }
        }

        private Color AccountInactiveColor
        {
            get
            {
                return Color.LightGray;
            }
        }

        private TextBox AccountTextBox(Account account, int yO, int indent = 0, bool isTotalTF = false)
        {
            int totalTFWidth = 100;
            int orderButtonWidth = 25;
            if (!ShowAccountReorderButtons) orderButtonWidth = 0;
            int padding = 20;
            int tfWidth = panelAccounts.Width - (2 * padding) - totalTFWidth - (3 * orderButtonWidth);
            int xO = padding;

            TextBox tf;

            if(isTotalTF)
            {
                tf = new TextBox();
                tf.ReadOnly = true;
                tf.Cursor = Cursors.Hand;
                tf.Font = new Font(tf.Font, FontStyle.Bold);
                tf.Width = totalTFWidth;
                tf.Text = account.Name;
                tf.Location = new Point(xO + tfWidth, yO);
                panelAccounts.Controls.Add(tf);
                AccountTotalButtons.Add(tf);
                if(!account.Active)
                {
                    tf.Enabled = false;
                }

                // add the ordering buttons
                if(ShowAccountReorderButtons)
                {
                    AccountButtons btns = new AccountButtons();
                    Button btn = new Button();
                    btn.Cursor = Cursors.Hand;
                    btn.Click += AccountReorderButtonClick;
                    btn.Text = "▲";
                    btn.Width = orderButtonWidth;
                    btn.Height = tf.Height;
                    btn.Location = new Point(xO + tfWidth + tf.Width, yO);
                    panelAccounts.Controls.Add(btn);
                    btns.UpButton = btn;

                    Button btn2 = new Button();
                    btn2.Cursor = Cursors.Hand;
                    btn2.Click += AccountReorderButtonClick;
                    btn2.Text = "▼";
                    btn2.Width = orderButtonWidth;
                    btn2.Height = tf.Height;
                    btn2.Location = new Point(xO + tfWidth + tf.Width + btn2.Width, yO);
                    panelAccounts.Controls.Add(btn2);
                    btns.DownButton = btn2;

                    Button btn3 = new Button();
                    btn3.Cursor = Cursors.Hand;
                    btn3.Click += AccountRemoveButtonClick;
                    btn3.Text = "X";
                    btn3.Font = new Font(btn3.Font.Name, btn3.Font.Size, FontStyle.Bold);
                    btn3.Width = orderButtonWidth;
                    btn3.Height = tf.Height;
                    btn3.Location = new Point(xO + tfWidth + tf.Width + btn2.Width + btn3.Width, yO);
                    panelAccounts.Controls.Add(btn3);
                    btns.RemoveButton = btn3;

                    AccountReorderingButtons.Add(btns);
                }                
            }
            else
            {
                tf = new TextBox();
                tf.Cursor = Cursors.Hand;
                tf.ReadOnly = true;
                tf.Click += AccountButtonClick;
                if (account.Id == currentAccountId)
                {
                    tf.BackColor = AccountActiveColor;
                }
                else
                {
                    tf.BackColor = AccountInactiveColor;
                }
                tf.Font = new Font(tf.Font, FontStyle.Bold);
                tf.Width = tfWidth - indent;
                tf.Text = account.Name;
                tf.Location = new Point(xO + indent, yO);
                panelAccounts.Controls.Add(tf);
                AccountButtons.Add(tf);

                if (!account.Active)
                {
                    tf.Enabled = false;
                }
            }
            

            return tf;
        }

        private void UpdateAllAccountValues()
        {
            if(allData.Accounts.Count > 0)
            {
                foreach(Account account in allData.Accounts)
                {
                    UpdateAccountValue(account);
                }
            }
        }

        private void UpdateAccountValue(Account account)
        {
            if (account != null)
            {
                decimal value = 0;
                List<Transaction> transactions = allData.Transactions.Where(r => r.AccountId == account.Id || (r.IsTransfer && r.TransferAccountId == account.Id)).ToList();
                if (transactions.Count > 0)
                {
                    foreach (Transaction t in transactions)
                    {
                        if(t.IsTransfer && t.TransferAccountId == account.Id)
                        {
                            // we need to invert the line total of this transaction
                            value -= t.Deposit - t.Payment;
                        }
                        else
                        {
                            value += t.Deposit - t.Payment;
                        }                        
                    }
                }
                account.Value = value;
            }
        }

        private void UpdateAccountValues()
        {
            decimal netWorth = 0;// total them every time for accuracy
            if(AccountButtons.Count > 0)
            {
                for(int a = 0; a < AccountButtons.Count; a++)
                {
                    Account account = GetAccount(GetAccountId(AccountButtons[a].Text));
                    AccountTotalButtons[a].Text = MoneyFormat(account.Value);
                    // we can't get the nice dark red color on the text unless we set the backcolor first
                    // weird microsoft bug :)
                    AccountTotalButtons[a].BackColor = Color.White;
                    
                    if (account.Value < 0)
                    {
                        AccountTotalButtons[a].ForeColor = Color.DarkRed;
                    }
                    else
                    {
                        AccountTotalButtons[a].ForeColor = Color.Black;
                    }
                    netWorth += account.Value;
                }
            }

            tfNetWorth.Text = "NET Worth: " + MoneyFormat(netWorth);
        }

        private void AccountButtonClick(object sender, EventArgs e)
        {
            string accountName = (sender as TextBox).Text;
            currentAccountId = GetAccountId(accountName);            
            RefreshAccountList(true);
            RefreshTransactionList();
            RefreshData();
        }

        private void AccountRemoveButtonClick(object sender, EventArgs e)
        {
            Button btn = (sender as Button);
            if (AccountReorderingButtons.Count > 0)
            {
                if (AccountButtons.Count > 0)
                {
                    for (int a = 0; a < AccountButtons.Count; a++)
                    {
                        
                        if(AccountReorderingButtons[a].RemoveButton == btn)
                        {
                            // this is the account to remove or disable
                            Account account = GetAccount(GetAccountId(AccountButtons[a].Text));

                            if(account.Active)
                            {
                                // remove or disable
                                if (!AccountHasTransactions(account))
                                {
                                    // safe to just remove this account
                                    allData.Accounts.Remove(account);
                                    RefreshAccountList();
                                    return;
                                }

                                account.Active = false;
                                AccountButtons[a].Enabled = false;
                                AccountTotalButtons[a].Enabled = false;
                            }
                            else
                            {
                                // enable
                                account.Active = true;
                                AccountButtons[a].Enabled = true;
                                AccountTotalButtons[a].Enabled = true;
                            }
                            
                        }
                    }
                }
            }
        }

        private void AccountReorderButtonClick(object sender, EventArgs e)
        {
            Button btn = (sender as Button);
            if(AccountReorderingButtons.Count > 0)
            {
                for(int a = 0; a < AccountReorderingButtons.Count; a++)
                {
                    bool hit = false;
                    int direction = -1;
                    if(AccountReorderingButtons[a].UpButton == btn)
                    {
                        // process up click
                        hit = true;
                    }
                    else if(AccountReorderingButtons[a].DownButton == btn)
                    {
                        hit = true;
                        direction = 1;
                    }

                    if(hit)
                    {
                        Account account = GetAccount(GetAccountId(AccountButtons[a].Text));

                        // we've got to change all the order numbers on the account
                        if(account.IsVirtualAcount)
                        {
                            List<Account> virtualAccounts = allData.Accounts.Where(r => r.VirtualAccountId == account.VirtualAccountId).OrderBy(r => r.DisplayOrder).ToList();
                            ReorderAccounts(virtualAccounts, direction, account);
                        }
                        else
                        {
                            List<Account> accounts = allData.Accounts.Where(r => r.IsVirtualAcount == false).OrderBy(r => r.DisplayOrder).ToList();
                            ReorderAccounts(accounts, direction, account);
                        }
                    }                    
                }
            }
            RefreshAccountList();
        }

        public void ReorderAccounts(List<Account> accounts, int direction, Account account)
        {
            if (accounts.Count > 0)
            {
                int newOrderNum = 0;
                for (int b = 0; b < accounts.Count; b++)
                {
                    if (direction == -1)
                    {
                        if (accounts.Count > b + 1)
                        {
                            if (accounts[b + 1] == account)
                            {
                                // this is the one to move UP!
                                accounts[b + 1].DisplayOrder = newOrderNum;
                                newOrderNum++;
                            }
                        }
                        if (accounts[b] != account)
                        {
                            accounts[b].DisplayOrder = newOrderNum;
                            newOrderNum++;
                        }
                    }
                    else
                    {
                        if (accounts[b] == account)
                        {
                            if (accounts.Count > b + 1)
                            {
                                accounts[b + 1].DisplayOrder = newOrderNum;
                                newOrderNum++;
                                accounts[b].DisplayOrder = newOrderNum;
                                newOrderNum++;
                                b++;
                            }
                        }
                        else
                        {
                            accounts[b].DisplayOrder = newOrderNum;
                            newOrderNum++;
                        }
                    }
                }
            }
        }

        public void RefreshData()
        {
            cbCategory.DataSource = GetCategoryComboBoxList(false, true);
            cbCategory.AutoCompleteSource = AutoCompleteSource.ListItems;

            cbPayee.DataSource = GetPayeeComboBoxList();
            cbPayee.AutoCompleteSource = AutoCompleteSource.ListItems;

            cbTransfer.DataSource = GetAccountsComboBoxList(false, true);
            cbTransfer.AutoCompleteSource = AutoCompleteSource.ListItems;
        }

        private void reconcileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Account account = GetAccount(currentAccountId);

            if(account.IsVirtualAcount)
            {
                MessageBox.Show("Virtual accounts are reconciled with the master account.");
                return;
            }

            if (FormReconcile.instance == null || FormReconcile.instance.IsDisposed)
            {
                new FormReconcile();
            }

            FormReconcile.instance.Init(this, account);
            FormReconcile.instance.Show();
            FormReconcile.instance.BringToFront();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            RefreshTransactionList();
        }

        private void accountsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenDialogAccounts();
        }

        private void trace(string str)
        {
            MessageBox.Show(str);
        }

        private void btnDeleteTransaction_Click(object sender, EventArgs e)
        {
            int i = dgTransactions.CurrentCell.RowIndex;

            int id = int.Parse(dgTransactions.CurrentRow.Cells[0].Value.ToString());

            Transaction transaction = allData.Transactions.FirstOrDefault(r => r.Id == id);

            if(transaction == null)
            {
                MessageBox.Show("Couldn't find a transaction to delete!");
            }
            else
            {
                decimal amount = transaction.Deposit - transaction.Payment;
                if (MessageBox.Show("Are you sure you want to delete this transaction?\r\n\r\n" + transaction.Date.ToShortDateString() + " " + transaction.Name + " " + MoneyFormat(amount), "DT Virtual Accounts | Delete Transaction", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    // the total is stored on the account, let's update that
                    Account account = GetAccount(transaction.AccountId);
                    account.Value -= amount;
                    if(transaction.IsTransfer)
                    {
                        Account transferAccount = GetAccount(transaction.TransferAccountId);
                        transferAccount.Value += amount;
                    }
                    allData.Transactions.Remove(transaction);
                    RefreshAccountList(); 
                    RefreshTransactionList();
                    RefreshData();
                    PlaySuccessSound();
                }
            }
        }

        private void DoTransactionsMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex < 0) return;
            string idStr = dgTransactions.Rows[e.RowIndex].Cells[0].Value.ToString();
            int id = int.Parse(idStr);
            if (e.Button == MouseButtons.Right)
            {
                
                EditTransaction(id);
            }
        }

        public void EditTransaction(int id)
        {
            Transaction t = GetTransaction(id);

            if(t == null)
            {
                MessageBox.Show("Could not find transaction to edit!");
                return;
            }

            Account account = GetAccount(t.AccountId);

            if(account == null)
            {
                MessageBox.Show("Could not find account for transaction to edit!");
                return;
            }

            if(account.Id != currentAccountId)
            {
                currentAccountId = account.Id;
                RefreshAccountList(true);
                RefreshTransactionList();
                RefreshData();
            }

            editTransactionId = t.Id;

            tfDeposit.Text = t.Deposit.ToString();
            tfMemo.Text = t.Memo;
            tfNum.Text = t.Num;
            tfPayment.Text = t.Payment.ToString();
            cbCategory.Text = GetCategory(t.CategoryId).Name;
            cbPayee.Text = t.Name;
            if (t.TransferAccountId != 0)
            {
                account = GetAccount(t.TransferAccountId);
                cbTransfer.Text = account.Name;
            }
            else
            {
                cbTransfer.Text = "";
            }
            dtDate.Value = t.Date;

            gbEntry.Text = "Edit Transaction";

            this.Show();// bubble up
            this.BringToFront();

        }

        private void btnNewTransaction_Click(object sender, EventArgs e)
        {
            editTransactionId = 0;
            ClearTransaction();
        }

        private void ClearTransaction()
        {
            gbEntry.Text = "New Transaction";
            tfDeposit.Text = "";
            tfMemo.Text = "";
            tfNum.Text = "";
            tfPayment.Text = "";
            cbCategory.Text = "";
            cbPayee.Text = "";
            cbTransfer.Text = "";
            dtDate.Value = DateTime.Now;
        }
    }
}
