<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/php/basic.php';clearQueryLogs();

query("SET SQL_BIG_SELECTS=1");

ini_set('memory_limit', '250M');

$userPermTable = strtolower($loggedInUserPerm);
if($loggedInUserPerm == 'VPSales') $userPermTable = 'regional';

// combined the search with the normal query
$searchAnd = '';

///////////////////////////CUSTOM SEARCH PARAMETERS/////////////////////////////////////////////////////
if(get('searchFormSubmitted') == '1')
{
	// commercial implementation still needs to be made
	// it looks like this is using the old salesstaff IN method
	// this was replaced with the pvr_sales_reps table
	
	$dateFrom = get('dateFrom');
	$dateTo = get('dateTo');
	
	if($dateFrom != '')
	{
		$dateFrom = date('Y-m-d', strtotime($dateFrom));
		$searchAnd .= " AND Daily_Shipments_7311.DocDate >= '$dateFrom'";
	}
	if($dateTo != '')
	{
		$dateTo = date('Y-m-d', strtotime($dateTo));
		$searchAnd .= " AND Daily_Shipments_7311.DocDate <= '$dateTo'";
	}
	
	if(get('poNum') != '')
	{
		$searchAnd .= " AND Daily_Shipments_7311.ponum='" . get('poNum') . "'";
	}
	
	if(get('soNum') != '')
	{
		$sonum = get('soNum');
		if(substr(strtolower($sonum), 0, 3) !== 'so-')
		{
			$sonum2 = 'SO-' . $sonum;
			$searchAnd .= " AND (Daily_Shipments_7311.sonum='$sonum' OR Daily_Shipments_7311.sonum='$sonum2')";
		}
		else
		{
			$searchAnd .= " AND Daily_Shipments_7311.sonum='$sonum'";
		}		
	}
	
	if($loggedInUserPerm != 'Sales')
	{
		if(get('salesman') != '' && get('salesman') != 'all')
		{
			$searchAnd .= " AND customers.sellnum IN('" . str_replace(",","','", get('salesman')) . "')";
		}
	}
	if(get('suppliers') != '' && get('suppliers') != 'all')
	{
		$searchAnd .= " AND customers.custnum IN('" . str_replace(",","','", get('suppliers')) . "')";
	}
	
	if(get('pvrNum') != '')
	{
		$searchAnd .= " AND Daily_Shipments_7311.pricing LIKE '%" . get('pvrNum') . "%'";
	}
	
	session('openOrdersSearchAnd', $searchAnd);
	
}
else
{
	// we need to carry over the searchAnd (for pagination and other sorts), unless the tab button was presses
	if(get('isSortClick') == '1' || get('paginationClick') == '1')
	{
		$searchAnd = session('openOrdersSearchAnd');
	}
	else
	{
		session('openOrdersSearchAnd', '');
	}
}
	
queryi($link, "SET max_allowed_packet=356M");

if($loggedInUserPerm == 'Admin' || $loggedInUserPerm == 'VPSales' || $loggedInUserPerm == 'National Commercial')
{
	$fromStmt = "FROM Daily_Shipments_7311 LEFT JOIN customers ON Daily_Shipments_7311.cnum = customers.custnum WHERE (Daily_Shipments_7311.orderStatus = 'BP' OR Daily_Shipments_7311.orderStatus = 'NP') $searchAnd ORDER BY Daily_Shipments_7311.salesstaff, Daily_Shipments_7311.cname, Daily_Shipments_7311.cname";
		
}
elseif($loggedInUserPerm == 'District' || $loggedInUserPerm == 'Regional')
{
	$fromStmt = "FROM Daily_Shipments_7311 LEFT JOIN customers ON Daily_Shipments_7311.cnum = customers.custnum LEFT JOIN SalesStaffRef ON customers.sellnum = SalesStaffRef.sellnum WHERE SalesStaffRef.$userPermTable = '".$loggedInUser."' AND Daily_Shipments_7311.orderStatus != 'S' AND Daily_Shipments_7311.orderStatus != 'C' AND Daily_Shipments_7311.salesstaff != '0' AND Daily_Shipments_7311.salesstaff != '199' $searchAnd ORDER BY Daily_Shipments_7311.salesstaff, Daily_Shipments_7311.cname"; 
}
elseif($loggedInUserPerm == 'Sales')
{
	$sellnumOr = '(' . getCombinedSellnumDatabaseOr($loggedInUserRecord['sellnums'], 'customers.sellnum') . ')';
	
	$fromStmt = "FROM Daily_Shipments_7311 LEFT JOIN customers ON Daily_Shipments_7311.cnum = customers.custnum WHERE $sellnumOr AND orderStatus != 'S' AND orderStatus != 'C' AND salesstaff != '0' AND salesstaff != '199' $searchAnd ORDER BY salesstaff, cname, cname"; 
}
elseif($loggedInUserPerm == 'Commercial')
{
	$linkedSellnums = $loggedInUserRecord['linked_sellnums'];
	$array = explode('-,-', substr($linkedSellnums, 1, strlen($linkedSellnums) - 2));
	
	$commercialSellnumsAnd = " AND customers.sellnum IN ('" . implode("','", $array) . "') ";
	
	$fromStmt = "FROM Daily_Shipments_7311 LEFT JOIN customers ON Daily_Shipments_7311.cnum = customers.custnum  LEFT JOIN SalesStaffRef ON customers.sellnum = SalesStaffRef.sellnum WHERE Daily_Shipments_7311.orderStatus != 'S' AND Daily_Shipments_7311.orderStatus != 'C' AND Daily_Shipments_7311.salesstaff != '0' AND Daily_Shipments_7311.salesstaff != '199' $commercialSellnumsAnd $searchAnd ORDER BY Daily_Shipments_7311.salesstaff, Daily_Shipments_7311.cname";
}

//echo 'page = ' . get('page');exit;

if(get('page') == '')
{
	$page = 1;
}
else
{
	$page = get('page');
	if(!is_numeric($page))
	{
		$page = 1;
	}
}

//alert($page);


$results_per_page = 50; 

// speed up queries by grabbing this info once
$allUsers = queryResult("SELECT * FROM Malarkey_Staff_Users");


// query the items you want to display on the current page from the database
$itemFrom = ($page - 1) * $results_per_page;
if($itemFrom < 0) $itemFrom = 0;

if(get('paginationClick') == '1' || get('isSortClick') == '1')
{
	$stmt = $fromStmt;
	$orderBy = session('orderBy');
	if(get('isSortClick') == '1')
	{
		$orderBy = get('orderBy');
		session('orderBy', $orderBy);
	}
	if(strpos($stmt, 'ORDER BY') !== false)
	{
		if(session('orderBy') != '')
		{
			$stmt = substr($stmt, 0, strpos($stmt, 'ORDER BY') + 9) . 'Daily_Shipments_7311.' . $orderBy . ',' . substr($stmt, strpos($stmt, 'ORDER BY') + 8);
		}
	}
	else
	{
		if($orderBy != '')
		{
			$stmt .= ' ORDER BY ' . 'Daily_Shipments_7311.' . $orderBy;
		}
	}
	
	$stmt = "SELECT Daily_Shipments_7311.id, Daily_Shipments_7311.sonum $stmt";
}
else
{
	$stmt = $fromStmt;
	session('orderBy', get('orderBy'));
	if(strpos($stmt, 'ORDER BY') !== false)
	{
		if(session('orderBy') != '')
		{
			$stmt = substr($stmt, 0, strpos($stmt, 'ORDER BY') + 9) . 'Daily_Shipments_7311.' . session('orderBy') . ',' . substr($stmt, strpos($stmt, 'ORDER BY') + 8);
		}
	}
	else
	{
		if(session('orderBy') != '')
		{
			$stmt .= ' ORDER BY Daily_Shipments_7311.' . session('orderBy');
		}
	}
	
	$stmt = "SELECT Daily_Shipments_7311.id, Daily_Shipments_7311.sonum $stmt";
}

$fullResults = queryResult($stmt);
$results = array();

$resultsPerPage = $results_per_page;
$pages = ceil(count($fullResults) / $resultsPerPage);

if(count($fullResults) > 0)
{
	$idArray = array();
	$startingNum = ($page - 1) * $resultsPerPage;
	for($a = $startingNum; $a < $startingNum + $resultsPerPage; $a++)
	{
		if(count($fullResults) < $a + 1) break;
		array_push($idArray, $fullResults[$a]['id']);
	}
	
	$idStr = "'" . implode("','", $idArray) . "'";
	
	$orderBy = session('orderBy');
	if($orderBy != '') $orderBy = 'ORDER BY ' . $orderBy;
	
	$idStmt = "SELECT customers.sellnum, Daily_Shipments_7311.* FROM Daily_Shipments_7311 LEFT JOIN customers ON Daily_Shipments_7311.cnum = customers.custnum WHERE Daily_Shipments_7311.id IN($idStr) $orderBy";
	
	$results = queryResult($idStmt);
}

$pagination = paginationAngular($pages, $page, true, session('orderBy'), false, true);

$showCompareLogic = true;

$reportName = 'Report Total';

include $_SERVER['DOCUMENT_ROOT'] . '/pages/angular/ajax-orders-include.php';

?>