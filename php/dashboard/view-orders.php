<div id="pvr-tiles" class="ordersContentDiv seven wide left floated column"> 
    
    <div id="pvr-pag" class="pvr-pag-div" ng-bind-html="pagination|trustAsHtml" ng-if="showPagnination"></div>
           
    <div id="orders-scrolly-div" class="scrolly column">
        
        <div class="ui divided items">
            
            <div id="angularOrdersDiv" class="content" ng-show="showOrderRecords">
                <div ng-if="true">
                    <i class="green checkmark icon"></i> = Salesperson has reviewed open order and it is accurate with respect to customer, product, quantity,  &amp; price.<br />
                    <i class="red checkmark icon"></i> = Sales order has changed and needs to be validated again.
                </div>
                
                <div ng-if="showValidatedTallies" ng-bind-html="validatedTallies|trustAsHtml"></div>
                
                <div class="ui two column stackable divided grid segment pvr-tile" ng-if="showEmptyResults">
                    <div class="ten wide divided column">
                        <div class="light sub-header">No Results</div>
                    </div>
                </div>
                
                <div id="pvr-tile-{{ orderRecord.id }}" class="ui two column stackable divided grid segment pvr-tile pvr-tile2" ng-click="clickDetails(orderRecord.id)" ng-repeat="orderRecord in orderRecords">
                    <div class="ten wide divided column">
                        
                        <div class="ui green header">
                        	<img class="validation-check" id="leftSideValidatedCheck-{{ orderRecord.id }}" src="/images/greenCheckBigOutline.png" ng-show="orderRecord.validated" />
                            <img class="validation-check" id="leftSideValidatedCheck-{{ orderRecord.id }}" src="/images/redCheckBig.png" ng-show="!orderRecord.validated && orderRecord.redValidated" />
                            {{ orderRecord.cname }}
                        </div>
                        <div class="ui divider"></div>
                        <div class="light sub-header">
                            {{ orderRecord.salespeople }}<br />{{ orderRecord.regionalName }}<br />{{ orderRecord.districtName }}
                        </div>
                    </div>
                    <div class="six wide divided column">
                        <div class="sub-header"><span>INV#:</span><strong>{{ orderRecord.invnum }}</strong></div>
                        <div class="sub-header"><span>SO#:</span><strong>{{ orderRecord.sonum }}</strong></div>
                        <div class="sub-header"><span>PO#:</span><strong>{{ orderRecord.ponum }}</strong></div>
                        <div class="sub-header"><span>PRICING:</span>
                            <strong>
                                {{ orderRecord.contract }} 
                                <span ng-repeat="pricingField in orderRecord.pricingFields"><span style="color:{{ pricingField.color }};">{{ pricingField.pvr }}</span><br /></span>
                            </strong>
                        </div>
                    </div>
                </div>
            </div>       
        </div>                        
    </div> 
    <div id="orders-left-side-shamrock" class="gifWrap" ng-if="showOrderWaiting"><img src="/images/loading.gif" class="gifLogo"></div>        
</div>