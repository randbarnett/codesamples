<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/php/basic.php';

set_time_limit(0);	
ini_set('memory_limit', '1000M');

$results = queryResult("SELECT * FROM Daily_Shipments_7311 WHERE orderStatus != 's' AND validated='0'");

//echo queryResultsToTable($results);exit;

$returnResults = array();

if(count($results) > 0)
{
	foreach($results as $orderRecord)
	{
		$record = $orderRecord;
		$validatedStr = validatedOrderStr($orderRecord['sonum']);
		$record['redValidated'] = '0';
		if($validatedStr != '')
		{
			$record['redValidated'] = '1';
			$record['redValidationDetails'] = str_replace('<br />', "\r\n", compareSnapshotString(createOrderSnapshot($orderRecord['sonum']), getLastValidatedSnapshot($orderRecord['sonum'])));
			
			array_push($returnResults, $record);
		}
	}
}

// now select what keys we are sending
$keys = array();
$keys['id'] = 'id';
$keys['cnum'] = 'cnum';
$keys['cname'] = 'cname';
$keys['sonum'] = 'sonum';
$keys['DocDate'] = 'OrderDate';
$keys['status'] = 'status';
$keys['orderStatus'] = 'OrderStatus';
$keys['validated'] = 'validated';
$keys['redValidated'] = 'redValidated';
$keys['redValidationDetails'] = 'redValidationDetails';

//echo queryResultsToTable($returnResults);exit;
queryResultsToCSV($returnResults, 'Red Validation Report ' . date('Y-m-d_H-i-s'), $keys);

?>