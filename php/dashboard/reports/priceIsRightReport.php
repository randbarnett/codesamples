<?php

include $_SERVER['DOCUMENT_ROOT'] . '/php/basic.php';

set_time_limit(0);	
ini_set('memory_limit', '1000M');

$quantityGreaterThan = post('quantity');

$salesOrderPricingExceptions = array();
array_push($salesOrderPricingExceptions, 'sample');
array_push($salesOrderPricingExceptions, 'claim');
array_push($salesOrderPricingExceptions, 'test');
array_push($salesOrderPricingExceptions, 'demo');
array_push($salesOrderPricingExceptions, 'donation');
array_push($salesOrderPricingExceptions, 'damage');
array_push($salesOrderPricingExceptions, 'n/c');
array_push($salesOrderPricingExceptions, 'n.c');
array_push($salesOrderPricingExceptions, 'no charge');
array_push($salesOrderPricingExceptions, 'no cost');
array_push($salesOrderPricingExceptions, 'sample');

$productNameExceptions = array();
array_push($productNameExceptions, 'adjustment');
array_push($productNameExceptions, '#2');

$poExceptions = array();
array_push($poExceptions, 'claim');
array_push($poExceptions, 'sample');

function hasException($array, $field)
{
	foreach($array as $record)
	{
		if(strpos(strtoupper($field), strtoupper($record)) !== false) return true;
	}
	return false;
}

//$csvFile = fopen($_SERVER['DOCUMENT_ROOT'] . '/pages/axPVRCompareReport.csv', 'a+');

$showEcho = false;

$csvArray = array();// an array of all line item arrays

function writeCSVLine($array)
{
	global $csvArray;	
	array_push($csvArray, $array);
}

$headers = array();
array_push($headers, 'Product Id');
array_push($headers, 'Product Name');
array_push($headers, 'Sales Order Pricing');
array_push($headers, 'Conflict Pricing');
array_push($headers, 'Quantity');
array_push($headers, 'Price');
array_push($headers, 'Line Total');
array_push($headers, 'Customer#');
array_push($headers, 'Customer Name');
array_push($headers, 'PO#');
array_push($headers, 'SO#');
array_push($headers, 'Order Date');
array_push($headers, 'List Price');
array_push($headers, 'Percentage of List Price');

writeCSVLine($headers);


function doEcho($str, $isSummary = false)
{
	global $showEcho;
	
	if(!$showEcho) return;
	
	echo $str;
}

set_time_limit(0);	
ini_set('memory_limit', '1000M');

elapsedTimeStart();

$resetShipDate = date('Y-m-d', strtotime(post('startDate')));// had problems not sending emails.  we'll use this as a new start date and clear the json list of so#s.
$lastBusinessDay = date('Y-m-d', strtotime(post('endDate')));//lastBusinessDay();

$lowPercentage = post('lowPercentage') / 100;
$highPercentage = post('highPercentage') / 100;

if(post('allDates') == '1')
{
	$resetShipDate = '2018-01-01';
	$openOrderLineItems = queryResult("SELECT list_price.price as list_price_val, Daily_Shipments_7311.*, Shipment_Line_Items.quant, Shipment_Line_Items.product, Shipment_Line_Items.pdesc, Shipment_Line_Items.price, Shipment_Line_Items.total, Shipment_Line_Items.contract FROM Daily_Shipments_7311 LEFT JOIN Shipment_Line_Items ON Daily_Shipments_7311.sonum = Shipment_Line_Items.sonum LEFT JOIN list_price ON Daily_Shipments_7311.cnum = list_price.cnum AND Shipment_Line_Items.product = list_price.prodid WHERE list_price.price != '' AND (Shipment_Line_Items.price < list_price.price * $lowPercentage OR Shipment_Line_Items.price > list_price.price * $highPercentage) AND Daily_Shipments_7311.DocDate >= '$resetShipDate' AND Daily_Shipments_7311.cname NOT LIKE '%SAMP%' AND Shipment_Line_Items.product NOT LIKE 'SMP%' AND Daily_Shipments_7311.cname NOT LIKE '%HABIT%' GROUP BY Shipment_Line_Items.id ORDER BY Shipment_Line_Items.sonum");
}
else
{
	$openOrderLineItems = queryResult("SELECT list_price.price as list_price_val, Daily_Shipments_7311.*, Shipment_Line_Items.quant, Shipment_Line_Items.product, Shipment_Line_Items.pdesc, Shipment_Line_Items.price, Shipment_Line_Items.total, Shipment_Line_Items.contract FROM Daily_Shipments_7311 LEFT JOIN Shipment_Line_Items ON Daily_Shipments_7311.sonum = Shipment_Line_Items.sonum LEFT JOIN list_price ON Daily_Shipments_7311.cnum = list_price.cnum AND Shipment_Line_Items.product = list_price.prodid WHERE list_price.price != '' AND (Shipment_Line_Items.price < list_price.price * $lowPercentage OR Shipment_Line_Items.price > list_price.price * $highPercentage) AND Daily_Shipments_7311.DocDate < '$lastBusinessDay' AND Daily_Shipments_7311.DocDate >= '$resetShipDate' AND Daily_Shipments_7311.cname NOT LIKE '%SAMP%' AND Shipment_Line_Items.product NOT LIKE 'SMP%' AND Daily_Shipments_7311.cname NOT LIKE '%HABIT%' GROUP BY Shipment_Line_Items.id ORDER BY Shipment_Line_Items.sonum");
}

doEcho('Num of Open Orders: ' . count($openOrderLineItems) . '<hr />', true);


doEcho(queryResultsToTable($openOrderLineItems));

if(count($openOrderLineItems) > 0)
{
	foreach($openOrderLineItems as $record)
	{
		if(hasException($salesOrderPricingExceptions, $record['contract']) || hasException($productNameExceptions, $record['pdesc']) || hasException($poExceptions, $record['ponum'])) continue;
		$array = array();
		array_push($array, $record['product']);
		array_push($array, $record['pdesc']);
		array_push($array, $record['contract']);
		array_push($array, '(pvr)');
		array_push($array, $record['quant']);
		array_push($array, $record['price']);
		array_push($array, $record['total']);
		array_push($array, $record['cnum']);
		array_push($array, $record['cname']);
		array_push($array, $record['ponum']);
		array_push($array, $record['sonum']);
		array_push($array, $record['DocDate']);
		array_push($array, $record['list_price_val']);
		array_push($array, round(($record['price'] / $record['list_price_val']) * 10000) / 100);
		
		if($quantityGreaterThan != '' && intval($record['quant']) <= $quantityGreaterThan)
		{
			continue;
		}
		
		writeCSVLine($array);
	}
}


if(count($csvArray) > 0)
{
	header('Content-Type: text/csv');
	header('Content-Disposition: attachment; filename="zero-price-report_' . post('startDate') . '_to_' . post('endDate') . '.csv"');
	$csvFile = fopen('php://output', 'wb');
	
	foreach($csvArray as $record)
	{
		fputcsv($csvFile, $record);
	}
	
	fclose($csvFile);
	
	exit;
}
		

?>