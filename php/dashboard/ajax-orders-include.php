<?php

$returnJSON = array();
$jsonResults = array();// we are going to add informtion to each result record and store it here until the end when we will write it to the $returnJSON.

$returnJSON['pagination'] = $pagination;

$rowCount = count($fullResults);

$rowCountLineItems = 0;

// compose an or statement with sonum's
$reportTotal = 0;
if(count($fullResults) > 0)
{
	$orArray = array();
	foreach($fullResults as $record)
	{
		array_push($orArray, $record['sonum']);
	}
	$orStr = "'" . implode("','", $orArray) . "'";
	// note, leaving off  + SUM(COALESCE(FreightTrack.freightinv, 0)
	$sumLineItemStmt = "SELECT SUM(Shipment_Line_Items.total) AS sumofally FROM Shipment_Line_Items LEFT JOIN FreightTrack ON Shipment_Line_Items.sonum = FreightTrack.sonumf WHERE Shipment_Line_Items.sonum IN($orStr)";
	$sumLineItemResult = queryResult($sumLineItemStmt);
	$reportTotal = $sumLineItemResult[0]['sumofally'];
}

$lastReportTotal = '<script type="text/javascript">setReportTotal("' . $reportName . ': "' . trim(omonetize($reportTotal)) . '","' . $rowCount . '");</script>';

$returnJSON['reportTotal'] = $reportName . ': ' . trim(omonetize($reportTotal));
$returnJSON['reportRowCount'] = $rowCount;
$returnJSON['pageNumber'] = $page;
$returnJSON['numberOfPages'] = $pages;
session('lastReportTotal', $lastReportTotal);

if(count($results) > 0)
{
	// speed up queries by grabbing this info once
	$allUsers = queryResult("SELECT * FROM Malarkey_Staff_Users");
		
	for($a = 0; $a < count($results); $a++)
	{
		$orderRecord = $results[$a];
		$id = $orderRecord['id'];
		$sellnumForUserLookup = $orderRecord['salesstaff'];
		if($sellnumForUserLookup == '888') $sellnumForUserLookup = $orderRecord['sellnum'];
		
		$bnrunsaleqryResults = getUsersArray($sellnumForUserLookup, $allUsers);
		$regionalUser = getUserRecord(getRegionalDistrictForSellnum('regional', $sellnumForUserLookup), $allUsers);
		$districtUser = getUserRecord(getRegionalDistrictForSellnum('district', $sellnumForUserLookup), $allUsers);
		
		$nstatus = orderStatus($orderRecord['orderStatus'], $orderRecord['status']);
		$ncarrier = carrier($orderRecord['ShipVia']);
		
		$salespeople = '';
	
		for($b = 0; $b < count($bnrunsaleqryResults); $b++)
		{
			$bnsalecount = $bnrunsaleqryResults[$b];					
			$salespeople .= $bnsalecount['firstn']." ".$bnsalecount['lastn'];
			if($b < count($bnrunsaleqryResults) - 1)
			{
				//$salespeople .= '&nbsp;&nbsp;|&nbsp;&nbsp;';
				//$salespeople .= '&#160;&#160;|&#160;&#160;';
				$salespeople .= '		|		';
			}                                
		}
		
		$orderRecord['salespeople'] = $salespeople;
		
		$orderRecord['regionalName'] = $regionalUser['firstn'] . ' ' . $regionalUser['lastn'] . ' - Regional Manager';
		$orderRecord['districtName'] = $districtUser['firstn'] . ' ' . $districtUser['lastn'] . ' - District Manager';
		
		
		$validatedStr = validatedOrderStr($orderRecord['sonum']);
		$orderRecord['validated'] = ($orderRecord['validated'] == '1' ? true : false);
		$orderRecord['redValidated'] = false;
		if($orderRecord['validated'] == false && $validatedStr != '') $orderRecord['redValidated'] = true;
		
		$stmt = "SELECT DISTINCT Shipment_Line_Items.sonum, Shipment_Line_Items.contract, Shipment_Line_Items.product, Shipment_Line_Items.pdesc, Shipment_Line_Items.quant, Shipment_Line_Items.quantship, Shipment_Line_Items.price, Shipment_Line_Items.total FROM Daily_Shipments_7311 LEFT JOIN Shipment_Line_Items  ON Daily_Shipments_7311.ponum = Shipment_Line_Items.ponum AND Daily_Shipments_7311.sonum = Shipment_Line_Items.sonum WHERE Daily_Shipments_7311.ponum = '".$orderRecord['ponum']."' AND Daily_Shipments_7311.sonum = '".$orderRecord['sonum']."' ORDER BY Shipment_Line_Items.price DESC";// removed GROUP BY Shipment_Line_Items.product 
		$lineItems = queryResult($stmt);
		
		$pricingExtraArray = array();
		$pricingExtraKeysArray = array();// for deduping
		$firstContract = 'hey';
		
		if(count($lineItems) > 0)
		{
			foreach($lineItems as $line)
			{
				$firstContract = $line['contract'];// we only show one contract for each so in the left side of the display
				
				if($showCompareLogic)
				{
					$conflicts = AXPVRCompareAngularFromSONUM($orderRecord['sonum']);
					
					if(count($conflicts) > 0)
					{
						foreach($conflicts as $conflictRecord)
						{
							if(!in_array($line['contract'] . ' : ' . $conflictRecord['pvr'], $pricingExtraKeysArray))
							{
								array_push($pricingExtraKeysArray, $line['contract'] . ' : ' . $conflictRecord['pvr']);
								array_push($pricingExtraArray, $conflictRecord);
							}
						}
					}
				}
			}
		}
		
		//$orderRecord['pricingField'] =  (count($pricingExtraArray) > 0 ? '<br />' : '');
		//$orderRecord['pricingField'] .= implode('<br />', $pricingExtraArray);
		$orderRecord['contract'] = $firstContract;
		$orderRecord['pricingFields'] = $pricingExtraArray;
		
		array_push($jsonResults, $orderRecord);
	}
}

$returnJSON['results'] = $jsonResults;

if(isset($elapsedTime))
{
	$returnJSON['elapsedTime'] = $elapsedTime;
}
else
{
	$returnJSON['elapsedTime'] = '';
}

if(isset($unvalidatedUserTallyStr))
{
	$returnJSON['unvalidatedUserTally'] = $unvalidatedUserTallyStr;
}
else
{
	$returnJSON['unvalidatedUserTally'] = '';
}

$returnJSON['orderBy'] = session('orderBy');

echo json_encode($returnJSON);

?>