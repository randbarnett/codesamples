<?php
	
	include $_SERVER['DOCUMENT_ROOT'] . '/php/basic.php';
	
	head('Payment', false, true);
	
	
?>

<style type="text/css">
	td
	{
		padding: 3px 6px;
	}
</style>

<script type="text/javascript">

	

</script>

<hr />

<div class="menuPageContainer">
	
    <h3>Payment Info</h3>
    
<?php
	$curryCount = curryCount();
	
	if(isDailySpecialsCart())
	{
		$maxCurries = maxCurriesPerDay(date('Y-m-d'));
	}
	else
	{	
		$maxCurries = maxCurriesPerDay(session('deliveryDate'));
	}
	
	$itemStockMessage = cartHasItemsOutOfStock();
	$maxItemPerDay = maxItemPerDayRemainingWarning();
	
	if((!isDailySpecialsCart()) && ($curryCount < 1 || $curryCount > $maxCurries || $itemStockMessage != '' || $maxItemPerDay != ''))
	{
		
		echo '<div class="redWarning">';
		if($curryCount < 1)
		{
			echo 'You must have at least one curry in the cart to checkout.';
		}
		elseif($curryCount > $maxCurries)
		{
			echo 'There is a maximum of ' . $maxCurries .' curries per order for the date you have selected.';
		}
		elseif($maxItemPerDay != '')
		{
			echo $maxItemPerDay;
		}
		else
		{
			echo $itemStockMessage;
		}
		
		echo '</div><br /><p>Please return to the <a href="/cart">cart</a> to modify your order.</p>';
	}
	elseif(isDailySpecialsCart() && $itemStockMessage != '')
	{
		echo '<div class="redWarning">';
		echo $itemStockMessage;
		echo '</div><br /><p>Please return to the <a href="/cart">cart</a> to modify your order.</p>';
	}
	else
	{
		echo '<h4>Order Summary</h4>';
		
		
		
		$orderObj = createOrderObj();
		$orderId = createOrderRecord($orderObj);		
		session('orderId', $orderId);
		
		$invoiceContent = makeInvoiceContent($orderObj);
		
		echo $invoiceContent;
		
		// Stripe
		
		$paymentIntentErrorArray = array();
		
		require_once($_SERVER['DOCUMENT_ROOT'] . '/php/stripe-php-7.27.0/init.php');
		
		\Stripe\Stripe::setApiKey($stripeSecret);// test 
		
		// create a customer
		
		$clientName = $orderObj['first_name'] . ' ' . $orderObj['last_name'];
		$clientEmail = $orderObj['email'];
		$clientPhone = $orderObj['phone'];
		
		$customer = \Stripe\Customer::create(
		[
			'name' => $clientName,
			'email' => $clientEmail,
			'phone' => $clientPhone
		]);
		//echo $customer;
		
		if(!$customer)
		{
			array_push($paymentIntentErrorArray, 'Could not create customer.');
		}
		else
		{
			
			$intent = \Stripe\PaymentIntent::create(
			[
			  'customer' => $customer->id,
			  'amount' => $orderObj['grandTotal'] * 100,
			  'currency' => 'usd',
			  'payment_method_types' => ['card'],
			  'metadata' =>
			  [
			  	'clientName' => $clientName,
				'orderID' => $orderId,
				'email' => $clientEmail,
				'phone' => $clientPhone
			  ]
			]);
			
			$intentAmountReadable = omonetize($intent->amount / 100);
			
			query("UPDATE orders SET payment_intent_id='" . es($intent->id) . "' WHERE id='" . es($orderId) . "'");
			
?>

	<script type="text/javascript">
		
		function stripeTokenHandler(token)
		{
		  // Insert the token ID into the form so it gets submitted to the server
		  var form = document.getElementById('payment-form');
		  var hiddenInput = document.createElement('input');
		  hiddenInput.setAttribute('type', 'hidden');
		  hiddenInput.setAttribute('name', 'stripeToken');
		  hiddenInput.setAttribute('value', token.id);
		  form.appendChild(hiddenInput);
		
		  // Submit the form
		  form.submit();
		}
		
		$(document).ready(function()
		{
			
			// Set your publishable key: remember to change this to your live publishable key in production
			// See your keys here: https://dashboard.stripe.com/account/apikeys
			
			var stripe = Stripe('<?php echo $stripePublic; ?>');// test account 
			
			var elements = stripe.elements();
			
			// Set up Stripe.js and Elements to use in checkout form
			var style = {
			  base: {
				color: "#32325d",
			  }
			};
			
			
			
			var card = elements.create('card');
			card.mount('#card-element');
			
			card.addEventListener('change', ({error}) => {
			  const displayError = document.getElementById('card-errors');
			  if (error) {
				displayError.textContent = error.message;
			  } else {
				displayError.textContent = '';
			  }
			});			
			
			var form = document.getElementById('payment-form');
			
			form.addEventListener('submit', function(ev) {
			  ev.preventDefault();
			  stripe.confirmCardPayment('<?= $intent->client_secret ?>', {
				payment_method: {
				  card: card,//cardNumber,
				  billing_details: {
					name: "<?php echo inputValue($clientName); ?>"
				  }
				}
			  }).then(function(result) {
				if (result.error) {
				  // Show error to your customer (e.g., insufficient funds)
				  console.log(result.error.message);
				  const displayError = document.getElementById('card-errors');
				  displayError.textContent = result.error.message;
				  //$("#card-errors").html("<p>Something went wrong with your payment:</p><p>" + result.error.message + "</p>");
				} else {
				  // The payment has been processed!
				  if (result.paymentIntent.status === 'succeeded') {
					// Show a success message to your customer
					// There's a risk of the customer closing the window before callback
					// execution. Set up a webhook or plugin to listen for the
					// payment_intent.succeeded event that handles any business critical
					// post-payment actions.
					
					$("#payment-form").hide();
					$("#successMessage").html("<p>Your payment was successful!</p><p>Once your payment has been confirmed, you will be sent a confirmation email with your invoice (usually within a few seconds).  If you do not receive the email, please go to the <a href='/contact'>contact</a> page and send me an email.</p>");
					
					$.get("/dailySpecialsCartSuccess", {}, function(pStr)
					{
						
					});
					
					$.get("/emptyCart", { "clearDate" : "1" }, function(pStr)
					{
						$.get("/cartCount", {}, function(pStr2)
						{
							$("#cartDiv").html(pStr2);
						});
					});
				  }
				}
			  });
			});
		});

	</script>
    
    Enter your credit card number:<br /><br />
    
    <div class="stripePaymentIntentDiv">

        <form action="/charge" method="post" id="payment-form">
          <div class="form-row">
            <label for="card-element">
              Credit or debit card
            </label>
            <div id="card-element" style="width: 350px;">
              <!-- A Stripe Element will be inserted here. -->
            </div>
        
            <!-- Used to display form errors. -->
            <div id="card-errors" role="alert" class="redWarning"></div>
          </div>
          
          <br />
        
          <button>Submit Payment</button>
        </form>
    	

		<div id="successMessage"></div>
        
    </div>
    
    <div style="height: 100px;"></div>
    
<?php
		}
	}
?>
</div>
    
<?php
	
	foot();
	
?>