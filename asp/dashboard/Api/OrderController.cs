﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dashboard.v2.Data;
using Dashboard.v2.ApiModels;
using Dashboard.v2.Core;
using System.Text.Json;
using Dashboard.v2.Utilities;
using Dashboard.v2.Data.CompositeModels;
using Dashboard.v2.Emails;
using Dashboard.v2.Emails.Models;

namespace Dashboard.v2.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IWebHostEnvironment environment;
        private readonly IOrderData orderData;
        private readonly IUserData userData;
        private readonly IDashboardData dashboardData;
        private readonly IRazorViewToStringRenderer emailRenderer;
        private List<OrderLineItem> totalLineItems;

        public OrderController(IWebHostEnvironment environment, IOrderData orderData, IUserData userData, IDashboardData dashboardData, IRazorViewToStringRenderer emailRenderer)
        {
            this.environment = environment;
            this.orderData = orderData;
            this.userData = userData;
            this.dashboardData = dashboardData;
            this.emailRenderer = emailRenderer;
        }

        [HttpGet("{type}")]
        public string Index(string type, int page, string orderBy, bool paginationClick, string pvrnum, string sonum, string ponum, string dateFrom, string dateTo, string customers, string salesReps, bool isSearch, bool clearSearch)
        {
            User loggedInUser = Utilities.SessionHelper.LoggedInUser(HttpContext.Session, userData);
            if (loggedInUser == null)
            {
                return DashboardUtilities.APIFailureJSON("Not logged in.");
            }

            SearchTerms searchTerms = new SearchTerms();

            if (isSearch)
            {                
                searchTerms.pvrnum = pvrnum;
                searchTerms.sonum = sonum;
                searchTerms.ponum = ponum;
                searchTerms.customers = customers;
                searchTerms.salesReps = salesReps;
                if(dateFrom != null && dateFrom != "") searchTerms.dateFrom = Utilities.Utilities.DateNoTimeToUTC(DateTime.Parse(dateFrom));
                if (dateTo != null && dateTo != "") searchTerms.dateTo = Utilities.Utilities.DateNoTimeToUTC(DateTime.Parse(dateTo));
                SessionHelper.SetObjectAsJson(HttpContext.Session, "searchTerms", searchTerms);
            }
            else if(!clearSearch)
            {
                SearchTerms searchTermsObj = SessionHelper.GetObjectFromJson<SearchTerms>(HttpContext.Session, "searchTerms");
                if (searchTermsObj != null)
                {
                    searchTerms = searchTermsObj;
                }
            }
            else if(clearSearch)
            {
                SessionHelper.SetObjectAsJson(HttpContext.Session, "searchTerms", null);
            }
            

            if (paginationClick == false)
            {
                if(orderBy == "false")
                {
                    Utilities.SessionHelper.SetStringValue(HttpContext.Session, "orderBy", "");
                }
                else
                {
                    Utilities.SessionHelper.SetStringValue(HttpContext.Session, "orderBy", orderBy);
                }
            }
            else
            {
                orderBy = Utilities.SessionHelper.GetStringValue(HttpContext.Session, "orderBy");
            }

            List<string> sellNumsForUser = dashboardData.SellNumsForUser(loggedInUser);

            ApiOrderModel model = new ApiOrderModel();
            model.sellNums = sellNumsForUser;
            model.type = type;
            List<OrderHeader> totalOrders = null;

            int resultsPerPage = 50;
            int resultStart = (page - 1) * resultsPerPage;

            if (type == "yesterday")
            {
                DateTime today = Utilities.Utilities.TodaysDateInUTC();
                totalOrders = orderData.GetHeaders(today.AddDays(-1), today, false, 0, int.MaxValue, sellNumsForUser, orderBy, searchTerms, loggedInUser.InstallationId).ToList(); ;
            }
            else if (type == "orders")
            {
                totalOrders = orderData.GetOrdersNotShipped(0, int.MaxValue, sellNumsForUser, orderBy, searchTerms, loggedInUser.InstallationId).ToList();
            }
            else if (type == "shipments")
            {
                DateTime today = Utilities.Utilities.TodaysDateInUTC();
                DateTime dateFrom2 = today.AddDays(-1);
                DateTime dateTo2 = today;
                if(dateFrom != "")
                {
                    dateFrom2 = searchTerms.dateFrom.GetValueOrDefault();
                    dateTo2 = searchTerms.dateTo.GetValueOrDefault();
                }
                orders = orderData.GetHeaders(dateFrom2, dateTo2, true, resultStart, resultsPerPage, sellNumsForUser, orderBy, searchTerms, loggedInUser.InstallationId).ToList();
                totalOrders = orders;
            }
            else if(type == "unvalidated")
            {
                totalOrders = orderData.GetOrdersNotValidated(0, int.MaxValue, sellNumsForUser, orderBy, searchTerms, loggedInUser.InstallationId).ToList();

                if(totalOrders.Count > 0)
                {
                    ApiUnvalidatedTallyRecord tallyRecord;
                    bool hit = false;
                    foreach(OrderHeader h in totalOrders)
                    {
                        hit = false;
                        if(model.unvalidatedTallyRecords.Count > 0)
                        {
                            foreach(ApiUnvalidatedTallyRecord r in model.unvalidatedTallyRecords)
                            {
                                if(r.SellNum == h.SellNum)
                                {
                                    hit = true;
                                    r.Tally++;
                                    break;
                                }
                            }
                        }
                        if(!hit)
                        {
                            tallyRecord = new();
                            tallyRecord.SellNum = h.SellNum;
                            tallyRecord.SellNames = DashboardUtilities.SalesNamesForSellNum(h.SellNum, userData);
                            if (tallyRecord.SellNames == "") tallyRecord.SellNames = "Blank";
                            tallyRecord.Tally = 1;
                            model.unvalidatedTallyRecords.Add(tallyRecord);
                        }
                    }

                    model.unvalidatedTallyRecords = model.unvalidatedTallyRecords.OrderByDescending(r => r.Tally).ToList();

                    // now by Contract (PVR)
                    foreach (OrderHeader h in totalOrders)
                    {
                        hit = false;
                        if (model.unvalidatedTallyRecordsByContract.Count > 0)
                        {
                            foreach (ApiUnvalidatedTallyRecord r in model.unvalidatedTallyRecordsByContract)
                            {
                                if (r.Contract == h.Contract)
                                {
                                    hit = true;
                                    r.Tally++;
                                    break;
                                }
                            }
                        }
                        if (!hit)
                        {
                            tallyRecord = new();
                            tallyRecord.SellNum = h.SellNum;
                            tallyRecord.SellNames = DashboardUtilities.SalesNamesForSellNum(h.SellNum, userData);
                            if (tallyRecord.SellNames == "") tallyRecord.SellNames = "Blank";
                            tallyRecord.Contract = h.Contract;
                            if (tallyRecord.Contract == "") tallyRecord.Contract = "Blank";
                            tallyRecord.Tally = 1;
                            model.unvalidatedTallyRecordsByContract.Add(tallyRecord);
                        }
                    }

                    model.unvalidatedTallyRecordsByContract = model.unvalidatedTallyRecordsByContract.OrderByDescending(r => r.Tally).ToList();
                }
            }

            if(totalOrders != null && totalOrders.Count > 0)
            {
                OrderHeader record;
                for(int a = (page - 1) * resultsPerPage; a < page * resultsPerPage; a++)
                {
                    if (totalOrders.Count < a + 1) break;
                    record = totalOrders[a];
                    ApiOrderRecord item = new ApiOrderRecord();
                    item.cname = record.CustomerName;
                    item.cnum = record.CustomerNum;
                    if(record.SellNum != "")
                    {
                        string sellnum = record.SellNum;
                        item.salespeople = DashboardUtilities.SalesNamesForSellNum(sellnum, userData);
                        item.districtName = DashboardUtilities.DistrictNameForSellNum(sellnum, userData);
                        if (item.districtName != "") item.districtName += " - District Manager";
                        item.regionalName = DashboardUtilities.RegionalNameForSellNum(sellnum, userData);
                        if (item.regionalName != "") item.regionalName += " - Regional Manager";
                    }

                    item.sonum = record.SONum;
                    item.shipdate = record.ShipDate.ToString();
                    item.ponum = record.PONum;
                    item.id = record.Id;
                    item.contract = record.Contract;
                    item.validated = record.Validated;
                    item.wasValidated = record.WasValidated;
                    model.results.Add(item);
                    
                }

                populateTotals(model, totalOrders);

                model.resultsPerPage = resultsPerPage;
                model.numberOfPages = (int)Math.Ceiling(((decimal)totalOrders.Count / resultsPerPage));
                model.pageNumber = page;
                model.orderBy = orderBy;
            }
            else
            {
                //return "no records";
            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
        }

        [HttpGet("details/{id}")]
        public string GetOrder(int id)
        {
            User loggedInUser = Utilities.SessionHelper.LoggedInUser(HttpContext.Session, userData);
            if (loggedInUser == null)
            {
                return DashboardUtilities.APIFailureJSON("Not logged in.");
            }

            ApiOrderDetailsModel model = new ApiOrderDetailsModel();
            OrderHeader orderRecord = orderData.GetHeader(id);

            if(orderRecord != null)
            {
                model.id = orderRecord.Id;
                model.cname = orderRecord.CustomerName;
                model.cnum = orderRecord.CustomerNum;
                model.currency = orderRecord.Currency;
                model.DocDate = orderRecord.OrderDate.GetValueOrDefault().ToLocalTime().ToString("f");
                model.sonum = orderRecord.SONum;
                model.ponum = orderRecord.PONum;
                model.shipdate = orderRecord.ShipDate.ToString();
                model.ShipFrom = orderRecord.ShipFromSite;
                model.ShipTo = orderRecord.ShipToCity;
                model.ShipVia = orderRecord.ModeOfDelivery;
                model.terms_of_delivery = orderRecord.TermsOfDelivery;
                model.shipdate = orderRecord.ShipDate.ToString();
                model.status = Utilities.DashboardUtilities.OrderStatusMutator(orderRecord.Status);

                List<OrderLineItem> lineItems = orderData.GetLineItems(orderRecord.SONum, Utilities.SessionHelper.InstallationId).ToList();

                decimal lineItemTotal = 0;

                if(lineItems.Count > 0)
                {
                    foreach(OrderLineItem item in lineItems)
                    {
                        ApiOrderDetailsLineItem record = new ApiOrderDetailsLineItem();
                        record.id = item.Id;
                        record.pdesc = item.ProductDescription;
                        record.product = item.Product;
                        record.price = Utilities.Utilities.MoneyFormat(item.Price);
                        record.quant = item.Quantity.ToString();
                        record.quantship = item.QuantityShipped.ToString(); ;
                        record.total = Utilities.Utilities.MoneyFormat(item.Total);
                        record.compare = item.Contract;
                        model.lineItems.Add(record);

                        lineItemTotal += item.Total;
                    }
                }

                model.orderTotal = Utilities.Utilities.MoneyFormat(lineItemTotal);

                List<OrderValidation> validations = orderData.GetOrderValidations(Utilities.SessionHelper.InstallationId, orderRecord.SONum);
                if(validations.Count > 0)
                {
                    ApiOrderValidation apiValidationRecord;
                    User validationUser;
                    foreach(OrderValidation validation in validations)
                    {
                        apiValidationRecord = new();
                        validationUser = userData.FindById(validation.UserId);
                        if(validationUser != null)
                        {
                            apiValidationRecord.FirstName = validationUser.FirstName;
                            apiValidationRecord.LastName = validationUser.LastName;
                        }
                        apiValidationRecord.DateTimeString = validation.DateTime.ToString();
                        if(validation.SuperAdminId != 0 && validationUser.Id != validation.SuperAdminId)
                        {
                            apiValidationRecord.SuperAdminString = "(" + validation.SuperAdminId.ToString() + ")";
                        }

                        model.orderValidations.Add(apiValidationRecord);
                    }
                }
                
            }

            return JSONHelper.Serialize(model);

           
            string json = System.IO.File.ReadAllText(environment.WebRootPath + "/assets/StaticFiles/OrderDetails.json");

            if (string.IsNullOrEmpty(json))
            {
                return "not found";
            }
            return json;
            
        }

        [HttpGet("validate/{id}")]
        public string Validate(int id)
        {
            User loggedInUser = Utilities.SessionHelper.LoggedInUser(HttpContext.Session, userData);
            if (loggedInUser == null)
            {
                return DashboardUtilities.APIFailureJSON("Not logged in.");
            }

            OrderHeader orderHeader = orderData.GetHeader(id);
            if(orderHeader != null)
            {
                if(orderHeader.InstallationId == Utilities.SessionHelper.InstallationId)
                {
                    orderHeader.Validated = true;
                    orderHeader.WasValidated = true;

                    // create the snapshot of the record as it is right now so that we can show the red validation flag if the order changes in the future
                    List<OrderLineItem> lineItems = orderData.GetLineItems(orderHeader.SONum, orderHeader.InstallationId).OrderBy(r => r.Product).ToList();
                    orderHeader.ValidatedSnapshot = DashboardUtilities.CreateOrderSnapshot(orderHeader, lineItems);

                    OrderValidation orderValidation = new();
                    orderValidation.UserId = loggedInUser.Id;
                    orderValidation.InstallationId = SessionHelper.InstallationId;
                    orderValidation.SuperAdminId = SessionHelper.SuperAdminId(HttpContext.Session);
                    orderValidation.DateTime = DateTime.Now;
                    orderValidation.SONum = orderHeader.SONum;
                    orderValidation.Snapshot = orderHeader.ValidatedSnapshot;
                    orderData.AddOrderValidation(orderValidation);

                    orderData.Commit();
                    return DashboardUtilities.APISuccessJSON("Order was validated!");
                }
            }

            return DashboardUtilities.APIFailureJSON("Could not validate.");
        }

        [HttpPost("requestorderrevision")]
        public async Task<string> RequestOrderRevision([FromForm] int id, [FromForm] string message)
        {
            User loggedInUser = Utilities.SessionHelper.LoggedInUser(HttpContext.Session, userData);
            if (loggedInUser == null)
            {
                return DashboardUtilities.APIFailureJSON("Not logged in.");
            }

            OrderHeader orderRecord = orderData.GetHeader(id);
            if(orderRecord == null)
            {
                return DashboardUtilities.APIFailureJSON("Could not find order.  Message not sent!");
            }

            Utilities.Email email = new(dashboardData, loggedInUser.InstallationId);
            email.To.Add("customersvs@malarkeyroofing.com");
            email.CC.Add("cbenedict@malarkeyroofing.com");
            email.Subject = "PO#: " + orderRecord.PONum + "/SO#: " + orderRecord.SONum + "/" + orderRecord.CustomerName;

            var model = new RequestOrderRevisionModel();
            model.SONum = orderRecord.SONum;
            model.PONum = orderRecord.PONum;
            model.CustomerName = orderRecord.CustomerName;
            model.Message = message;
            email.Message = await emailRenderer.RenderViewToStringAsync("/Pages/Emails/RequestOrderRevision.cshtml", model);

            if(email.Send())
            {
                DashboardUtilities.APISuccessJSON("Email Sent!");
            }
            else
            {

            }
            

            return DashboardUtilities.APIFailureJSON("Request Order Revision not sent.");
        }

        private void populateTotals(ApiOrderModel model, List<OrderHeader> orderHeaders)
        {
            decimal total = 0;

            if(orderHeaders.Count > 0)
            {
                List<OrderLineItem> totalLineItems = orderData.GetLineItems(orderHeaders, Utilities.SessionHelper.InstallationId).ToList();
                if(totalLineItems.Count > 0)
                {
                    foreach(OrderLineItem li in totalLineItems)
                    {
                        total += li.Total;
                    }
                }
            }

            model.reportRowCount = Utilities.Utilities.IntToString(orderHeaders.Count);
            model.reportTotal = Utilities.Utilities.MoneyFormat(total);
        }
    }
}
