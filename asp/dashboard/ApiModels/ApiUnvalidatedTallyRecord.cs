﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dashboard.v2.ApiModels
{
    public class ApiUnvalidatedTallyRecord
    {
        public string SellNames;
        public int Tally;
        public string SellNum;
        public string Contract;
    }
}
