﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dashboard.v2.ApiModels
{
    public class ApiOrderModel
    {
        public List<ApiOrderRecord> results = new List<ApiOrderRecord>();

        public string reportRowCount = "";

        public string reportTotal = "";

        public int resultsPerPage = 0;

        public int numberOfPages = 1;

        public int pageNumber = 1;

        public string orderBy = "";

        public List<string> sellNums;

        public string type;

        public List<ApiUnvalidatedTallyRecord> unvalidatedTallyRecords = new();

        public List<ApiUnvalidatedTallyRecord> unvalidatedTallyRecordsByContract = new();
    }
}
