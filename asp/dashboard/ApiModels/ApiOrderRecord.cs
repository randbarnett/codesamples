﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dashboard.v2.ApiModels
{
    public class ApiOrderRecord
    {
        public int id;
        public string cnum;
        public string cname;
        public string sonum;
        public string ponum;
        public string shipdate;
        public string contract;
        public string salespeople;
        public string districtName;
        public string regionalName;
        public bool validated;
        public bool wasValidated;
    }
}
