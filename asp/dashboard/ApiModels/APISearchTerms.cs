﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dashboard.v2.ApiModels
{
    public class APISearchTerms
    {
        public string customers;
        public string salesReps;
        public string dateFrom;
        public string dateThru;
        public string sonum;
        public string ponum;
        public string pvrnum;
    }
}
