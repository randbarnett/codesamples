﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dashboard.v2.Utilities
{
    public class JSONHelper
    {
        public static string APIJSONResponse(bool success, string message)
        {
            string str = "{ \"success\": " + success.ToString().ToLower() + ", \"message\":\"" + message.Replace("\"", "\\\"") + "\"}";
            return str;
        }

        public static string Serialize(object obj)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(obj);
        }

        public static T Deserialize<T>(string str)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(str);
        }
    }
}
