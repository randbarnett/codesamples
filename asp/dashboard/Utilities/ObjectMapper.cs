﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Dashboard.v2.Utilities
{
    public static class ObjectMapper
    {
        public static void MatchAndMap<TSource, T>(this TSource source, T destination)
        {
            if (source != null && destination != null)
            {
                List<PropertyInfo> sourceProperties = source.GetType().GetProperties().ToList<PropertyInfo>();
                List<PropertyInfo> destinationProperties = destination.GetType().GetProperties().ToList<PropertyInfo>();

                foreach (PropertyInfo sourceProperty in sourceProperties)
                {
                    PropertyInfo destinationProperty = destinationProperties.Find(item => item.Name == sourceProperty.Name);

                    if (destinationProperty != null)
                    {
                        try
                        {
                            destinationProperty.SetValue(destination, sourceProperty.GetValue(source, null), null);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
            }

        }

        public static T MapProperties<T>(this object source)
        {
            var destination = Activator.CreateInstance<T>();
            MatchAndMap(source, destination);

            return destination;
        }

    }
}