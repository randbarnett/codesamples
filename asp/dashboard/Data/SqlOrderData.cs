﻿using Dashboard.v2.Core;
using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using EFCore.BulkExtensions;
using Dashboard.v2.Data.CompositeModels;

namespace Dashboard.v2.Data
{
    public class SqlOrderData : IOrderData
    {
        private readonly DashboardV2DbContext db;

        public SqlOrderData(DashboardV2DbContext db)
        {
            this.db = db;
        }

        public IEnumerable<OrderHeader> GetHeaders(DateTime startDate, DateTime endDate, bool shipped, int limitStart, int maxRecords, List<string> sellNums, string orderBy, SearchTerms searchTerms, int installationId)
        {
            var query = from o in db.OrderHeaders
                        where o.Shipped == shipped
                        where o.OrderDate >= startDate
                        where o.OrderDate < endDate
                        where o.InstallationId == installationId
                        where o.OrderStatus != "C"
                        select o;

            query = query.Where(r => sellNums.Contains(r.SellNum));

            if (searchTerms.sonum != null && searchTerms.sonum != "")
            {
                query = query.Where(r => r.SONum == searchTerms.sonum);
            }
            if (searchTerms.ponum != null && searchTerms.ponum != "")
            {
                query = query.Where(r => r.PONum == searchTerms.ponum);
            }
            if (searchTerms.pvrnum != null && searchTerms.pvrnum != "")
            {
                query = query.Where(r => r.Contract == searchTerms.pvrnum);
            }
            if (searchTerms.dateFrom != null)
            {
                query = query.Where(r => r.OrderDate >= searchTerms.dateFrom);
            }
            if (searchTerms.dateTo != null)
            {
                query = query.Where(r => r.OrderDate <= searchTerms.dateTo);
            }
            if (searchTerms.customers != null && searchTerms.customers != "")
            {
                string[] customers = searchTerms.customers.Split(',');
                query = query.Where(r => customers.Contains(r.CustomerNum));
            }
            if (searchTerms.salesReps != null && searchTerms.salesReps != "")
            {
                string[] salesReps = searchTerms.salesReps.Split(',');
                query = query.Where(r => salesReps.Contains(r.SellNum));
            }

            if (orderBy == "OrderDate")
            {
                return query.OrderBy(r => r.OrderDate).Skip(limitStart).Take(maxRecords).ToList();
            }
            else if (orderBy == "CustomerName")
            {
                return query.OrderBy(r => r.CustomerName).Skip(limitStart).Take(maxRecords).ToList();
            }
            else if (orderBy == "ShipDate")
            {
                return query.OrderBy(r => r.ShipDate).Skip(limitStart).Take(maxRecords).ToList();
            }
            else if (orderBy == "PONum")
            {
                return query.OrderBy(r => r.PONum).Skip(limitStart).Take(maxRecords).ToList();
            }
            else if (orderBy == "SONum")
            {
                return query.OrderBy(r => r.SONum).Skip(limitStart).Take(maxRecords).ToList();
            }
            return query.OrderBy(r => r.CustomerName).Skip(limitStart).Take(maxRecords).ToList();
        }

        public IEnumerable<OrderLineItem> GetLineItems(string soNum, int installationId)
        {
            var query = from li in db.OrderLineItems
                        where li.SONum == soNum
                        where li.InstallationId == installationId
                        select li;
            return query;
        }

        OrderHeader IOrderData.AddHeader(OrderHeader h)
        {
            db.Add(h);
            return h;
        }

        OrderLineItem IOrderData.AddLineItem(OrderLineItem li)
        {
            db.Add(li);
            return li;
        }

        int IOrderData.Commit()
        {
            return db.SaveChanges();
        }

        void IOrderData.DeleteOrdersNotShipped()
        {           
            //db.OrderHeaders.Where(oh => oh.Shipped == false);

            List<OrderHeader> headers = db.OrderHeaders.Where(oh => oh.Shipped == false).ToList();

            if(headers.Count > 0)
            {
                List<string> sonums = new List<string>();
                foreach(OrderHeader oh in headers)
                {
                    sonums.Add(oh.SONum);
                }
            }            
        }

        void IOrderData.DeleteOrdersBySONum(List<string> sonums)
        {
            db.OrderLineItems.Where(r => sonums.Contains(r.SONum)).BatchDelete();
            db.OrderHeaders.Where(r => sonums.Contains(r.SONum)).BatchDelete();
            db.SaveChanges();
        }

        void IOrderData.DeleteOrderLineItemsBySONum(List<string> sonums)
        {
            db.OrderLineItems.Where(r => sonums.Contains(r.SONum)).BatchDelete();
            db.SaveChanges();
        }

        IEnumerable<OrderHeader> IOrderData.GetAllHeaders(int installationId)
        {
            return db.OrderHeaders.Where(h => h.InstallationId == installationId);
        }

        IEnumerable<OrderLineItem> IOrderData.GetAllLineItems(int installationId)
        {
            return db.OrderLineItems.Where(li => li.InstallationId == installationId);
        }

        List<OrderHeader> IOrderData.GetAllOrdersNotShipped(int installationId)
        {
            var query = from o in db.OrderHeaders
                        where o.Shipped == false
                        where o.InstallationId == installationId
                        select o;
            return query.ToList();
        }

        OrderHeader IOrderData.GetHeader(string sonum, int installationId)
        {
            return db.OrderHeaders.FirstOrDefault(r => r.SONum == sonum);
        }

        OrderHeader IOrderData.UpdateHeader(OrderHeader h)
        {
            var entity = db.OrderHeaders.Attach(h);
            entity.State = EntityState.Modified;
            return h;
        }

        void IOrderData.AddOrderHeaders(List<OrderHeader> headers)
        {
            db.BulkInsert(headers);
        }

        void IOrderData.AddLineItems(List<OrderLineItem> lineItems)
        {
            db.BulkInsert(lineItems);
        }

        IEnumerable<OrderHeader> IOrderData.GetOrdersNotShipped(int limitStart, int maxRecords, List<string> sellNums, string orderBy, SearchTerms searchTerms, int installationId)
        {
            var query = from o in db.OrderHeaders
                        where o.Shipped == false
                        where o.InstallationId == installationId
                        where o.OrderStatus != "C"
                        select o;

            query = query.Where(r => sellNums.Contains(r.SellNum));

            if (searchTerms.sonum != null && searchTerms.sonum != "")
            {
                query = query.Where(r => r.SONum == searchTerms.sonum);
            }
            if (searchTerms.ponum != null && searchTerms.ponum != "")
            {
                query = query.Where(r => r.PONum == searchTerms.ponum);
            }
            if (searchTerms.pvrnum != null && searchTerms.pvrnum != "")
            {
                query = query.Where(r => r.Contract == searchTerms.pvrnum);
            }
            if (searchTerms.dateFrom != null)
            {
                query = query.Where(r => r.OrderDate >= searchTerms.dateFrom);
            }
            if (searchTerms.dateTo != null)
            {
                query = query.Where(r => r.OrderDate <= searchTerms.dateTo);
            }
            if(searchTerms.customers != null && searchTerms.customers != "")
            {
                string[] customers = searchTerms.customers.Split(',');
                query = query.Where(r => customers.Contains(r.CustomerNum));
            }
            if (searchTerms.salesReps != null && searchTerms.salesReps != "")
            {
                string[] salesReps = searchTerms.salesReps.Split(',');
                query = query.Where(r => salesReps.Contains(r.SellNum));
            }

            if (orderBy == "OrderDate")
            {
                return query.OrderBy(r => r.OrderDate).Skip(limitStart).Take(maxRecords).ToList();
            }
            else if(orderBy == "CustomerName")
            {
                return query.OrderBy(r => r.CustomerName).Skip(limitStart).Take(maxRecords).ToList();
            }
            else if (orderBy == "ShipDate")
            {
                return query.OrderBy(r => r.ShipDate).Skip(limitStart).Take(maxRecords).ToList();
            }
            else if (orderBy == "PONum")
            {
                return query.OrderBy(r => r.PONum).Skip(limitStart).Take(maxRecords).ToList();
            }
            else if (orderBy == "SONum")
            {
                return query.OrderBy(r => r.SONum).Skip(limitStart).Take(maxRecords).ToList();
            }
            return query.OrderBy(r => r.CustomerName).Skip(limitStart).Take(maxRecords).ToList();
        }

        OrderHeader IOrderData.GetHeader(int i)
        {
            return db.OrderHeaders.FirstOrDefault(r => r.Id == i);
        }

        IEnumerable<OrderLineItem> IOrderData.GetLineItems(List<OrderHeader> orderHeaders, int installationId)
        {
            List<OrderLineItem> returnLineItems = new List<OrderLineItem>();

            if(orderHeaders.Count > 0)
            {
                List<string> sonums = new List<string>();
                foreach(OrderHeader oh in orderHeaders)
                {
                    sonums.Add(oh.SONum);
                }

                returnLineItems = db.OrderLineItems.Where(r => sonums.Contains(r.SONum)).Where(r => r.InstallationId == installationId).ToList();

            }

            return returnLineItems;
        }

        void IOrderData.UpdateHeaders(List<OrderHeader> headers)
        {
            db.BulkUpdate(headers);
        }

        IEnumerable<OrderHeader> IOrderData.GetOrdersNotValidated(int limitStart, int maxRecords, List<string> sellNums, string orderBy, SearchTerms searchTerms, int installationId)
        {
            var query = from o in db.OrderHeaders
                        where o.Shipped == false
                        where o.InstallationId == installationId
                        where o.OrderStatus != "C"
                        where o.Validated == false
                        select o;

            query = query.Where(r => sellNums.Contains(r.SellNum));

            if (searchTerms.sonum != null && searchTerms.sonum != "")
            {
                query = query.Where(r => r.SONum == searchTerms.sonum);
            }
            if (searchTerms.ponum != null && searchTerms.ponum != "")
            {
                query = query.Where(r => r.PONum == searchTerms.ponum);
            }
            if (searchTerms.pvrnum != null && searchTerms.pvrnum != "")
            {
                query = query.Where(r => r.Contract == searchTerms.pvrnum);
            }
            if (searchTerms.dateFrom != null)
            {
                query = query.Where(r => r.OrderDate >= searchTerms.dateFrom);
            }
            if (searchTerms.dateTo != null)
            {
                query = query.Where(r => r.OrderDate <= searchTerms.dateTo);
            }
            if (searchTerms.customers != null && searchTerms.customers != "")
            {
                string[] customers = searchTerms.customers.Split(',');
                query = query.Where(r => customers.Contains(r.CustomerNum));
            }
            if (searchTerms.salesReps != null && searchTerms.salesReps != "")
            {
                string[] salesReps = searchTerms.salesReps.Split(',');
                query = query.Where(r => salesReps.Contains(r.SellNum));
            }

            if (orderBy == "OrderDate")
            {
                return query.OrderBy(r => r.OrderDate).Skip(limitStart).Take(maxRecords).ToList();
            }
            else if (orderBy == "CustomerName")
            {
                return query.OrderBy(r => r.CustomerName).Skip(limitStart).Take(maxRecords).ToList();
            }
            else if (orderBy == "ShipDate")
            {
                return query.OrderBy(r => r.ShipDate).Skip(limitStart).Take(maxRecords).ToList();
            }
            else if (orderBy == "PONum")
            {
                return query.OrderBy(r => r.PONum).Skip(limitStart).Take(maxRecords).ToList();
            }
            else if (orderBy == "SONum")
            {
                return query.OrderBy(r => r.SONum).Skip(limitStart).Take(maxRecords).ToList();
            }
            return query.OrderBy(r => r.CustomerName).Skip(limitStart).Take(maxRecords).ToList();
        }

        OrderValidation IOrderData.AddOrderValidation(OrderValidation orderValidation)
        {
            db.OrderValidations.Add(orderValidation);
            return orderValidation;
        }

        List<OrderValidation> IOrderData.GetOrderValidations(int installationId, string sonum)
        {
            var query = from r in db.OrderValidations
                        where r.InstallationId == installationId
                        where r.SONum == sonum
                        orderby r.DateTime descending
                        select r;

            return query.ToList();
        }
    }
}
