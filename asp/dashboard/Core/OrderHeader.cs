﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Dashboard.v2.Core
{
    public class OrderHeader
    {
        public int Id { get; set; }

        public int InstallationId { get; set; }
        [StringLength(40)]
        public string SONum { get; set; }
        [StringLength(40)]
        public string PONum { get; set; }
        [StringLength(12)]
        public string CustomerNum { get; set; }
        [StringLength(100)]
        public string CustomerName { get; set; }
        [StringLength(10)]
        public string SellNum { get; set; }

        public DateTime? OrderDate { get; set; }

        public DateTime? RequestedDate { get; set; }

        public DateTime? PricingDate { get; set; }

        public DateTime? ShipDate { get; set; }

        public bool Shipped { get; set; }
        [StringLength(10)]
        public string Status { get; set; }
        [StringLength(10)]
        public string OrderStatus { get; set; }
        [StringLength(50)]
        public string ModeOfDelivery { get; set; }
        [StringLength(200)]
        public string TermsOfDelivery { get; set; }
        [StringLength(100)]
        public string ShipToCity { get; set; }
        [StringLength(10)]
        public string ShipFromSite { get; set; }
        [StringLength(20)]
        public string Currency { get; set; }
        [StringLength(35)]
        public string Contract { get; set; }

        public bool Validated { get; set; }

        public bool WasValidated { get; set; }
        [StringLength(3000)]
        public string ValidatedSnapshot { get; set; }

    }
}
