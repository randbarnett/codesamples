﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dashboard.v2.Core
{
    public class OrderLineItem
    {
        public int Id { get; set; }

        public int InstallationId { get; set; }
        [StringLength(40)]
        public string SONum { get; set; }
        [StringLength(40)]
        public string PONum { get; set; }
        [StringLength(50)]
        public string Product { get; set; }
        [StringLength(200)]
        public string ProductDescription { get; set; }

        [Column(TypeName = "decimal(9,2)")]
        public decimal Quantity { get; set; }

        [Column(TypeName = "decimal(9,2)")]
        public decimal QuantityShipped { get; set; }

        [Column(TypeName = "decimal(9,2)")]
        public decimal Price { get; set; }

        [Column(TypeName = "decimal(9,2)")]
        public decimal Total { get; set; }
        [StringLength(100)]
        public string Contract { get; set; }
        [StringLength(20)]
        public string Color { get; set; }
    }
}
