// Dot Trombone Multiselect

var dtMultiselectIdCount = 0;
var dtMultiselectEventListeners = new Array();

function dtMultiselectAddEventListener(typeStr, idOrClass, funcStr)
{
	dtMultiselectEventListeners.push(new Array(typeStr, idOrClass, funcStr));
}

function dtMultiselectGetValues(id)
{
	var array = new Array();
	$("#" + id + "-dt-multiselect .dt-multiselect-upper-div-record").each(function()
	{
		array.push($(this).attr("value"));
	});
	
	return array;
}

function dtMultiselectDispatchEvent(typeStr, id)
{
	var record;
	var selector;
	var elemType;
	var id2 = id + "-dt-multiselect";
	
	for(var a = 0; a < dtMultiselectEventListeners.length; a++)
	{
		record = dtMultiselectEventListeners[a];
		selector = record[1].substr(1);
		elemType = record[1].charAt(0);
		if(record[0] == typeStr)
		{
			if(elemType == '#')
			{
				if(id2 == selector + "-dt-multiselect")
				{
					this[record[2]]();
				}
			}
			else if(elemType == '.')
			{
				if($("#" + id).hasClass(selector))
				{
					this[record[2]]();
					return;
				}
			}
		}
	}
}

function dtMultiselectClearByClass(pClass)
{
	var id;
	$("." + pClass).each(function()
	{
		var id = $(this).attr("id");
		dtMultiselectClearById(id);
	});
}

function dtMultiselectClearById(id)
{
	dtMultiselectRemoveAllItems(id);
}

function instantiateDTMultiselect(id)
{
	dtMultiselectInstantiateObject($("#" + id));
}

function instantiateDTMultiselectsByClass(pClass)
{
	$("." + pClass).each(function()
	{
		var id = $(this).attr("id");
	
		if(typeof id == "undefined" || id == "")
		{
			id = "dtMutliselectId-" + dtMultiselectIdCount;
			dtMultiselectIdCount++;
			$(this).attr("id", id);
		}
		
		dtMultiselectInstantiateObject(id);
	});
}

function dtMultiselectInputValue(str)
{
	return str.split('"').join('\\"');
}

function dtMultiselectInstantiateObject(id)
{
	var placeholder = $("#" + id).attr("placeholder");
	if(placeholder == "undefined" || placeholder == undefined) placeholder = "";
	
	$("#" + id).attr("multiple", "multiple");
	$("#" + id).before('<div id="' + id + '-dt-multiselect" class="dt-multiselect-upper-div" originalId="' + id + '"><div id="' + id + '-dt-multiselect-placeholder" class="dt-multiselect-placeholder">' + placeholder + '</div><input type="text" id="' + id + '-dt-multiselect-input" class="dt-multiselect-input" onkeyup="javascript: dtMultiselectDoFilter(\'' + id + '\');" /></div>');
	$("#" + id).hide();
	
	var optionListStr = "";
	
	$("#" + id + " option").each(function()
	{
		optionListStr += '<div class="dt-multiselect-list-item" value="' + dtMultiselectInputValue($(this).attr("value")) + '" onclick="javascript: dtMultiselectRecordClick(\'' + id + '\', this, \'' + $(this).attr("value") + '\');" originalId="' + id + '">' + $(this).html() + '</div>';
	});
	
	$("#" + id).before('<div id="' + id + '-dt-multiselect-list" class="dt-multiselect-list" originalId="' + id + '">' + optionListStr + '</div>');
	
	$("#" + id + "-dt-multiselect").click(function()
	{
		dtMultiselectDoFocus(id);
	});
	
	$("#" + id + "-dt-multiselect-list").focusout(function()
	{
		dtMultiselectDoLostFocus(id);
	});
}

function dtMultiselectDoFocus(id)
{	
	$("#" + id + "-dt-multiselect-list").show();
	$("#" + id + "-dt-multiselect-input").focus();
}

function dtMultiselectDoLostFocus(id)
{
	$("#" + id + "-dt-multiselect-list").hide();
}

function dtMultiselectUpperRecordCloseButton()
{
	return '<div class="dt-multiselect-upper-div-close-btn"><span style="margin: 0 .8rem;color:red;font-style: bold;">X</span></div>';
}

function dtMultiselectRecordClick(id, pThis, val)
{
	// add record
	$(pThis).hide();
	$(pThis).addClass("selected");
	$("#" + id + "-dt-multiselect").append('<div class="dt-multiselect-upper-div-record" value="' + val + '" onclick="javascript: dtMultiselectRemoveItem(\'' + $(pThis).attr("value") + '\', \'' + id + '\');" originalId="' + id + '">' + $(pThis).html() + dtMultiselectUpperRecordCloseButton() + '</div>');
	$("#" + id + "-dt-multiselect-placeholder").hide();
	
	var elem = $("#" + id + "-dt-multiselect-input").detach();
	$("#" + id + "-dt-multiselect").append(elem);
	
	$("#" + id + "-dt-multiselect-input").focus();	
	dtMultiselectDispatchEvent("change", id);
}

function dtMultiselectRemoveAllItems(id)
{
	var idValArray = new Array();
	$(".dt-multiselect-upper-div-record").each(function()
	{
		if($(this).attr("originalId") == id)
		{
			idValArray.push(new Array(id, $(this).attr("value")));
		}
	});
	
	for(var a = 0; a < idValArray.length; a++)
	{
		dtMultiselectRemoveItem(idValArray[a][1], idValArray[a][0]);
	}
}

function dtMultiselectRemoveItem(val, id)
{
	$(".dt-multiselect-upper-div-record").each(function()
	{
		if($(this).attr("originalId") == id && $(this).attr("value") == val)
		{
			$(this).remove();
		}
	});
	
	$(".dt-multiselect-list-item").each(function()
	{
		if($(this).attr("originalId") == id && $(this).attr("value") == val)
		{
			$(this).show();
			$(this).removeClass("selected");
		}
	});
	var hasChildren = false;
	$("#" + id + "-dt-multiselect .dt-multiselect-upper-div-record").each(function()
	{
		hasChildren = true;
	});
	if(!hasChildren)
	{
		if($("#" + id + "-dt-multiselect-input").val() == "")
		{
			$("#" + id + "-dt-multiselect-placeholder").show();
		}
	}
	
	//2021-08-27 When a record is removed, the filter is being cleared.  Can't find out why.
	//alert($("#" + id + "-dt-multiselect-input").val());
	//dtMultiselectDoFilter(id);
	$("#" + id + "-dt-multiselect-input").focus();
	dtMultiselectDispatchEvent("change", id);
}

function dtMultiselectIsHovered(id)
{
	return $("#" + id + ":hover").length > 0;
}

function dtMultiselectDoFilter(id)
{
	var str = $("#" + id + "-dt-multiselect-input").val().toLowerCase();
	var str2;
	var showAll = false;	
	if(str == "") showAll = true;		
	
	$(".dt-multiselect-list-item").each(function()
	{
		if($(this).attr("originalId") == id)
		{
			str2 = $(this).html().toLowerCase();
			if(showAll || str2.indexOf(str) != -1)
			{
				if(!$(this).hasClass("selected"))
				{
					$(this).show();
				}
			}
			else
			{
				$(this).hide();
			}
		}
	});
	
	var hasChildren = false;
	$("#" + id + "-dt-multiselect .dt-multiselect-upper-div-record").each(function()
	{
		hasChildren = true;
	});
	
	if(str == "" && !hasChildren)
	{
		$("#" + id + "-dt-multiselect-placeholder").show();
	}
	else
	{
		$("#" + id + "-dt-multiselect-placeholder").hide();
	}
}

$(document).ready(function()
{
	$("body").mouseup(function()
	{
		$(".dt-multiselect-list").each(function()
		{
			if(!dtMultiselectIsHovered($(this).attr("id")) && !dtMultiselectIsHovered($(this).attr("originalId") + "-dt-multiselect-upper-div"))
			{
				$(this).hide();
				$("#" + $(this).attr("originalId") + "-dt-multiselect-input").val("");
				dtMultiselectDoFilter($(this).attr("originalId"));
				$("#" + $(this).attr("originalId") + "-dt-multiselect-upper-div").focus();
			}
			else
			{
							
			}
		});
	});
});