// JavaScript Document

/*
usage:

var ticker = dtTicker(document.getElementById('tickerCanvas'), '/xml/dtFontSet.xml', '/xml/testMessages.xml?random=' + Math.floor(Math.random() * 999999));
ticker.setSize(4, 2, 2, 100, 9, '#666666', '#ff0000');
	
*/


var dtTicker = function(canvas, fontURL, messagesURL)
{
	var obj = new Object();
	
	obj.canvas = canvas;
	obj.context = canvas.getContext('2d');
	obj.dotArray = new Array();
	obj.enterFrameInterval = 0;
	obj.flip = 1;
	obj.fontURL = fontURL;
	obj.fontSet = '';
	obj.fontSetLoaded = false;
	obj.fontArray = new Array();
	obj.messagesURL = messagesURL;
	obj.messages = new Array();
	obj.messagesLoaded = false;
	obj.messageLength = 100;
	obj.testLetter = '';
	obj.tester = "testing";
	obj.baseline = 2;
	obj.dotSize = 2;
	obj.vSpacing = 4;
	obj.hSpacing = 4;
	obj.cols = 10;
	obj.rows = 7;
	obj.defaultColor = '#666666';
	obj.onColor = '#00ff00';
	obj.currentColor = obj.onColor;
	obj.currentMessage = -1;
	obj.caretPosition = 0;
	obj.spaceColumns = 4;
	obj.speed = 40;
	obj.letterSpacing = 1;
	obj.scrollSpeed = 1;
	
	// music
	obj.staff = false;
	obj.numOfStaves = 1;
	obj.staffOffset = 5;
	obj.noteOffset;
	obj.staffSpacing = 6;
	obj.staffColor = "#ffffff";
	
	// music
	obj.drawStaff = function(x, pLength)
	{
		for(var col = 0; col < pLength; col++)
		{
			for(var row = 0; row < 5; row++)
			{
				this.setDotColor(x + col, this.staffOffset + (row * this.staffSpacing), this.staffColor);
			}
		}
	}
	
	obj.computeNoteY = function(note)
	{
		var array = note.split(" ");
		var noteName = array[0];
		var octave = array[1];
		if(octave > 0) octave -= 1;
		octave *= -7;
		switch(noteName)
		{
			case 'a': return octave - 5;
			case 'b': return octave - 6;
			case 'c': return octave;
			case 'd': return octave - 1;
			case 'e': return octave - 2;
			case 'f': return octave - 3;
			case 'g': return octave - 4;
		}
	}
	
	obj.setterTest = function(n)
	{
		if(n == null)
		{
			alert('This is the accessor.');
		}
		else
		{
			alert('This is the mutator.');
		}
	}
	
	obj.dot = function(dotSize, x, y, color)
	{
		//alert("dot()");
		var dotObj = new Object();
		dotObj.x = x;
		dotObj.y = y;
		dotObj.dotSize = dotSize / 2;
		dotObj.defaultColor = color;
		dotObj.currentColor = color;
		dotObj.context = this.context;
		
		dotObj.draw = function()
		{
			this.context.beginPath();
			this.context.arc(this.dotSize + this.x, this.dotSize + this.y, this.dotSize, 0, 2 * Math.PI);
			this.context.fillStyle = this.currentColor;
			this.context.fill();
		}
		
		dotObj.setColor = function(n)
		{
			//alert("setColor()" + n);
			this.currentColor = n;
			this.draw();
		}
		
		dotObj.draw();
		
		return dotObj;
	}
	
	obj.setSize = function(dotSize, vSpacing, hSpacing, cols, rows, defaultColor, onColor)
	{
		this.dotSize = dotSize;
		this.vSpacing = vSpacing;
		this.hSpacing = hSpacing;
		this.cols = cols;
		this.rows = rows;
		if(this.rows > 10)
		{
			this.scrollSpeed = 2;
		}
		else
		{
			this.scrollSpeed = 1;
		}
		this.defaultColor = defaultColor;
		this.onColor = onColor;
		//alert("setSize()");
		this.dotArray = new Array();
		var d;
		for(var col = 0; col < cols; col++)
		{
			this.dotArray[col] = new Array();
			for(var row = 0; row < rows; row++)
			{
				d = this.dot(dotSize, col * (dotSize + hSpacing), row * (dotSize + vSpacing), defaultColor);
				this.dotArray[col][row] = d;
				
				//break;
			}
			//break;
		}
	}
	
	obj.blankSlate = function()
	{
		this.clearContext();
		this.setSize(this.dotSize, this.vSpacing, this.hSpacing, this.cols, this.rows, this.defaultColor, this.onColor);
	}
		
	obj.setDotColor = function(col, row, color)
	{
		if(color == '0' || col < 0 || row >= this.rows || row < 0) return;
		if(this.dotArray.length > col && this.dotArray[col].length > row)
		{
			if(color == '1')
			{
				this.dotArray[col][row].setColor(this.currentColor);
			}
			else
			{
				if(color.charAt(0) != '#') color = '#' + color;
				this.dotArray[col][row].setColor(color);
			}
		}
	}
	
	obj.drawSymbol = function(str, x, y)
	{
		if(str == ' ') return this.spaceColumns;
		x = x | 0;
		y = y | 0;
		for(var a = 0; a < this.fontArray.length; a++)
		{
			if(this.fontArray[a][0] == str)
			{
				return this.mDrawSymbol(x, y, this.fontArray[a][1], this.fontArray[a][2]);
			}
		}
		
		return 0;
	}
	
	obj.mSymbolColumns = function(data)
	{
		return (data.split('!')).length;
	}
	
	obj.mDrawSymbol = function(x, y, data, ignoreBaseline)
	{
		var baseline = (ignoreBaseline == 'true' ? 0 : this.baseline);
		var cols = data.split('!');
		for(var col = 0; col < cols.length; col++)
		{
			var array = cols[col].split('|');
			cols[col] = new Array((array.length > 1 ? array[0] : ''), (array.length > 1 ? array[1] : array[0]));
			cols[col][0] = cols[col][0].split(',');
			cols[col][1] = cols[col][1].split(',');
		}
		
		for(var col = 0; col < cols.length; col++)
		{
			//alert('col = ' + col);
			if(x + col <= this.dotArray.length)
			{
				
				var row = 0;
				
				for(var i = 0; i < cols[col][0].length; i++)
				{
					if(cols[col][0][i] == '') continue;
					row = this.rows - baseline + cols[col][0].length - i - 1;
					this.setDotColor(x + col, row, cols[col][0][i]);
				}				
				// above baseline
				for(var i = 0; i < cols[col][1].length; i++)
				{					
					row = this.rows - baseline - i - 1;
					this.setDotColor(x + col, row + y, cols[col][1][i]);
				}
			}
		}
		
		return col;
	}
	
	obj.drawTestSymbol = function()
	{
		if(this.fontArray.length > 0)
		{
			var n = Math.floor(Math.random() * this.fontArray.length);
			alert(this.fontArray[n][0]);
			alert(this.fontArray[n][1]);
			this.blankSlate();
			this.mDrawSymbol(0, 0, this.fontArray[n][1]);
		}
	}
		
	
	obj.clearContext = function()
	{
		this.context.clearRect(0, 0 , this.canvas.width, this.canvas.height);
	}
	
	obj.parseXMLNode = function(str)
	{
		var obj = new Object();
		if(str.indexOf('<symbol') == 0)
		{
		
		}
	}
	
	obj.hasNode = function(needle, haystack)
	{
		if(haystack.indexOf(needle) == 0)
		{
			return haystack.substr(haystack.indexOf(needle), haystack.indexOf(">", haystack.indexOf(needle)) + 1)
		}
		return "";
	}
	
	obj.hasAttribute = function(attr, haystack)
	{
		if(haystack.indexOf(attr) != -1)
		{
			//return '0';
			return haystack.substr(haystack.indexOf('"', haystack.indexOf(attr)) + 1);// haystack.indexOf(attr));//, haystack.indexOf('"', haystack.indexOf('"', haystack.indexOf(attr)) + 1) - (haystack.indexOf('"', haystack.indexOf(attr)) + 1));
		}
		return "";
	}
					
			
	obj.displayCurrentMessage = function()
	{
		this.blankSlate();
		var message = this.messages[this.currentMessage];
		var char;
		var colCount;
		var xN = this.caretPosition;
		var messageLength = 0;
		var thisLength = 0;
		var endIndex = 0;
		var endIndex2 = 0;
		var startIndex = 0;
		var innerStr = '';
		var subStr = '';
		var yOffset = 0;
		this.currentColor = this.onColor;
		
		//alert(message);
		for(var a = 0; a < message.length; a++)
		{
			yOffset = 0;
			//this.letterSpacing = 0;
			noteOffset = (-1 * this.rows) + this.staffOffset + (this.staffSpacing * 2) + 2;
			char = message.charAt(a);//alert(char + ' ' + xN);
			var messageSubstr = message.substr(a);
			if(char == "<")
			{
				//alert('hit');
				
				var settingsSubstr = this.hasNode("<settings", messageSubstr);//alert(messageSubstr);
				if(settingsSubstr != "")
				{
					//alert(settingsSubstr);
					var spacing2 = this.hasAttribute("letterspacing", settingsSubstr);//alert(spacing2);
					if(spacing2.length > 0)
					{
						//alert('hit');
						this.letterSpacing = parseInt(spacing2);
						
					}
					a += settingsSubstr.length;
					continue;
				}
				
				if(message.indexOf('</settings>', a) == a)
				{
					this.letterSpacing = 1;
					a += 10;
					continue;
				}
				
				
				if(message.indexOf('<staff>', a) == a)
				{
					this.staff = true;
					a += 6;
					continue;
				}
				
				if(message.indexOf('</staff>', a) == a)
				{
					this.staff = false;
					a += 7;
					continue;
				}
				
				if(message.indexOf('<symbol', a) == a)
				{
					endIndex = message.indexOf('</symbol>', a + 1);
					subStr = message.substr(a, endIndex + 9 - a);
					//alert("subStr = " + subStr);
					innerStr = subStr.substr(8, subStr.length - 17);
					//alert("innerStr = " + innerStr);
					colCount = obj.drawSymbol(innerStr, xN, 0);
					thisLength = colCount + this.letterSpacing;
					xN += thisLength;
					messageLength += thisLength;
					a += endIndex + 8 - a;
					continue;
				}
				
				if(message.indexOf('<font ', a) == a)
				{
					endIndex = message.indexOf('>', a);
					if(message.indexOf('color="', a) < endIndex)
					{
						startIndex = message.indexOf('"', message.indexOf('color=', a));
						endIndex2 = message.indexOf('"', startIndex + 1);
						this.currentColor = message.substr(startIndex + 1, endIndex2 - startIndex - 1);
						//alert(currentColor);
						
					}
					a += endIndex - message.indexOf('<font ', a);
					continue;
				}
				if(message.indexOf('</font>', a) == a)
				{
					this.currentColor = this.onColor;
					a += message.indexOf('>', a) - message.indexOf('</font>', a);
					continue;
				}				
				
				if(message.indexOf('<note', a) == a)
				{
					endIndex = message.indexOf('</note>', a + 1);
					subStr = message.substr(a, endIndex + 7 - a);
					//alert("subStr = " + subStr);
					innerStr = subStr.substr(subStr.indexOf(">", 0) + 1, subStr.indexOf("<", subStr.indexOf(">", 0) + 1) - subStr.indexOf(">", 0) - 1) ;
					//alert("innerStr = " + innerStr);
					
					var noteDuration;
					if(message.indexOf('duration="', a) < endIndex)
					{
						startIndex = message.indexOf('"', message.indexOf('duration=', a));
						endIndex2 = message.indexOf('"', startIndex + 1);
						noteDuration = message.substr(startIndex + 1, endIndex2 - startIndex - 1);
						//alert(currentColor);
						
					}
					
					char = "note2";
					yOffset += noteOffset + (this.computeNoteY(innerStr) * (this.staffSpacing / 2));//alert(innerStr);
					//yOffset = this.staffOffset;
					//colCount = obj.drawSymbol("note2", xN, this.staffOffset);
					//thisLength = colCount + this.letterSpacing;
					//xN += thisLength;
					//messageLength += thisLength;
					a += message.indexOf('>', endIndex) - a;
					//continue;
				}
			}
			
			colCount = obj.drawSymbol(char, xN, yOffset);
			thisLength = colCount + this.letterSpacing;
			if(this.staff)
			{
				this.drawStaff(xN, thisLength);
			}
			xN += thisLength;
			messageLength += thisLength;
		}
		
		this.messageLength = messageLength;
	}
	
	obj.doEnterFrame = function()
	{
		this.clearContext();
		this.caretPosition -= this.scrollSpeed;
		
		if(this.caretPosition < -1 * this.messageLength) this.caretPosition = this.cols;
		
		this.displayCurrentMessage();
	}
	
	
	
	obj.animate = function(milliseconds)
	{
		clearInterval(this.enterFrameInterval);
		this.enterFrameInterval = setScopedInterval(this.doEnterFrame, milliseconds, this);
	}
	
	obj.stop = function()
	{
		clearInterval(this.enterFrameInterval);
	}
	
	obj.doInit = function()
	{
		if(this.fontSetLoaded && this.messagesLoaded)
		{
			// display the messages
			this.caretPosition = this.cols;
			this.currentMessage++;
			if(this.currentMessage > this.messages.length - 1) this.currentMessage = 0;
			this.displayCurrentMessage();
			
			this.animate(this.speed)
		}
	}
	
	obj.loadAll = function()
	{
		var t = this;
		t.fontArray = new Array();
		$.get(this.fontURL, function(n)
		{
			var xml = $(n);
			
			xml.find('font').each(function()
			{
				$(this).find('symbol').each(function()
				{
					//alert($(this).text());
					t.fontArray.push(new Array($(this).attr('name'), $(this).text(), $(this).attr('ignoreBaseline')));
					
				});
			});
			
			t.fontSetLoaded = true;
			t.doInit();
			
		}, 'text');
		
		t.messages = new Array();
		$.get(this.messagesURL, function(n)
		{
			var xml = $(n);
			
			xml.find('messages').each(function()
			{
				$(this).find('message').each(function()
				{
					//alert($(this).text());
					t.messages.push($(this).html());
					
				});
			});
			t.messagesLoaded = true;
			t.doInit();
		}, 'text');
	}
	
	obj.loadFontFromData = function(n)
	{
		var t = this;
		var xml = $(n);
		
		t.fontArray = new Array();
			
		xml.find('font').each(function()
		{
			$(this).find('symbol').each(function()
			{
				//alert($(this).text());
				t.fontArray.push(new Array($(this).attr('name'), $(this).text(), $(this).attr('ignoreBaseline')));
				
			});
		});
		
		t.fontSetLoaded = true;
		t.stop();
		t.doInit();
	}
	
	obj.displayMessage = function(str)
	{
		var t= this;
		
		t.messages = new Array();
		
		var xml = $(str);
		
		xml.find('messages').each(function()
		{
			$(this).find('message').each(function()
			{
				//alert($(this).text());
				t.messages.push($(this).html());
				
			});
		});
		t.messagesLoaded = true;
		t.stop();
		t.doInit();
	}
	
	obj.fontXMLTest = function()
	{
		var n = Math.floor(Math.random() * this.fontArray.length);
		return this.fontArray[n][0] + " = " + this.fontArray[n][1];
	}
	
	obj.loadAll();
	
	return obj;
}



