// Dot Trombone ©2004
// works best with a canvas of 400x150
// just include this script, make a canvas and then call dotTrombone('myCanvasId');

var polygonsCanvasId;
var polygonArray = new Array();
var polygonsOldXMN = 0;
var polygonsOldYMN = 0;
var polygonsFrameCount = 0;
var polygonsIdleCount = 1002;
var polygonsDraggingNum = -1;

function newPolygon(pNumPoints, pRadius)
{
	var obj = new Object();
	obj.x = 0;
	obj.y = 0;
	obj.radius = pRadius;
	obj.rotation = 0;
	obj.color = "#ff0000";
	obj.numOfPoints = pNumPoints;
	obj.scopedPoints = function()
	{
		var returnPoints = new Array();
		var spokeDegrees = 360 / this.numOfPoints;		
		
		for(var a = 0; a <= 360; a += spokeDegrees)
		{
			var point = new Object();
			point.x = this.x + (Math.cos(a * dg) * this.radius);
			point.y = this.y + (Math.sin(a * dg) * this.radius);
			returnPoints.push(point);
		}
		
		return returnPoints;
	}
	obj.scopedLines = function()
	{
		var returnLines = new Array();
		var points = this.scopedPoints();
		
		for(var a = 0; a < points.length - 1; a++)
		{
			var array = new Array();
			var obj = new Object();
			obj.x = points[a].x;
			obj.y = points[a].y;
			array.push(obj);
			obj = new Object();
			obj.x = points[a + 1].x;
			obj.y = points[a + 1].y;
			array.push(obj);
			
			returnLines.push(array);
		}
		
		var array = new Array();
		var obj = new Object();
		obj.x = points[points.length - 1].x;
		obj.y = points[points.length - 1].y;
		array.push(obj);
		obj = new Object();
		obj.x = points[0].x;
		obj.y = points[0].y;
		array.push(obj);
		
		returnLines.push(array);
		
		return returnLines;
	}
	
	return obj;
}

function dopolygonsMouseDown(e)
{
	polygonsDraggingNum = -1;
	
	var x = e.clientX;
	var y = e.clientY;
	
	var canvas = document.getElementById(polygonsCanvasId);
	var ctx = canvas.getContext('2d');
	
	var rect = canvas.getBoundingClientRect();
	x -= rect.left;
	y -= rect.top;
	
	var obj;
	
	for(var a = 0; a < polygonArray.length; a++)
	{
		obj = polygonArray[a];
		
		if(findTan(obj.x - x, obj.y - y) < obj.radius)
		{
			polygonsDraggingNum = a;
		}
	}
}

function dopolygonsTouchStart(e)
{
	polygonsDraggingNum = -1;
	
	var x = e.touches[0].clientX;
	var y = e.touches[0].clientY;
	
	var elem = document.getElementById(polygonsCanvasId);
	
	var canvas = document.getElementById(polygonsCanvasId);
	var ctx = canvas.getContext('2d');
	
	var rect = canvas.getBoundingClientRect();
	x -= rect.left;
	y -= rect.top;
	
	polygonsOldXMN = x;
	polygonsOldYMN = y;
	
	var obj;
	
	for(var a = 0; a < polygonArray.length; a++)
	{
		obj = polygonArray[a];
		
		if(findTan(obj.x - x, obj.y - y) < obj.radius)
		{
			polygonsDraggingNum = a;
		}
	}
}

function dopolygonsStageMouseUp(elem)
{
	polygonsDraggingNum = -1;
}

function findTan(a,b)
{
			
	a=Math.abs(a);
	b=Math.abs(b);
	var c = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
	return(c);
}

function polygonDraw(ctx, obj)
{
	ctx.strokeStyle = obj.color;	
	var points = obj.scopedPoints();
	
	ctx.beginPath();	
	ctx.strokeStyle = obj.color;	
	ctx.moveTo(points[0].x, points[0].y);
	
	for(var a = 1; a < points.length; a++)
	{		
		ctx.lineTo(points[a].x, points[a].y);
		ctx.stroke();
	}	
}

function polygonRefresh(x, y, canvas, ctx)
{
	//var canvas = document.getElementById(dottromboneCanvasId);
	//var ctx = canvas.getContext('2d');
	
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	
	var xmn = x;
	var ymn = y;
	
	var xDiff = xmn - polygonsOldXMN;
	var yDiff = ymn - polygonsOldYMN;
	
	polygonsOldXMN = xmn;
	polygonsOldYMN = ymn;
	
	if(polygonsDraggingNum != -1)
	{
		var degrees = Math.atan2(-yDiff, xDiff) * rd;
		var collision = detectCollisionLinear(polygonArray[polygonsDraggingNum], degrees, findTan(yDiff, xDiff), polygonArray, "slide");
		
		
		//polygonArray[polygonsDraggingNum].x += xDiff;
		//polygonArray[polygonsDraggingNum].y += yDiff;
	}
	
	for(var a = 0; a < polygonArray.length; a++)
	{
		polygonDraw(ctx, polygonArray[a]);
	}
}


function polygonsMouseMove(e)
{
	var x = e.clientX;
	var y = e.clientY;
	
	var canvas = document.getElementById(polygonsCanvasId);
	var ctx = canvas.getContext('2d');
	
	var rect = canvas.getBoundingClientRect();
	x -= rect.left;
	y -= rect.top;
	
	polygonRefresh(x, y, canvas, ctx);
}

function polygonsTouchMove(e)
{
	var x = e.touches[0].clientX;
	var y = e.touches[0].clientY;
	
	e.preventDefault();
	
	var elem = document.getElementById(polygonsCanvasId);
	
	var canvas = document.getElementById(polygonsCanvasId);
	var ctx = canvas.getContext('2d');
	
	var rect = canvas.getBoundingClientRect();
	x -= rect.left;
	y -= rect.top;
	
	polygonRefresh(x, y, canvas, ctx);
}

function polygonsInit(id)
{
	polygonsCanvasId = id;
	document.getElementById(id).addEventListener('mousemove', polygonsMouseMove);
	document.getElementById(id).addEventListener('touchmove', polygonsTouchMove);
	
	var canvas = document.getElementById(polygonsCanvasId);
	var ctx = canvas.getContext('2d');
	
	var rect = canvas.getBoundingClientRect();
	
	//insert circles
	
	var obj = newPolygon(5, 45);
	obj.x = (canvas.width / 2) - (canvas.width / 4);
	obj.y = canvas.height / 4;
	obj.color = "#00ff00";
	polygonArray.push(obj);
	
	obj = newPolygon(6, 45);
	obj.x = (canvas.width / 2) + (canvas.width / 4);
	obj.y = canvas.height / 4;
	obj.color = "#00ff00";
	polygonArray.push(obj);
	
	obj = newPolygon(3, 45);
	obj.x = (canvas.width / 2) - (canvas.width / 4);
	obj.y = canvas.height * .75;
	obj.color = "#0000ff";
	polygonArray.push(obj);
	
	obj = newPolygon(8, 45);
	obj.x = (canvas.width / 2) + (canvas.width / 4);
	obj.y = canvas.height * .75;
	obj.color = "#0000ff";
	polygonArray.push(obj);
	
	polygonRefresh(rect.width / 2, rect.height / 2, canvas, ctx);
	
	document.addEventListener("mouseup", dopolygonsStageMouseUp);
	document.addEventListener("touchend", dopolygonsStageMouseUp);
	
	document.addEventListener("mousedown", dopolygonsMouseDown);
	document.addEventListener("touchstart", dopolygonsTouchStart);
}	