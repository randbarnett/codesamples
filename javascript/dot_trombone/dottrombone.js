// Dot Trombone ©2004
// works best with a canvas of 400x150
// just include this script, make a canvas and then call dotTrombone('myCanvasId');

var dottromboneCanvasId;
var dtArray = new Array();
var dtOldXMN = 0;
var dtOldYMN = 0;
var dtFrameCount = 0;
var dtIdleCount = 1002;

function findTan(a,b)
{
			
	a=Math.abs(a);
	b=Math.abs(b);
	var c = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
	return(c);
}

function drawRedDot(ctx, x, y, radius)
{
	ctx.beginPath();
	ctx.arc(x, y, radius, 0, 2 * Math.PI, false);
	ctx.fillStyle = '#f00';
	ctx.fill();
	
	ctx.lineWidth = 1;
	ctx.strokeStyle = "#f00";	
	ctx.stroke();
}

function dottromboneRefresh(x, y, canvas, ctx)
{
	//var canvas = document.getElementById(dottromboneCanvasId);
	//var ctx = canvas.getContext('2d');
	
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	
	var xmn = x;
	var ymn = y;
	
	dtFrameCount++;
	
	
	var x9 = xmn - 200;
	var y9 = ymn - 75;
	
	x9 *= -1;
	y9 *= -1;
	var rot = Math.atan2(y9,x9) * rd;
	
	if(xmn==dtOldXMN&&ymn==dtOldYMN){
		idleCount++;
		if(idleCount>1000){
			rot=180;
		}
	}
	else{
		if(dtFrameCount>5){
			idleCount=0;
		}
	}
	dtOldXMN=xmn;
	dtOldYMN=ymn;
	
	if(dtFrameCount<5){
		rot=180;
	}
	if(xmn<-10){
		//rot=180;
	}
	
	var xN=50+(Math.cos(rot*dg)*5);
	var yN=50-(Math.sin(rot*dg)*5);
	var zN=4;
	var tD=rot+180;
	if(tD>360){
		tD-=360;
	}

	var xD;
	var yD;
	var zD;
	var a;
	var b;
	var c;
	var C1;
	var tD2;
	var xAngle;
	var x1;
	var zDiff;
	var yAngle;
	var zAngle;
	
	for(var q = 1; q <= 235; q++)
	{
		xD=dtArray["xD"+q];
		yD=dtArray["yD"+q];
		zD=dtArray["zD"+q];

		a=yN-yD;
		b=(xN-xD)*-1;
		c=findTan(a,b);
		C1=Math.atan2(a,b)*rd;
		tD2=tD;
		if(tD2>C1+180){
			tD2-=360;}
		if(tD2<C1-180){
			tD2+=360;}
		xAngle=C1-tD2;
	
		x1=Math.cos(xAngle*dg)*c;
		zDiff=(zN-zD)*-1;
		yAngle=Math.atan2(zDiff,x1)*rd;

		zAngle=Math.atan2(.1,x1)*rd;
		if(zAngle<0){
			zAngle*=-1;}
		
		drawRedDot(ctx, 200-(Math.tan(xAngle*dg)*200), 25-(Math.tan(yAngle*dg)*200), (Math.tan(zAngle*dg)*200) / 2);
		
		//dotArray["dot"+q].x=100-(Math.tan(xAngle*dg)*200);
		//dotArray["dot"+q].y=25-(Math.tan(yAngle*dg)*200);
		//dotArray["dot"+q].width=Math.tan(zAngle*dg)*200;
		//dotArray["dot"+q].height=Math.tan(zAngle*dg)*200;
	}
}

function dotTromboneMouseMove(e)
{
	var x = e.clientX;
	var y = e.clientY;
	
	var canvas = document.getElementById(dottromboneCanvasId);
	var ctx = canvas.getContext('2d');
	
	var rect = canvas.getBoundingClientRect();
	x -= rect.left;
	y -= rect.top;
	
	dottromboneRefresh(x, y, canvas, ctx);
}

function dotTromboneTouchMove(e)
{
	var x = e.touches[0].clientX;
	var y = e.touches[0].clientY;
	
	e.preventDefault();
	
	var elem = document.getElementById(dottromboneCanvasId);
	
	var canvas = document.getElementById(dottromboneCanvasId);
	var ctx = canvas.getContext('2d');
	
	var rect = canvas.getBoundingClientRect();
	x -= rect.left;
	y -= rect.top;
	
	dottromboneRefresh(x, y, canvas, ctx);
}

function dotTrombone(id)
{
	dottromboneCanvasId = id;
	document.getElementById(id).addEventListener('mousemove', dotTromboneMouseMove);
	document.getElementById(id).addEventListener('touchmove', dotTromboneTouchMove);
	
	//insert dot trombone
	
	var q;
			
	for(q=1;q<=50;q++){
		dtArray["xD"+q]=50;
		dtArray["yD"+q]=49+(q*.08);
		dtArray["zD"+q]=3;
	}
	
	for(q=1;q<=18;q++){
		dtArray["xD"+(q+50)]=50;
		dtArray["yD"+(q+50)]=49-(Math.sin((q*10)*dg)*.25);
		dtArray["zD"+(q+50)]=3.25-(Math.cos((q*10)*dg)*.25);
	}
	for(q=1;q<=20;q++){
		dtArray["xD"+(q+68)]=50;
		dtArray["yD"+(q+68)]=49+(q*.1);
		dtArray["zD"+(q+68)]=3.5;
	}
	for(q=1;q<=18;q++){
		dtArray["xD"+(88+q)]=49.75+(Math.cos((q*10)*dg)*.25);
		dtArray["yD"+(88+q)]=53+(Math.sin((q*10)*dg)*.25);
		dtArray["zD"+(88+q)]=3;
	}
	for(q=1;q<=25;q++){
		dtArray["xD"+(106+q)]=49.5;
		dtArray["yD"+(106+q)]=53-(.1*q);
		dtArray["zD"+(106+q)]=3;
	}
	for(q=1;q<=9;q++){
		dtArray["xD"+(131+q)]=50;
		dtArray["yD"+(131+q)]=51+(Math.sin((q*10)*dg)*.45);
		dtArray["zD"+(131+q)]=3.75-(Math.cos((q*10)*dg)*.25);
	}
	for(q=1;q<=9;q++){
		dtArray["xD"+(140+q)]=50;
		dtArray["yD"+(140+q)]=51+(Math.sin((q*10)*dg)*.45);
		dtArray["zD"+(140+q)]=3.25+(Math.cos((q*10)*dg)*.25);
	}
	for(q=1;q<=9;q++){
		dtArray["xD"+(149+q)]=50-(Math.cos((q*10)*dg)*.25);;
		dtArray["yD"+(149+q)]=51+(Math.sin((q*10)*dg)*.45);
		dtArray["zD"+(149+q)]=3.5
	}
	for(q=1;q<=9;q++){
		dtArray["xD"+(158+q)]=50+(Math.cos((q*10)*dg)*.25);
		dtArray["yD"+(158+q)]=51+(Math.sin((q*10)*dg)*.45);
		dtArray["zD"+(158+q)]=3.5
	}
	for(q=1;q<=18;q++){
		dtArray["xD"+(167+q)]=50+(Math.sin((q*20)*dg)*.25);
		dtArray["yD"+(167+q)]=51.45;
		dtArray["zD"+(167+q)]=3.5+(Math.cos((q*20)*dg)*.25);
	}
	for(q=1;q<=10;q++){
		dtArray["xD"+(185+q)]=50;
		dtArray["yD"+(185+q)]=50.4;
		dtArray["zD"+(185+q)]=3+(.05*q);
	}
	for(q=1;q<=10;q++){
		dtArray["xD"+(195+q)]=50;
		dtArray["yD"+(195+q)]=49.2;
		dtArray["zD"+(195+q)]=3+(.05*q);
	}
	for(q=1;q<=10;q++){
		dtArray["xD"+(205+q)]=50;
		dtArray["yD"+(205+q)]=49.6;
		dtArray["zD"+(205+q)]=3+(.05*q);
	}
	for(q=1;q<=10;q++){
		dtArray["xD"+(215+q)]=50-(.05*q);
		dtArray["yD"+(215+q)]=50.6;
		dtArray["zD"+(215+q)]=3;
	}
	for(q=1;q<=10;q++){
		dtArray["xD"+(225+q)]=50-(.05*q);
		dtArray["yD"+(225+q)]=51;
		dtArray["zD"+(225+q)]=3;
	}
	
	var canvas = document.getElementById(dottromboneCanvasId);
	var ctx = canvas.getContext('2d');
	
	var rect = canvas.getBoundingClientRect();
	
	dottromboneRefresh(rect.width / 2, rect.height / 2, canvas, ctx);
}	