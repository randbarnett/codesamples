// Dot Trombone ©2004
// works best with a canvas of 400x150
// just include this script, make a canvas and then call dotTrombone('myCanvasId');

var intersectionOfTwoLinesCanvasId;
var intersectionOfTwoLinesArray = new Array();
var intersectionOfTwoLinesOldXMN = 0;
var intersectionOfTwoLinesOldYMN = 0;
var intersectionOfTwoLinesFrameCount = 0;
var intersectionOfTwoLinesIdleCount = 1002;
var intersectionOfTwoLinesDraggingNum = -1;

function doIntersectionOfTwoLinesMouseDown(e)
{
	intersectionOfTwoLinesDraggingNum = -1;
	
	var x = e.clientX;
	var y = e.clientY;
	
	var canvas = document.getElementById(intersectionOfTwoLinesCanvasId);
	var ctx = canvas.getContext('2d');
	
	var rect = canvas.getBoundingClientRect();
	x -= rect.left;
	y -= rect.top;
	
	var obj;
	
	for(var a = 0; a < intersectionOfTwoLinesArray.length; a++)
	{
		obj = intersectionOfTwoLinesArray[a];
		
		if(findTan(obj.x - x, obj.y - y) < obj.radius)
		{
			intersectionOfTwoLinesDraggingNum = a;
		}
	}
}

function doIntersectionOfTwoLinesTouchStart(e)
{
	intersectionOfTwoLinesDraggingNum = -1;
	
	var x = e.touches[0].clientX;
	var y = e.touches[0].clientY;
	
	var canvas = document.getElementById(intersectionOfTwoLinesCanvasId);
	var ctx = canvas.getContext('2d');
	
	var rect = canvas.getBoundingClientRect();
	x -= rect.left;
	y -= rect.top;
	
	intersectionOfTwoLinesOldXMN = x;
	intersectionOfTwoLinesOldYMN = y;
	
	var obj;
	
	for(var a = 0; a < intersectionOfTwoLinesArray.length; a++)
	{
		obj = intersectionOfTwoLinesArray[a];
		
		if(findTan(obj.x - x, obj.y - y) < obj.radius)
		{
			intersectionOfTwoLinesDraggingNum = a;
		}
	}
	
	intersectionOfTwoLinesRefresh(x, y, canvas, ctx);
}

function doIntersectionOfTwoLinesStageMouseUp(elem)
{
	intersectionOfTwoLinesDraggingNum = -1;
}

function findTan(a,b)
{
			
	a=Math.abs(a);
	b=Math.abs(b);
	var c = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
	return(c);
}

function intersectionOfTwoLinesDrawCircle(ctx, x, y, radius, color)
{
	ctx.beginPath();
	ctx.arc(x, y, radius, 0, 2 * Math.PI, false);
	//ctx.fillStyle = '#f00';
	//ctx.fill();
	
	ctx.lineWidth = 1;
	ctx.strokeStyle = color;	
	ctx.stroke();
}

function intersectionOfTwoLinesRefresh(x, y, canvas, ctx)
{
	//var canvas = document.getElementById(dottromboneCanvasId);
	//var ctx = canvas.getContext('2d');
	
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	
	var xmn = x;
	var ymn = y;
	
	var xDiff = xmn - intersectionOfTwoLinesOldXMN;
	var yDiff = ymn - intersectionOfTwoLinesOldYMN;
	
	if(intersectionOfTwoLinesOldXMN == 0 || intersectionOfTwoLinesOldYMN == 0)
	{
		xDiff = 0;
		yDiff = 0;
	}
	
	intersectionOfTwoLinesOldXMN = xmn;
	intersectionOfTwoLinesOldYMN = ymn;
	
	var obj;
	
	if(intersectionOfTwoLinesDraggingNum != -1)
	{
		intersectionOfTwoLinesArray[intersectionOfTwoLinesDraggingNum].x += xDiff;
		intersectionOfTwoLinesArray[intersectionOfTwoLinesDraggingNum].y += yDiff;
	}
	
	var mIsIntercepting = isIntercepting(intersectionOfTwoLinesArray[0], intersectionOfTwoLinesArray[1], intersectionOfTwoLinesArray[2], intersectionOfTwoLinesArray[3]);
	
	// draw the connecting lines first so that they are under the circles
	ctx.beginPath();
	ctx.strokeStyle = (mIsIntercepting ? "red" : intersectionOfTwoLinesArray[0].color);
	ctx.moveTo(intersectionOfTwoLinesArray[0].x, intersectionOfTwoLinesArray[0].y);
	ctx.lineTo(intersectionOfTwoLinesArray[1].x, intersectionOfTwoLinesArray[1].y);	
	ctx.stroke();
	
	ctx.beginPath();
	ctx.strokeStyle = (mIsIntercepting ? "red" : intersectionOfTwoLinesArray[2].color);
	ctx.moveTo(intersectionOfTwoLinesArray[2].x, intersectionOfTwoLinesArray[2].y);
	ctx.lineTo(intersectionOfTwoLinesArray[3].x, intersectionOfTwoLinesArray[3].y);
	ctx.stroke();
	
	for(var a = 0; a < intersectionOfTwoLinesArray.length; a++)
	{
		obj = intersectionOfTwoLinesArray[a];
		intersectionOfTwoLinesDrawCircle(ctx, obj.x, obj.y, obj.radius, obj.color);
	}	
}


function intersectionOfTwoLinesMouseMove(e)
{
	var x = e.clientX;
	var y = e.clientY;
	
	var canvas = document.getElementById(intersectionOfTwoLinesCanvasId);
	var ctx = canvas.getContext('2d');
	
	var rect = canvas.getBoundingClientRect();
	x -= rect.left;
	y -= rect.top;
	
	intersectionOfTwoLinesRefresh(x, y, canvas, ctx);
}

function intersectionOfTwoLinesTouchMove(e)
{
	var x = e.touches[0].clientX;
	var y = e.touches[0].clientY;
	
	e.preventDefault();
	
	var elem = document.getElementById(intersectionOfTwoLinesCanvasId);
	
	var canvas = document.getElementById(intersectionOfTwoLinesCanvasId);
	var ctx = canvas.getContext('2d');
	
	var rect = canvas.getBoundingClientRect();
	x -= rect.left;
	y -= rect.top;
	
	intersectionOfTwoLinesRefresh(x, y, canvas, ctx);
}

function intersectionOfTwoLinesInit(id)
{
	intersectionOfTwoLinesCanvasId = id;
	document.getElementById(id).addEventListener('mousemove', intersectionOfTwoLinesMouseMove);
	document.getElementById(id).addEventListener('touchmove', intersectionOfTwoLinesTouchMove);
	
	var canvas = document.getElementById(intersectionOfTwoLinesCanvasId);
	var ctx = canvas.getContext('2d');
	
	var rect = canvas.getBoundingClientRect();
	
	//insert circles
	
	var obj = new Object();
	obj.x = (canvas.width / 2) - (canvas.width / 4);
	obj.y = canvas.height / 4;
	obj.radius = 30;
	obj.color = "#00ff00";
	intersectionOfTwoLinesArray.push(obj);
	
	obj = new Object();
	obj.x = (canvas.width / 2) + (canvas.width / 4);
	obj.y = canvas.height / 4;
	obj.radius = 30;
	obj.color = "#00ff00";
	intersectionOfTwoLinesArray.push(obj);
	
	obj = new Object();
	obj.x = (canvas.width / 2) - (canvas.width / 4);
	obj.y = canvas.height * .75;
	obj.radius = 30;
	obj.color = "#0000ff";
	intersectionOfTwoLinesArray.push(obj);
	
	obj = new Object();
	obj.x = (canvas.width / 2) + (canvas.width / 4);
	obj.y = canvas.height * .75;
	obj.radius = 30;
	obj.color = "#0000ff";
	intersectionOfTwoLinesArray.push(obj);
	
	intersectionOfTwoLinesRefresh(rect.width / 2, rect.height / 2, canvas, ctx);
	
	document.addEventListener("mouseup", doIntersectionOfTwoLinesStageMouseUp);
	document.addEventListener("touchend", doIntersectionOfTwoLinesStageMouseUp);
	
	document.addEventListener("mousedown", doIntersectionOfTwoLinesMouseDown);
	document.addEventListener("touchstart", doIntersectionOfTwoLinesTouchStart);
}	