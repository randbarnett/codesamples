// Circle Show

var circleShowCanvasId;
var circleShowObjects = new Array();
var dg = Math.PI / 180;
var circleShowDragNum = -1;
var lastMouseX = 0;
var lastMouseY = 0;
var circleShowDragTolerance = 10;

function circleShowRefresh()
{
	var canvas = document.getElementById(circleShowCanvasId);
	var ctx = canvas.getContext('2d');
	
	var rect = canvas.getBoundingClientRect();
	
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	
	ctx.beginPath();
	ctx.rect(0, 0, canvas.width, canvas.height);
	ctx.fillStyle = "#000";
	ctx.fill();
	
	var obj;
	
	for(var a = 0; a < circleShowObjects.length; a++)
	{
		obj = circleShowObjects[a];
		
		for(var degrees = 0; degrees < 360; degrees += obj.spacingDegrees)
		{
			ctx.beginPath();
			ctx.strokeStyle = "#0f0";
			ctx.moveTo(obj.x, obj.y);
			ctx.lineTo(obj.x + (Math.cos(degrees * dg) * obj.radius), obj.y - (Math.sin(degrees * dg) * obj.radius));
			ctx.stroke();
		}
	}
}

function circleShowCreateNewCircle(x, y, spacingDegrees)
{
	var obj = new Object();
	obj.x = x;
	obj.y = y;
	obj.spacingDegrees = spacingDegrees;
	obj.radius = 1200;
	
	circleShowObjects.push(obj);
	
	circleShowRefresh();
}

function circleShowNewCircle()
{
	var canvas = document.getElementById(circleShowCanvasId);	
	
	circleShowCreateNewCircle(canvas.width / 2, canvas.height / 2, 2);
}

function circleShowMouseMove(e)
{
	var x = e.clientX;
	var y = e.clientY;
	
	var canvas = document.getElementById(circleShowCanvasId);
	var ctx = canvas.getContext('2d');
	
	var rect = canvas.getBoundingClientRect();
	x -= rect.left;
	y -= rect.top;
	
	var xDiff = x - lastMouseX;
	var yDiff = y - lastMouseY;
	
	lastMouseX = x;
	lastMouseY = y;
	
	for(var a = 0; a < circleShowObjects.length; a++)
	{
		if(circleShowDragNum == a)
		{
			var obj = circleShowObjects[a];
			
			obj.x += xDiff;
			obj.y += yDiff;
		}
	}
	
	circleShowRefresh();
}

function circleShowTouchMove(e)
{
	var x = e.touches[0].clientX;
	var y = e.touches[0].clientY;
	
	e.preventDefault();
	
	var elem = document.getElementById(circleShowCanvasId);
	
	var canvas = document.getElementById(circleShowCanvasId);
	var ctx = canvas.getContext('2d');
	
	var rect = canvas.getBoundingClientRect();
	x -= rect.left;
	y -= rect.top;
	
	var xDiff = x - lastMouseX;
	var yDiff = y - lastMouseY;
	
	lastMouseX = x;
	lastMouseY = y;
	
	for(var a = 0; a < circleShowObjects.length; a++)
	{
		if(circleShowDragNum == a)
		{
			var obj = circleShowObjects[a];
			
			obj.x += xDiff;
			obj.y += yDiff;
		}
	}
	
	circleShowRefresh();
}

function circleShowMouseDown(e)
{
	var x = e.clientX;
	var y = e.clientY;
	
	var canvas = document.getElementById(circleShowCanvasId);
	var ctx = canvas.getContext('2d');
	
	var rect = canvas.getBoundingClientRect();
	x -= rect.left;
	y -= rect.top;	
	
	circleShowDragNum = -1;
	lastMouseX = x;
	lastMouseY = y;
	
	for(var a = 0; a < circleShowObjects.length; a++)
	{
		var obj = circleShowObjects[a];
		
		if(Math.abs(obj.x - x) < circleShowDragTolerance && Math.abs(obj.y - y) < circleShowDragTolerance)
		{
			// drag it!
			circleShowDragNum = a;
		}
	}
}

function circleShowTouchDown(e)
{
	var x = e.touches[0].clientX;
	var y = e.touches[0].clientY;
	
	var elem = document.getElementById(circleShowCanvasId);
	
	var canvas = document.getElementById(circleShowCanvasId);
	var ctx = canvas.getContext('2d');
	
	var rect = canvas.getBoundingClientRect();
	x -= rect.left;
	y -= rect.top;
	
	circleShowDragNum = -1;
	lastMouseX = x;
	lastMouseY = y;
	
	for(var a = 0; a < circleShowObjects.length; a++)
	{
		var obj = circleShowObjects[a];
		
		if(Math.abs(obj.x - x) < circleShowDragTolerance && Math.abs(obj.y - y) < circleShowDragTolerance)
		{
			// drag it!
			circleShowDragNum = a;
		}
	}
}

function circleShowMouseUp(e)
{
	circleShowDragNum = -1;
}

function circleShowTouchUp(e)
{
	circleShowDragNum = -1;
}

function circleShowRemoveAll()
{
	circleShowObjects = new Array();
	circleShowDragNum = -1;
	circleShowRefresh();
}

function circleShow(id)
{
	circleShowCanvasId = id;
	
	document.getElementById(id).addEventListener('mousemove', circleShowMouseMove);
	document.getElementById(id).addEventListener('touchmove', circleShowTouchMove);
	
	document.getElementById(id).addEventListener('mousedown', circleShowMouseDown);
	document.getElementById(id).addEventListener('mouseup', circleShowMouseUp);
	
	document.getElementById(id).addEventListener('touchstart', circleShowTouchDown);
	document.getElementById(id).addEventListener('touchend', circleShowTouchUp);
	
	var canvas = document.getElementById(circleShowCanvasId);
	
	circleShowCreateNewCircle(Math.round(canvas.width * .33), Math.round(canvas.height / 2), 2);
	circleShowCreateNewCircle(Math.round(canvas.width * .66), Math.round(canvas.height / 2), 2);
	
	
	
}